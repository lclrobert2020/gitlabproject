// const format = require('date-fns/format')
let category = []
let dateList = []
let expense = []
let totalExpense = []


function getDate() {
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1;
  let yyyy = today.getFullYear();

  if (dd < 10) {
      dd = '0' + dd
  }

  if (mm < 10) {
      mm = '0' + mm
  }

  today = yyyy + '-' + mm + '-' + dd;
  document.querySelector('.dayFrom input[type="date"]').value = today;
  document.querySelector('.dayTo input[type="date"]').value = today;
}

const chartTableA = document.querySelector('.chartAreaA')
const chartTableB = document.querySelector('.chartAreaB')

let submit = document.querySelector("#dateInput")
submit.addEventListener("submit", async (event) => {
  event.preventDefault()

  genPieChart()
  genLineChart()
})

async function exportCSV() {
  let dateFrom = document.querySelector('.dayFrom input[type="date"]').value
  let dateTo = document.querySelector('.dayTo input[type="date"]').value
  
  window.open(`/report.csv?dateFrom=${dateFrom}&dateTo=${dateTo}`)

}

async function getDataWithDate() { 
  let dateFrom = await document.querySelector('.dayFrom input[type="date"]').value
  let dateTo = await document.querySelector('.dayTo input[type="date"]').value
  const response = await fetch(`/report?dateFrom=${dateFrom}&dateTo=${dateTo}`, { // `/page?date=${date}` or `/report?dateFrom=${dateFrom}&dateTo=${dateTo}`
      method: 'GET'
  })
  const transactions = await response.json()

  console.log(transactions)
  if (transactions.length == 0) {
    chartTableA.innerHTML = `
    <div>There is no expense data on the chosen range.
      <canvas id="pieChart" width="400" height="400" style="display: none"></canvas>
    </div>
    `
    chartTableB.innerHTML = `
    <div>There is no expense data on the chosen range.
      <canvas id="barChart" width="400" height="400" style="display: none"></canvas>
    </div>
    `
    
  } else {
    category = []
    expense = []

    transactions.forEach(transaction => {
      console.log(transaction)
      const perCategory = transaction.name
      category.push(perCategory)
      const perExpense = transaction.sum
      expense.push(perExpense)
    });
  }
}

async function getDailyExpenseWithDate() { 
  let dateFrom = await document.querySelector('.dayFrom input[type="date"]').value
  let dateTo = await document.querySelector('.dayTo input[type="date"]').value
  const response = await fetch(`/dailyExpense?dateFrom=${dateFrom}&dateTo=${dateTo}`, { // `/page?date=${date}` or `/report?dateFrom=${dateFrom}&dateTo=${dateTo}`
      method: 'GET'
  })
  const transactions = await response.json()

  // console.log(dateFrom," - ", dateTo)
  // console.log(transactions)
  if (transactions.length == 0) {
    chartTableA.innerHTML = `
    <div>There is no expense data on the chosen range.
      <canvas id="pieChart" width="400" height="400" style="display: none"></canvas>
    </div>
    `
    
  } else {
    dateList = []
    totalExpense = []

    transactions.forEach(transaction => {
      console.log(transaction.date)
      const perDate = transaction.date
      dateList.push(new Date(perDate).toDateString())
      const perExpense = transaction.sum
      totalExpense.push(perExpense)
    });
  }
}

async function genPieChart() {
  chartTableA.innerHTML = `<canvas id="pieChart" width="400vw" height="400vh"></canvas>`
  await getDataWithDate()
  const ctx = document.getElementById('pieChart').getContext('2d');
  new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels: category,
      datasets: [{
        label: 'HKD',
        data: expense,
        backgroundColor: [
          'rgba(255, 99, 132)',
          'rgba(54, 162, 235)',
          'rgba(255, 206, 86)',
          'rgba(75, 192, 192)',
          'rgba(153, 102, 255)',
          'rgba(255, 159, 64)',
          'rgba(210,241,59)',
          'rgba(255,83,100)',
          'rgba(248,127,240)',
          'rgba(127,209,244)',
          'rgba(98,232,178)',
          'rgba(210,241,118)',
          'rgb(73,237,237)'
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          'rgba(210,241,59, 1)',
          'rgba(255,83,100, 1)',
          'rgba(248,127,240, 1)',
          'rgba(127,209,244, 1)',
          'rgba(98,232,178, 1)',
          'rgba(210,241,118, 1)',
          'rgb(73,237,237, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      responsive: true, 
      maintainAspectRatio: false,
    }
  })
  let pieSize = chartTableA.querySelector("#pieChart")
  pieSize.style.height = "500px"
  pieSize.style.width = "auto"
};

async function genLineChart() {
  chartTableB.innerHTML = `<canvas id="lineChart" width="700px" height="250px"></canvas>`
  await getDailyExpenseWithDate()
  let ctx = document.getElementById('lineChart').getContext('2d');
  let myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: dateList,
      datasets: [{   
        label: 'HKD',
        data: totalExpense,
        backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      maintainAspectRatio: false,
      // indexAxis: 'y',
      borderSkipped: 'right',
      scales: {
        xAxes: [{
          
          ticks: {
              beginAtZero: true,
              
          }
        }],
        yAxes: [{
          // barThickness: 20,
          // maxBarThickness: 20,
          beginAtZero: true,
          min: 0,
          barPercentage: 0.7,
          categoryPercentage: 1.0,
          responsive: true,
        }]
      },

    }
  });
  let lineSize = chartTableB.querySelector("#lineChart")
  // lineSize.style.height = "300px"
  // lineSize.style.width = "900px"
}

window.onload = function () {
  getDate();
  genPieChart()
  genLineChart()
  loadUser()
};

async function loadUser() {
  const res = await fetch('/personalinfo', {
      method: "get",
  });
  const result = await res.json();
  if (result.username) {
      document.querySelector("#navbarSupportedContent").innerHTML = `
      <ul class="navbar-nav ml-auto">
      <li class="nav-item">
          <a class="nav-link" href="/wallet.html" id="usernameDiv"></a>
      </li>
      </ul>   
      <a href="/logout" class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</a>
  `
      if(result.picture){
          document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`
      }else{
          document.querySelector("#usernameDiv").innerHTML = `<img src="/userImage/default.png" alt="userpicture"  height="30px" width="30px"> ${result.username}`
      }
  }

  console.log(result)
}

