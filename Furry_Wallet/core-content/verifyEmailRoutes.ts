import nodemailer from "nodemailer"
// import { client } from './main';
import dotenv from "dotenv";

dotenv.config();


// Not a Route, You will learn to use this tomorrow

export function verifyEmail(clientEmail: string, activate_code: string, type: number) {

    let transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.VER_EMAIL,
            pass: process.env.VER_PW

        }
    })
    let title =``
    let link = ``
    if (type === 0) {
        title ='This is verification email from furry wallet';
        // Would have problem to write localhost:8080
        link = `<a href="http://localhost:8080/verify?code=${activate_code}" title="verify email">Please click this link to verify your account</a>`
    }else{
        title = `This is reset password email from furry wallet`
        link = `<a href="http://localhost:8080/resetpassword.html?code=${activate_code}" title="verify email">Please click this link to reset your password</a>`
    }


    let options = {
        //寄件者
        from: process.env.VER_EMAIL,
        //收件者
        to: clientEmail,

        //主旨
        subject: title, // Subject line
        //純文字
        text: 'Welcome to furry wallet, please use the following link to verify your email account', // plaintext body
        //嵌入 html 的內文
        html: link,
    };

    // Promisify
    transporter.sendMail(options, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('訊息發送: ' + info.response);
        }
    });

}