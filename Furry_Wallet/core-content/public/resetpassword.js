let password = document.querySelector("#passwordField")
let confirm_password = document.querySelector("#passwordRepeatField");

function validatePassword() {
    if (password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
        document.querySelector("#submit").click();
    } else {
        confirm_password.setCustomValidity('');
    }
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;


window.onload = function checkLoginError() {
    let searchURL = window.location.search;
    const urlParams = new URLSearchParams(searchURL);

    if(urlParams.get('code')){
        document.querySelector('#resetCode').value = urlParams.get('code');
        // document.querySelector('#resetCode').setAttribute("value", `${urlParams.get('code')}`)
    }

    if (urlParams.get('usernameFieldError')) {
        document.querySelector("#usernameFieldError").innerHTML = urlParams.get('usernameFieldError');
    }
    if (urlParams.get('emailFieldError')) {
        document.querySelector("#emailFieldError").innerHTML = urlParams.get('emailFieldError');
    }
    if (urlParams.get('passwordRepeatField')) {
        document.querySelector("#passwordRepeatFieldError").innerHTML = urlParams.get('passwordRepeatField');
    }
    if (urlParams.get('passwordFieldError')) {
        document.querySelector("#passwordFieldError").innerHTML = urlParams.get('passwordFieldError');
    }
}