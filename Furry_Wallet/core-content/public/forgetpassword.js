

window.onload = function checkLoginError() {
    let searchURL = window.location.search;
    const urlParams = new URLSearchParams(searchURL);
    if (urlParams.get('usernameFieldError')) {
        document.querySelector("#usernameFieldError").innerHTML = urlParams.get('usernameFieldError');
    }
    if (urlParams.get('emailFieldError')) {
        document.querySelector("#emailFieldError").innerHTML = urlParams.get('emailFieldError');
    }
    if (urlParams.get('passwordRepeatField')) {
        document.querySelector("#passwordRepeatFieldError").innerHTML = urlParams.get('passwordRepeatField');
    }
    if (urlParams.get('passwordFieldError')) {
        document.querySelector("#passwordFieldError").innerHTML = urlParams.get('passwordFieldError');
    }
}