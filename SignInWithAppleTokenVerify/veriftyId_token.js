const axios = require("axios");
const jwt = require("jsonwebtoken");
const NodeRSA = require('node-rsa');

const getApplePublicKey = async (id_token) => {
    let res = await axios.request({
        method: "GET",
        url: "https://appleid.apple.com/auth/keys",
    })
    const keyArray = res.data.keys
    if (!Array.isArray(keyArray) || keyArray.length === 0) {
        throw 'Either the public keys array is empty or its not an array'
    }
    let decodedJWT = jwt.decode(id_token, { complete: true })
    if (decodedJWT === null) {
        throw 'The decoded JWT is null'
    } else if (typeof decodedJWT === 'string') {
        throw 'The decoded JWT has no header'
    } else {
        const header = decodedJWT.header;
        if (!header || !header.kid) {
            throw 'The decoded JWT has header without "kid" key and value pair'
        }
        const headerKid = header.kid;
        const filteredKeyArray = keyArray.filter((element) => { return element.kid == headerKid })
        if (filteredKeyArray.length === 0) {
            throw 'No public keys have the same kid as the idToken kid'
        }
        const publicKey = filteredKeyArray[0];
        const pubKey = new NodeRSA();
        pubKey.importKey({ n: Buffer.from(publicKey.n, 'base64'), e: Buffer.from(publicKey.e, 'base64') }, 'components-public');
        return {publicKey: pubKey.exportKey('public')}
    }
}

const verifyIdToken = async (id_Token, nonce=undefined) => {
    const appleKey= await getApplePublicKey(id_Token);
    const applePublicKey = appleKey.publicKey;
    const option = nonce ? { algorithms: ['RS256'], nonce: nonce } : { algorithms: ['RS256']};
    const jwtClaims = jwt.verify(id_Token, applePublicKey, option);
    return jwtClaims;
};


const veriftyId_token = (token, nonce = undefined) => {
    return new Promise((res, rej) => {
        (async (res, rej) => { 
            try {
                const result = await verifyIdToken(token, nonce); 
                res(result) 
            } catch (error) {
                rej(error)
            }
        })(res,rej)
    })
}

module.exports=veriftyId_token;