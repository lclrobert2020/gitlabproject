import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('dinning_records')) {
    return
  }
  await knex.schema.createTable('dinning_records', table => {
    table.increments();
    table.integer('user_id').unsigned().notNullable().references('users.id');
    table.integer('restaurant_id').unsigned().notNullable().references('restaurants.id');
    table.timestamps(false);
    table.float('rating').notNullable();
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('dinning_records')
}

