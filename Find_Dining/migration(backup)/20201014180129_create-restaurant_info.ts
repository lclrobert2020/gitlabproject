import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('dinning_categories')) {
    return
  }
  await knex.schema.createTable('dinning_categories', table => {
    table.increments();
    table.string('category', 255).notNullable();
  })

  if (await knex.schema.hasTable('price_ranges')) {
    return
  }
  await knex.schema.createTable('price_ranges', table => {
    table.increments();
    table.string('range', 255).notNullable();
  })

  if (await knex.schema.hasTable('restaurant_districts')) {
    return
  }
  await knex.schema.createTable('restaurant_districts', table => {
    table.increments();
    table.string('district', 255).notNullable();
  })
  
  if (await knex.schema.hasTable('restaurants')) {
    return
  }
  await knex.schema.createTable('restaurants', table => {
    table.increments();
    table.string('name', 255).notNullable();
    table.string('address', 255).notNullable();
    table.string('link');
    table.integer('smile');
    table.integer('cry');
    table.integer('reviews');
    table.float('rating');
    table.integer('dinning_category_1_id').unsigned().notNullable().references('dinning_categories.id');
    table.integer('dinning_category_2_id').unsigned().notNullable().references('dinning_categories.id');
    table.integer('price_range_id').unsigned().notNullable().references('price_ranges.id');
    table.integer('restaurant_district_id').unsigned().notNullable().references('restaurant_districts.id');
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.
    dropTableIfExists('restaurants').
    dropTableIfExists('restaurant_districts').
    dropTableIfExists('price_ranges').
    dropTableIfExists('dinning_categories')
}

