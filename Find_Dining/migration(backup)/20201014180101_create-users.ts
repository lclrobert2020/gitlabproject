import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('users')) {
    return
  }
  await knex.schema.createTable('users', table => {
    table.increments();
    table.string('username', 255).notNullable();
    table.string('password', 60).notNullable();
    table.string('email').unique().notNullable();
    table.string('update_email').unique();
    table.string('google_id')
    table.timestamps(false);
    table.string('activation_code');
    table.string('profile_picture');
    table.boolean('is_active');

  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('users');
}
