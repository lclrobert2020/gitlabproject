#%%
import requests
from bs4 import BeautifulSoup
import csv

file = open('./restaurants.csv','w')
writer = csv.writer(file)

writer.writerow(['name', 'district', 'location', 'price', 'category', 'reviews', 'smiles', 'cry', 'rating', 'link', 'photo'])

address = [
    '/district/tsim-sha-tsui?categoryGroupId=10008&page=',  #Asian
    '?cuisineId=1004&districtId=2008&page=',                #Hong Kong Style
    '?cuisineId=4000&districtId=2008&page=',                #Western
    '/district/tsim-sha-tsui?categoryGroupId=10002&page='   #Chinese
]
y = 0   #address index
x = 1   #page
start = True
while start:
    # URL = f'https://www.openrice.com/zh/hongkong/restaurants?page={x}'
    URL = f'https://www.openrice.com/en/hongkong/restaurants{address[y]}{x}'
    headers = {"User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36'}
    x += 1
    print(URL)
    page = requests.get(URL, headers=headers)
    soup = BeautifulSoup(page.content, 'html.parser')
    url = soup.find("meta",  property="og:url")
    print(url["content"] == URL)
    if url["content"] == URL:
        cards = soup.find_all(class_='content-cell-wrapper')
        names = soup.find_all(class_='title-name')
        locations = soup.find_all(class_='address') 
        priceRanges = soup.find_all(class_='icon-info-food-price')
        categories = soup.find_all(class_='pois-categoryui-list')
        allReviews = soup.find_all(class_='counters-container')
        allSmiles = soup.find_all(class_='smile-face')
        allCry = soup.find_all(class_='sad-face')
        links = soup.find_all(class_='title-name')
        photos = soup.find_all(class_='door-photo')
        # if soup.find(class_='pois-restaurant-list-cell-sponsored'):
            # allReviews.insert(0,0)
            # allSmiles.insert(0,0)
            # allCry.insert(0,0)
        for z in range(len(cards)):
            if cards[z].find(class_='pois-restaurant-list-cell-content-right-info-rating') == None:
                allSmiles.insert(z, BeautifulSoup('<div><span>0</span></div>', 'html.parser'))
                allCry.insert(z, BeautifulSoup('<div><span>0</span></div>', 'html.parser'))
                # print(z)

            if cards[z].find(class_='counters-container') == None:
                allReviews.insert(z, BeautifulSoup('<span>0</span>', 'html.parser'))
            if cards[z].find(class_='door-photo') == None:
                photos.insert(z, 0)
        # print(allSmiles[1])
        i = 0
        while i < len(names):  
            name = names[i].findChild().text
            district = locations[i].a.text
            locations[i].span.a.decompose() #remove a tag
            location = locations[i].text.strip()
            priceRange = priceRanges[i].span.text.replace('$', '')
            cat = categories[i].text.strip().split('\n')
            listOfCat = []
            for v in range(len(listOfCat)):
                for b in range(len(cat)):
                    if listOfCat[v] != cat[b]:
                        listOfCat.append(cat[b])

            category = '|'.join(cat)
            # if soup.find(class_='pois-restaurant-list-cell-sponsored') and i == 0:
            #     reviews = 0
            #     smiles = 0
            #     cry = 0
            # else:
            reviews = allReviews[i].text.strip().replace('Reviews','').replace('(','').replace(')','').replace(' ','')
            smiles = allSmiles[i].span.text.strip()
            cry = allCry[i].span.text.strip()
            link = links[i].find('a', href=True)['href']
            if photos[i] == 0:
                photo = "None"
            else:
                photo = photos[i]['style'].replace("background-image: url('", "").replace("');", "")
            print(photo)

            if (int(smiles) == 0 and int(cry) == 0):
                rating = 0
            else:
                rating = round(((int(smiles)/(int(smiles)+int(cry)))*5),2)


            writer.writerow([name, district, location, priceRange, category, reviews, smiles, cry, rating, link, photo])
            i += 1
    else:
        if y == (len(address) - 1): 
            start = False
        x = 1
        y += 1

file.close()

#%%
#To clear duplicate data
import pandas as pd

df = pd.read_csv('restaurants.csv')

print(df[~df.duplicated(subset=['location'])])

df[~df.duplicated(subset=['location'])].to_csv('restaurants(clean).csv', index=False)


# dfNew = pd.read_csv('restaurants(clean).csv')
# dfRename = dfNew.rename(columns={'': 'id'})
# print(dfRename)
# dfRename.to_csv('restaurants(clean).csv', index=False)




# %%
import csv
import json

csvfile = open('restaurants(clean).csv', 'r')
jsonfile = open('restaurants.json', 'w')

reader = csv.DictReader(csvfile)
out = json.dumps( [ row for row in reader ],indent=4 )
jsonfile.write(out)
print("done")
# %%
