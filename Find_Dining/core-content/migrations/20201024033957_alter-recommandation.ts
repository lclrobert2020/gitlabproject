import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('recommendation')) {
    await knex.schema.alterTable('recommendation', table => {
      table.float('scores');
    })
  }
  
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.table('recommendation', table => {
    table.dropColumn('scores')
  })
    
}

