import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('users')) {
        return
    }
    await knex.schema.createTable('users', table => {
        table.increments();
        table.string('username', 255).notNullable();
        table.string('password', 60).notNullable();
        table.string('email').unique().notNullable();
        table.string('update_email').unique();
        table.string('google_id')
        table.timestamps(false);
        table.string('activation_code');
        table.string('profile_picture');
        table.boolean('is_active');
    })

    if (await knex.schema.hasTable('restaurants')) {
        return
    }
    await knex.schema.createTable('restaurants', table => {
        table.increments();
        table.string('name', 255).notNullable();
        table.string('district', 255).notNullable();
        table.string('address', 255).notNullable();
        table.string('price', 255).notNullable();
        table.string('category', 255).notNullable();
        table.integer('reviews');
        table.integer('smile');
        table.integer('cry');
        table.float('rating');
        table.string('link', 255).notNullable();
        table.string('photo', 255)
    })

    if (await knex.schema.hasTable('dinning_records')) {
        return
    }
    await knex.schema.createTable('dinning_records', table => {
        table.increments();
        table.integer('user_id').unsigned().notNullable().references('users.id');
        table.integer('restaurant_id').unsigned().notNullable().references('restaurants.id');
        table.integer('choice').notNullable();
        table.timestamps(false);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('users')
    await knex.schema.dropTableIfExists('restaurants')
    await knex.schema.dropTableIfExists('dining_records')
}

