import Knex from 'knex';
// import * as cp from "child_process"
// let spawn = cp.spawnSync
class SearchRestaurantService {
  constructor(public knex: Knex) { }

  async loadRestaurantByAreas(area: string, user_id: number) {
      const restaurants = await this.knex
        .select('*')
        .from('restaurants')
        .join('recommendation', { 'restaurants.id': 'recommendation.shop_id' })
        .where('user_id', user_id)
        .where('restaurants.district', area)
        .orderBy('is_recommended', 'asc')
        .returning('*')
      return restaurants
    }

    async loadRestaurantByItsID(rest_ids: number[], area: string) {
      const restaurants = await this.knex
      .select('*')
      .from('restaurants')
      .join('recommendation', { 'restaurants.id': 'recommendation.shop_id' })
      .whereIn('restaurants.id', rest_ids)
      .where('restaurants.district', area)
      .orderBy('recommendation.is_recommended', 'asc')
      .returning('*')
      return restaurants
    }

    // async loadRestaurantByID(area: string, id: number) {
    //   let userRating: any[] = await this.knex
    //     .select('*')
    //     .from('dinning_records')
    //     .where('user_id', id)
  
    //   console.log(userRating.length)
    //   if (userRating.length > 10) {
    //     let process1 = spawn('python', [
    //       "./AI/testmodel.py",
    //       String(id)
    //     ],
    //       { encoding: 'utf-8' })
    //     let restaurants: any[] = []
    //     let results = JSON.parse(process1.stdout)
    //     let ids: any[] = []
    //     for (let result of results) {
    //       ids.push(result)
    //     }
    //     restaurants = await this.knex
    //     .select('*')
    //     .from('restaurants')
    //     .whereIn('id', ids)
        
    //     .returning('*')

    //     return restaurants
    //   } else {
    //     const restaurants = await this.knex
    //       .select('*')
    //       .from('restaurants')
    //       .join('recommendation', { 'restaurants.id': 'recommendation.shop_id' })
    //       .where('user_id', id)
    //       .where('restaurants.district', area)
    //       .orderBy('is_recommended', 'asc')
    //       .returning('*')
    //     return restaurants
    //   }

  //   // process1.stdout.on('data', async (data) => {
  //   //   data.toString('utf8')
  //   //   console.log(data.toString('utf8'));
  //   // })
  //   // } else {
  //   //   const restaurants = await this.knex
  //   //     .select('*')
  //   //     .from('restaurants')
  //   //     .join('recommendation', { 'restaurants.id': 'recommendation.shop_id' })
  //   //     .where('user_id', id)
  //   //     .where('restaurants.district', area)
  //   //     .orderBy('is_recommended', 'asc')
  //   //     .returning('*')
  //   //   return restaurants
  //   // }

  
}

export default SearchRestaurantService