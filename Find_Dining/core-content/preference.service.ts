import Knex from 'knex';

class PreferenceService {
  constructor(public knex: Knex) {}

  async saveUserPreference({ user_id: id, preference: json }) {
    const preference = await this.knex
      .insert({
        user_id: id,
        preference: json,
        created_at: this.knex.fn.now(),
        updated_at: this.knex.fn.now()
      })
      .into('preferences')
      .returning('*')
    return preference
  }

  async loadUserPreference(id: number) {
    console.log(id);
    // const preference = await this.knex.select('*').from('preferences').where('user_id', id).returning('*')
    const preference = await this.knex
      .select('*')
      .from('preferences')
      .where('user_id', id)
      .returning('*')
    console.log(preference)
    return preference
  }
  
  async updateUserPreference({ user_id: id, preference: json }) {
    const newPreference = await this.knex('preferences')
      .update({ 
        preference: json,
        updated_at: this.knex.fn.now()
      })
      .where('user_id', id)
    return newPreference
  }
}

export default PreferenceService