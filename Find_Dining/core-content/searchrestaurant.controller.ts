// import fetch from 'node-fetch';
import SearchRestaurantService from './searchrestaurant.service';
import DiningRecordService from './diningrecord.service';
import express from 'express'
import * as cp from "child_process"
let spawn = cp.spawnSync

class SearchRestaurantController {
  constructor(
    public searchRestaurantService: SearchRestaurantService,
    public diningRecordService: DiningRecordService) { }

  loadRecommendedRestaurants = async (req: express.Request, res: express.Response) => {
    try {
      let userRating = await this.diningRecordService.loadRecordByUserID(req.session?.user.id)
      console.log(userRating.length)

      let areas = req.body.location
      let tableList = {}

      for (let area of areas) {
        if (userRating.length > 10) {
          console.log('Restaurants commended in AI')
          let process1 = spawn('python', [
            "./AI/testmodel.py",
            String(req.session?.user.id)
          ],
            { encoding: 'utf-8' })
          console.log('Python loading in AI finished:', process1)
          let restaurantIDs = JSON.parse(process1.stdout)
          console.log('Restaurant info translated')

          let restaurantIDList: any[] = []
          for (let restaurantID of restaurantIDs) {
            restaurantIDList.push(restaurantID)
          }

          let tableAI = await this.searchRestaurantService.loadRestaurantByItsID(restaurantIDList, area)
          if (tableAI.length) { tableList[`${area}`] = tableAI }
        } else {
          console.log('Restaurants commended in filtering')
          let table = await this.searchRestaurantService.loadRestaurantByAreas(area, req.session?.user.id)
          if (table.length) { tableList[`${area}`] = table }
        }
      }

      // console.log(tableList)

      if (req.session) { req.session.restaurants = tableList }
      res.json({
        url: '/recommendrestaurant_private.html',
        list: tableList
      })
    } catch (error) {
      res.status(404)
    }
  }

  getRestaurantListFromSession = async (req: express.Request, res: express.Response) => {
    try {
      if (req.session?.restaurants) {
        res.json({
          success: true,
          list: req.session?.restaurants
        })
      } else {
        res.json({ success: false })
      }
    } catch {
      res.status(404)
    }
  }
}

export default SearchRestaurantController