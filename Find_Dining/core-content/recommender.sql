INSERT INTO recommendation (user_id, shop_id, is_recommended) VALUES
('1', 30, true),
('1', 17, true),
('1', 33, true);

INSERT INTO restaurants (name, district, address, price, category, reviews, smile, cry, rating, link, photo) VALUES
('Shoken 初見', 'Tsuen Wan',
'Shop F, G/F, Wah Tat Building, 22 Hoi Pa Street, Tsuen Wan',
'101-200', 'Japanese/Hot Pot',
586, 510, 9, 4.91,
'en/hongkong/r-shoken-tsuen-wan-japanese-hot-pot-r554516',
'https://static7.orstatic.com/userphoto2/photo/1B/11V7/07H9YUC297A027CB4FD484tx.jpg');

SELECT * FROM restaurants INNER JOIN recommendation ON restaurants.id = shop_id WHERE restaurants.district = 'Tsim Sha Tsui';

INSERT INTO recommendation (user_id, shop_id, is_recommended) VALUES 
('1', 417, false),
('1', 300, false),
('1', 417, false),
('1', 825, false),
('1', 96, false);
