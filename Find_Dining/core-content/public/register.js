let password = document.querySelector("#passwordField");
let confirm_password = document.querySelector("#passwordRepeatField");

function validatePassword() {
  if (password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
    document.querySelector("#submit").click();
  } else {
    confirm_password.setCustomValidity("");
  }
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

window.onload = function checkLoginError() {
  let searchURL = window.location.search;
  const urlParams = new URLSearchParams(searchURL);
  if (urlParams.get("usernameFieldError")) {
    document.querySelector("#usernameFieldError").innerHTML = urlParams.get(
      "usernameFieldError"
    );
  }
  if (urlParams.get("emailFieldError")) {
    document.querySelector("#emailFieldError").innerHTML = urlParams.get(
      "emailFieldError"
    );
  }
  if (urlParams.get("passwordRepeatField")) {
    document.querySelector(
      "#passwordRepeatFieldError"
    ).innerHTML = urlParams.get("passwordRepeatField");
  }
  if (urlParams.get("passwordFieldError")) {
    document.querySelector("#passwordFieldError").innerHTML = urlParams.get(
      "passwordFieldError"
    );
  }
};

//Limit price range checkbox not more than 1
let buttons = document.querySelectorAll(".price-buttons");
let maxChecked = 1;
for (let i = 0; i < buttons.length; i++) {
  buttons[i].onclick = selectiveCheck;
}
function selectiveCheck() {
  let checkedChecks = document.querySelectorAll(".price-buttons:checked");
  if (checkedChecks.length >= maxChecked + 1) {
    return false;
  }
}

let submit = document.querySelector("#registerForm");
submit.addEventListener("submit", async (event) => {
  event.preventDefault();
  let activeDishes = document.querySelectorAll(".buttons:checked");
  let activePrice = document.querySelector(".price-buttons:checked");

  let dishList = [];
  for (let dish of activeDishes) {
    dishList.push(dish.value);
  }
  //   console.log(JSON.stringify({ dishList }));
  if (activeDishes.length) {
    let res = await fetch("/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({
        email: submit.emailField.value,
        username: submit.usernameField.value,
        password: submit.passwordField.value,
        passwordRepeat: submit.passwordRepeatField.value,
        dishList: dishList,
        price: activePrice.value,
      }),
    });

    if (res.url) {
      location.href = res.url;
    }
    //   } else {
    //     document.querySelector(
    //       ".message"
    //     ).innerHTML = `<p>Please choose the cuisine before search.</p>`;
  }
});

// document.onreadystatechange = function () {
//   var state = document.readyState;
//   if (state == "interactive") {
//     document.getElementById("submit").value.visibility = "hidden";
//   } else if (state == "complete") {
//     setTimeout(function () {
//       document.getElementById("interactive");
//       document.getElementById("load").style.visibility = "hidden";
//       document.getElementById("contents").style.visibility = "visible";
//     }, 1000);
//   }
// };
