let password = document.querySelector("#passwordField");
let confirm_password = document.querySelector("#passwordRepeatField");

function validatePassword() {
  if (password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
    document.querySelector("#submit").click();
  } else {
    confirm_password.setCustomValidity("");
  }
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

//Limit price range checkbox not more than 1
let buttons = document.querySelectorAll(".price-buttons");
let maxChecked = 1;
for (let i = 0; i < buttons.length; i++) {
  buttons[i].onclick = selectiveCheck;
}
function selectiveCheck() {
  let checkedChecks = document.querySelectorAll(".price-buttons:checked");
  if (checkedChecks.length >= maxChecked + 1) {
    return false;
  }
}

let submit = document.querySelector("#editForm");
submit.addEventListener("submit", async function (e) {
  e.preventDefault();
  document.querySelector("#passwordRepeatFieldError").innerHTML = ``;

  let activeDishes = document.querySelectorAll(".buttons:checked");
  let activePrice = document.querySelector(".price-buttons:checked");

  let dishList = [];
  for (let dish of activeDishes) {
    dishList.push(dish.value);
  }

  const form = this;
  const formObject = {};
  formObject["username"] = form.usernameField.value;
  formObject["password"] = form.passwordField.value;
  formObject["passwordRepeat"] = form.passwordField.value;
  formObject["oldPassword"] = form.oldPasswordField.value;
  formObject["dishList"] = dishList;
  formObject["price"] = activePrice.value

  let checker = false;

  console.log(formObject);
  for (let value in formObject) {
    if (formObject[value]) {
      checker = true;
      break;
    }
  }
  if (!formObject["dishList"] && !formObject["price"]) {
    checker = false
    document.querySelector(
        "#passwordRepeatFieldError"
      ).innerHTML = `Please fill your preference of price range and cuisine style`; 
} else {
    checker = true
}

  if (!checker) {
    document.querySelector(
      "#passwordRepeatFieldError"
    ).innerHTML = `Please fill in one row for submit`;
    return;
  }

  const res = await fetch("/editform", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(formObject),
  });

  const result = await res.json();
  console.log(result);
  if (result.redirect) {
    window.location.href = "/?verify=Password changed, Please login again";
  }
  if (!result.success) {
    if (result.oldPasswordError) {
      document.querySelector("#oldPasswordFieldError").innerHTML =
        result.oldPasswordError;
    }
    if (result.usernameError) {
      document.querySelector("#usernameFieldError").innerHTML =
        result.usernameError;
    }
    if (result.passwordRepeatError) {
      document.querySelector("#passwordRepeatFieldError").innerHTML =
        result.passwordRepeatError;
    }
    if (result.passwordError) {
      document.querySelector("#passwordFieldError").innerHTML =
        result.passwordError;
    }
  }
  loadUser();

  if(result.url) {
    location.href = result.url;
  }
});

async function loadUser() {
  const res = await fetch("/personalinfo", {
    method: "get",
  });
  const result = await res.json();
  console.log(result);
  if (result.username) {
    document.querySelector("#usernamespan").innerHTML = `${result.username}`;
    document.querySelector("#emailspan").innerHTML = `${result.email}`;
    
    if (result.picture) {
      document.querySelector(
        "#usernameDiv"
      ).innerHTML = `<img src="${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`;
    } else {
      document.querySelector(
        "#usernameDiv"
      ).innerHTML = `<div alt="userpicture" height="30px" width="30px"> ${result.username}</div>`;
    }
  }

  console.log(result);
}

loadUser();
