let counter = 0;
let list;
let board;
let carousel;
let sublist;

async function getData() {
  const res = await fetch("/recommendrestaurant/");
  const fullList = await res.json();
  // console.log(fullList)
  return fullList.list;
}

async function init() {
  sublist = await getData();
  list = [];
  for (let key in sublist) {
    list = list.concat(sublist[key]);
  }
  //    console.log(list.length)
  board = document.querySelector("#board");
  carousel = new Carousel(board);
}

init();

class Carousel {
  constructor(element) {
    this.board = element;
    // add first two cards programmatically
    this.push();
    this.push();
    this.push();
    this.push();
    // handle gestures
    this.handle();
  }

  handle() {
    // list all cards
    this.cards = this.board.querySelectorAll(".card");
    // get top card
    this.topCard = this.cards[this.cards.length - 1];
    // get next card
    this.nextCard = this.cards[this.cards.length - 2];

    // if at least one card is present
    if (this.cards.length > 0) {
      // set default top card position and scale
      this.topCard.style.transform =
        "translateX(-50%) translateY(-50%) rotate(0deg) rotateY(0deg) scale(1)";
      // destroy previous Hammer instance, if present
      if (this.hammer) this.hammer.destroy();

      // listen for tap and pan gestures on top card
      this.hammer = new Hammer(this.topCard);
      this.hammer.add(new Hammer.Tap());
      this.hammer.add(
        new Hammer.Pan({
          position: Hammer.position_ALL,
          threshold: 0,
        })
      );

      // pass events data to custom callbacks
      this.hammer.on("tap", (e) => {
        this.onTap(e);
      });
      this.hammer.on("pan", (e) => {
        this.onPan(e);
      });
    }
  }

  onTap(e) {
    // get finger position on top card
    let propX =
      (e.center.x - e.target.getBoundingClientRect().left) /
      e.target.clientWidth;
    // get rotation degrees around Y axis (+/- 15) based on finger position
    let rotateY = 15 * (propX < 0.05 ? -1 : 1);

    // enable transform transition
    this.topCard.style.transition = "transform 100ms ease-out";
    // apply rotation around Y axis
    this.topCard.style.transform =
      "translateX(-50%) translateY(-50%) rotate(0deg) rotateY(" +
      rotateY +
      "deg) scale(1)";

    // wait for transition end
    setTimeout(() => {
      // reset transform properties
      this.topCard.style.transform =
        "translateX(-50%) translateY(-50%) rotate(0deg) rotateY(0deg) scale(1)";
    }, 100);
  }

  onPan(e) {
    if (!this.isPanning) {
      this.isPanning = true;
      // remove transition properties
      this.topCard.style.transition = null;
      if (this.nextCard) this.nextCard.style.transition = null;

      // get top card coordinates in pixels
      let style = window.getComputedStyle(this.topCard);
      let mx = style.transform.match(/^matrix\((.+)\)$/);
      this.startPosX = mx ? parseFloat(mx[1].split(", ")[4]) : 0;
      this.startPosY = mx ? parseFloat(mx[1].split(", ")[5]) : 0;

      // get top card bounds
      let bounds = this.topCard.getBoundingClientRect();
      // get finger position on top card, top (1) or bottom (-1)
      this.isDraggingFrom =
        e.center.y - bounds.top > this.topCard.clientHeight / 2 ? -1 : 1;
    }

    // get new coordinates
    let posX = e.deltaX + this.startPosX;
    let posY = e.deltaY + this.startPosY;

    // get ratio between swiped pixels and the axes
    let propX = e.deltaX / this.board.clientWidth;
    let propY = e.deltaY / this.board.clientHeight;

    // get swipe direction, left (-1) or right (1)
    let dirX = e.deltaX < 0 ? -1 : 1;

    // get degrees of rotation, between 0 and +/- 45
    let deg = this.isDraggingFrom * dirX * Math.abs(propX) * 45;

    // get scale ratio, between .95 and 1
    let scale = (95 + 5 * Math.abs(propX)) / 100;

    // move and rotate top card
    this.topCard.style.transform =
      "translateX(" +
      posX +
      "px) translateY(" +
      posY +
      "px) rotate(" +
      deg +
      "deg) rotateY(0deg) scale(1)";

    // scale up next card
    if (this.nextCard)
      this.nextCard.style.transform =
        "translateX(-50%) translateY(-50%) rotate(0deg) rotateY(0deg) scale(" +
        scale +
        ")";

    if (e.isFinal) {
      this.isPanning = false;
      let successful = false;
      let obj;
      console.log(propX, propY, e.direction);
      // set back transition properties
      this.topCard.style.transition = "transform 200ms ease-out";
      if (this.nextCard)
        this.nextCard.style.transition = "transform 100ms linear";
      // check threshold and movement direction
      if (propX > 0.25) {
        successful = true;
        obj = { choice: 0, shop_id: this.topCard.getAttribute("shop_id") };
        // get right border position
        posX = this.board.clientWidth;
      } else if (propX < -0.25) {
        successful = true;
        // get left border position
        obj = { choice: 1, shop_id: this.topCard.getAttribute("shop_id") };
        posX = -(this.board.clientWidth + this.topCard.clientWidth);
      } else if (propY < -0.25) {
        successful = true;
        // get top border position
        posY = -(this.board.clientHeight + this.topCard.clientHeight);
      }

      if (successful) {
        // throw card in the chosen direction
        this.topCard.style.transform =
          "translateX(" +
          posX +
          "px) translateY(" +
          posY +
          "px) rotate(" +
          deg +
          "deg)";
        if (obj) {
          postData(obj);
        }
        // wait transition end
        setTimeout(() => {
          // remove swiped card
          this.board.removeChild(this.topCard);
          // add new card
          this.push();
          // handle gestures on new top card
          this.handle();
        }, 200);
      } else {
        // reset cards position and size
        this.topCard.style.transform =
          "translateX(-50%) translateY(-50%) rotate(0deg) rotateY(0deg) scale(1)";
        if (this.nextCard)
          this.nextCard.style.transform =
            "translateX(-50%) translateY(-50%) rotate(0deg) rotateY(0deg) scale(0.95)";
      }
    }
  }

  push() {
    let switchCard = (x, y) => {
      let style = window.getComputedStyle(this.topCard);
      let mx = style.transform.match(/^matrix\((.+)\)$/);
      let startPosX = mx ? parseFloat(mx[1].split(", ")[4]) : 0;
      let startPosY = mx ? parseFloat(mx[1].split(", ")[5]) : 0;
      let posX = startPosX;
      let posY = startPosY;
      let deg = y;
      if (x == `right`) {
        posX = -(this.board.clientWidth + this.topCard.clientWidth);
      }
      if (x == `left`) {
        posX = this.board.clientWidth;
      }
      if (x == `up`) {
        posY = -(this.board.clientHeight + this.topCard.clientHeight);
      }

      this.topCard.style.transform =
        "translateX(" +
        posX +
        "px) translateY(" +
        posY +
        "px) rotate(" +
        deg +
        "deg)";
      this.topCard.style.transition = "transform 200ms ease-out";
      // wait transition end
      setTimeout(() => {
        // remove swiped card
        this.board.removeChild(this.topCard);
        // add new card
        this.push();
        // handle gestures on new top card
        this.handle();
      }, 200);
    };

    if (counter >= list.length) {
      counter = 0;
    }
    let data = list[counter];
    let card = document.createElement("div");
    let link = "https://www.openrice.com/" + data.link;
    card.classList.add("card");
    card.style.backgroundImage = `url(${data.photo})`;
    card.setAttribute("shop_id", data.shop_id);
    card.innerHTML = `
            <div class="restinfo">
                <div class="restname">${data.name}</div>
                <div class="resttype">${data.category}</div>
            </div>
            <button class="infobtn btn"><i class="fas fa-info-circle"></i></button>
            <div class="cardbtndiv">
                <div class="cardbtn">
                <button type="button" class="dislikebtn btn-danger" shop_id=${data.shop_id}><i class="fas fa-lg fa-times"></i><div>Unlike<div></button>
                <button type="button" class="skipbtn btn-primary" shop_id=${data.shop_id}><i class="fas fa-lg fa-undo-alt"></i><div>Skip</div></button>
                <button type="button" class="likebtn btn-success" shop_id=${data.shop_id}><i class="fas fa-lg fa-thumbs-up"></i><div>Like</div></button>
                </div>
            </div>
            <div class="moreinfocard container">
              <div><label class="restname col-sm-2">Name: </label><span class="col-sm-10">${data.name}</span></div>
              <div><label class="resttype col-sm-2">Style: </label><span class="col-sm-10">${data.category}</span></div>
              <div><label class="restlocation col-sm-2">Location: </label><span class="col-sm-10">${data.address}</span></div>
              <div><label class="price col-sm-2">Price: </label><span class="col-sm-10">${data.price}</span></div>
              <div><label class="rating col-sm-2">Rating: </label><span class="col-sm-10">${data.rating}<span>/5</span></span></div>
              <div class="restRate">
                <div><image class="smile-face" src="./like.svg"> Smile: ${data.smile} </div>
                <div><image class="cry-face" src="./dislike.svg"> Cry: ${data.cry} </div>
              </div>
              <div class="formOpenrice">
                <form action=${link}>
                  <input type="submit" value="More information" />
                </form>
              </div>
            </div>
            `;
    card.querySelector(`.infobtn`).addEventListener(`click`, (e) => {
      card = e.currentTarget.parentNode.querySelector(`.moreinfocard`);
      if (
        !card.classList.contains(`notshow`) &&
        !card.classList.contains(`show`)
      ) {
        card.classList.add(`show`);
      } else if (card.classList.contains(`notshow`)) {
        card.classList.remove(`notshow`);
        card.classList.add(`show`);
      } else {
        card.classList.remove(`show`);
        card.classList.add(`notshow`);
      }
    });
    card.querySelector(`.dislikebtn`).addEventListener(`click`, (e) => {
      switchCard(`right`, 45);
      let obj = { choice: 0, shop_id: data.shop_id };
      postData(obj);
    });
    card.querySelector(`.likebtn`).addEventListener(`click`, (e) => {
      switchCard(`left`, -45);
      let obj = { choice: 1, shop_id: data.shop_id };
      postData(obj);
    });
    card.querySelector(`.skipbtn`).addEventListener(`click`, (e) => {
      switchCard(`up`, 0);
    });
    counter = counter + 1;
    this.board.insertBefore(card, this.board.firstChild);
  }
}

async function postData(obj) {
  fetch("/postdata", {
    method: "POST",
    // headers 加入 json 格式
    headers: {
      "Content-Type": "application/json",
    },
    // body 將 json 轉字串送出
    body: JSON.stringify(obj),
  });
}

async function loadUser() {
  const res = await fetch("/personalinfo", {
    method: "get",
  });
  const result = await res.json();
  if (result.username) {
    if (result.picture) {
      document.querySelector(
        "#usernameDiv"
      ).innerHTML = `<img src="${result.picture}" alt="userpicture"  height="30px" width="30px"> ${result.username}`;
    } else {
      document.querySelector(
        "#usernameDiv"
      ).innerHTML = `<div alt="userpicture" height="30px" width="30px"> ${result.username}</div>`;
    }
  }

  console.log(result);
}

loadUser();
