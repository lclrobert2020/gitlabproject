import Knex from 'knex'

class RecommendationService {
  constructor(public knex: Knex) {}

  async saveEachUserRecommendation(user_id, restaurant_id) {
    const recommendList = await this.knex
      .insert({
        user_id: user_id,
        shop_id: restaurant_id,
        is_recommended: false,
        created_at: this.knex.fn.now(),
        updated_at: this.knex.fn.now()
      }).into("recommendation")
    return recommendList
  }

  async updateRecommendRestaurantStatus(id, shop_id) {
    const updatedStatus = await this.knex('recommendation')
      .update({
        is_recommended: true,
        updated_at: this.knex.fn.now()
      })
      .where({
        user_id: id,
        shop_id: shop_id
      })
      .returning('user_id')
      return updatedStatus
  }
}

export default RecommendationService;