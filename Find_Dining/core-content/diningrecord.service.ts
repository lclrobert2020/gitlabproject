import Knex from 'knex';

class DiningRecordService {
  constructor(public knex: Knex) {}

  async saveDiningRecord(id, shop_id, choice) {
    const newRecord = await this.knex
      .insert({
        user_id: id,
        restaurant_id: shop_id,
        choice: choice,
        created_at: this.knex.fn.now(),
        updated_at: this.knex.fn.now()
      })
      .into('dinning_records')
      .returning('id')
      return newRecord
  }

  async loadRecordByUserID(id: number) {
    let userRating: any[] = await this.knex
      .select('*')
      .from('dinning_records')
      .where('user_id', id)
    return userRating
    } 
}

export default DiningRecordService
