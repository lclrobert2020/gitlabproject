import SearchRestaurantService from './searchrestaurant.service';
import SearchRestaurantController from './searchrestaurant.controller';
import DiningRecordService from './diningrecord.service';
// import UserService from './user.service';
import express from 'express';
import { User } from './models';
import Knex = require('knex');
const knexConfig = require('./knexfile')


describe('Search Rerestaurant Test Suit', () => {
  let knex: Knex
  let searchRestaurantService: SearchRestaurantService;
  let searchRestaurantController: SearchRestaurantController;
  let diningRecordService: DiningRecordService;
  // let userService: UserService;
  let req: express.Request;
  let res: express.Response
  let initUser: User = {
    id: 1,
    username: 'william',
    email: 'william@gmail.com',
    password: '123456',
    is_active: true
  }

  beforeAll(async () => {
    knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);
    await knex('recommendation').del();
    await knex('users').del();
    await knex('restaurants').del();
    await knex('users')
      .insert(initUser)
    await knex('restaurants')
      .insert({
        id: 1,
        name: 'ABC restaurant',
        link: 'http://www.abc-restaurant.com',
        address: '123 ABC St',
        district: 'TST',
        smile: 300,
        cry: 10,
        reviews: 500,
        rating: 4.98,
        photo: 'abc.jpg',
        price: '101-200',
        category: "Japanese|Sushi/Sashimi"
      })
    await knex('recommendation')
      .insert({
        id: 1,
        user_id: 1,
        shop_id: 1,
        is_recommended: false
      })
    // userService = new UserService({} as Knex)
  });

  afterAll(() => {
    knex.destroy();
  });

  beforeEach(() => {
    searchRestaurantService = new SearchRestaurantService({} as Knex)
    diningRecordService = new DiningRecordService({} as Knex)

    jest
      .spyOn(searchRestaurantService, 'loadRestaurantByAreas')
      .mockImplementation(async () => [{
        id: 1,
        name: 'ABC restaurant',
        link: 'http://www.abc-restaurant.com',
        address: '123 ABC St',
        district: 'TST',
        smile: 300,
        cry: 10,
        reviews: 500,
        rating: 4.98,
        photo: 'abc.jpg',
        category: "Japanese|Sushi/Sashimi"
      }])

    jest
      .spyOn(searchRestaurantService, 'loadRestaurantByItsID')
      .mockImplementation(async () => [{
        id: 1,
        name: 'ABC restaurant',
        link: 'http://www.abc-restaurant.com',
        address: '123 ABC St',
        district: 'TST',
        smile: 300,
        cry: 10,
        reviews: 500,
        rating: 4.98,
        photo: 'abc.jpg',
        category: "Japanese|Sushi/Sashimi"
      }])

    jest
      .spyOn(diningRecordService, 'loadRecordByUserID')
      .mockImplementation(async () => [{
        user_id: 1,
      }])

    console.log = jest.fn()

    searchRestaurantController = new SearchRestaurantController(searchRestaurantService, diningRecordService)

    req = {} as express.Request;
    res = {
      json: jest.fn(),
      redirect: jest.fn(),
      status: jest.fn(() => {
        return { json: (data: any) => { console.log(data) } }
      })
    } as any;
  })

  it('should load a restaurant', async () => {
    req.body = {
      location: ['TST']
    }
    if (req.session?.user) {
      req.session.user = {
        id: 1
      }
    }
    await searchRestaurantController.loadRecommendedRestaurants(req, res)
    expect(searchRestaurantService.loadRestaurantByItsID).not.toBeCalled()
    expect(searchRestaurantService.loadRestaurantByAreas).toBeCalled()
    expect(searchRestaurantService.loadRestaurantByAreas).toHaveReturned()
    expect(diningRecordService.loadRecordByUserID).toBeCalled()
    // expect(diningRecordService.loadRecordByUserID).toBeCalledWith({ name: 'ABC restaurant' })


  })
})