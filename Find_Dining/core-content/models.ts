export interface User {
  id?: number;
  username?: string;
  email?: string;
  password?: string;
  is_active?: boolean;
}

export interface UserOfGoogle {
  id?: number;
  username?: string;
  email?: string;
  password?: string;
  profile_picture?: string;
  google_id?: string;
  is_active?: boolean;
}