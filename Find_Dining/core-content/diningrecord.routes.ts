import DiningRecordController from './diningrecord.controller';
import express from 'express';

export function diningRecordRouter(diningRecordController: DiningRecordController) {
  let diningRecordRoutes = express.Router()
  diningRecordRoutes.post('/postdata', diningRecordController.post)

  return diningRecordRoutes
}