import Knex from 'knex';
import { User, UserOfGoogle } from './models';

class UserService {
  constructor(public knex: Knex) { }

  async loadUserByEmail({ email: email }: User) {
    const result = await this.knex
      .select('*')
      .from('users')
      .where('email', email)
      .returning('*')
    return result;
  }

  async loadUserByID({ id }: User) {
    const users = await this.knex
      .select('*')
      .from('users')
      .where('id', id)
    return users
  }

  async saveNewUser(user: User) {
    const newUser = await this.knex
      .insert({
        username: user.username,
        password: user.password,
        email: user.email,
        is_active: true,
        created_at: this.knex.fn.now(),
        updated_at: this.knex.fn.now()
      })
      .into('users')
      .returning('*')
    return newUser
  }

  async updateUsername({ username, id }: User){
    const newUsername = await this.knex('users')
      .update({ username: username })
      .where('id', id)
    return newUsername
  }

  async updateUserPassword({ password, id }: User){
    const newPassword = await this.knex('users')
      .update({ password: password })
      .where('id', id)
    return newPassword
  }


  // async saveEachUserRecommendation(user_id, restaurant_id) {
  //   const recommendList = await this.knex
  //     .insert({
  //       user_id: user_id,
  //       shop_id: restaurant_id,
  //       is_recommended: false,
  //       created_at: this.knex.fn.now(),
  //       updated_at: this.knex.fn.now()
  //     }).into("recommendation")
  //   return recommendList
  // }

  async saveNewUserFromGoogle(google: UserOfGoogle) {
    const newUser = await this.knex
      .insert({
        username: google.username,
        password: google.password,
        google_id: google.google_id,
        email: google.email,
        profile_picture: google.profile_picture,
        is_active: true,
        created_at: this.knex.fn.now(),
        updated_at: this.knex.fn.now()
      })
      .into('users')
      .returning('*')
    return newUser
  }

  // async saveUserPreference({ user_id: id, preference: json }) {
  //   const preference = await this.knex
  //     .insert({
  //       user_id: id,
  //       preference: json,
  //       created_at: this.knex.fn.now(),
  //       updated_at: this.knex.fn.now()
  //     })
  //     .into('preferences')
  //     .returning('*')
  //   return preference
  // }

  // async loadUserPreference(id) {
  //   const preference = await this.knex
  //     .select('*')
  //     .from('preferences')
  //     .where('user_id', id)
  //   return preference
  // }
  
  // async updateUserPreference({ user_id: id, preference: json }) {
  //   const newPreference = await this.knex('preferences')
  //     .update({ 
  //       preference: json,
  //       updated_at: this.knex.fn.now()
  //     })
  //     .where('user_id', id)
  //   return newPreference
  // }

  async updateGoogleLoginUser(google: UserOfGoogle) {
    return await this.knex('users')
      .update({ google_id: google.google_id })
      .where('email', google.email)
  }

  async resetPassword({ password, email }: User) {
    return await this.knex('users')
      .update({ password: password })
      .where('email', email)
  }

  // async updateEmail({email, update_email}: User) {
  //   if (update_email) {
  //     const updatedEmail = await this.knex('users')
  //       .update({ email: update_email })
  //       .update({ activation_code: null })
  //       .update({ update_email: null })
  //       .where('email', email)
  //       .returning('email')
  //     return updatedEmail;
  //   } else {
  //     const activatedEmail = await this.knex('users')
  //       .update({ is_active: true })
  //       .update({ activation_code: null })
  //       .where('email', email)
  //     return activatedEmail;
  //   }
  // }

  // async checkActivationCode({ activation_code }: User) {
  //   const result = await this.knex
  //     .select('*')
  //     .from('users')
  //     .where('activation_code', activation_code)
  //   return result;
  // }
}

export default UserService