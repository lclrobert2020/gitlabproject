#%%
import tensorflow as tf
from tensorflow import keras
import psycopg2
import os
from dotenv import load_dotenv
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
# from tensorflow.keras_preprocessing.text import Tokenizer_from_json
# import tf.keras.preprocessing.text.tokenizer_from_json
import sys 
import json
import pickle

load_dotenv()

con = psycopg2.connect(
  host = os.getenv('DB_HOST'),
  database = os.getenv('DB_NAME'),
  user = os.getenv('DB_USER'),
  password = os.getenv('DB_PASSWORD')
)

def convert(userPreference):
  init = []
  i = 0
  for row in userPreference: # each row is a list
    init.append(row)
    init[i].insert(0, str(row[0].replace(' ', '').replace('-', '')))
    init[i].pop(1)
    if len(row[1].split('|')) == 2:
      types = row[1].split('|')
      init[i].insert(1, types[0].replace('/', '').replace(' ', '').replace('-', '').replace('&', ''))
      init[i].insert(2, types[1].replace('/', '').replace(' ', '').replace('-', '').replace('&', ''))
      init[i].pop(3)
    else:
      types = row[1].split('|')
      init[i].insert(1, types[0].replace('/', '').replace(' ', '').replace('-', '').replace('&', ''))
      init[i].insert(2, '0')
      init[i].pop(3)
    i += 1
  space = " "
  results = []
#   print(len(init))
  for each in init:
    results.append(space.join(str(x) for x in each))
  return results
userid = int(sys.argv[1])
# userid = 30
model = keras.models.load_model(f"./AI/user{userid}.h5")
cur = con.cursor()
cur.execute(f"SELECT price, category, reviews, smile, cry, rating, restaurants.id FROM restaurants INNER JOIN recommendation ON restaurants.id = shop_id WHERE recommendation.user_id = {userid}")
recomendation = cur.fetchall()
# cur.execute("SELECT restaurants.id FROM restaurants INNER JOIN dinning_records ON restaurants.id = restaurant_id")
# restaurant_id = cur.fetchall()

allFeatures = []
allId = []
for each in recomendation:
  # print(list(each[:-1]))
  allFeatures.append(list(each[:-1]))
  allId.append(each[-1])

newlist = []
# for row in allFeatures:
#     newlist.append(list(row))
features = convert(allFeatures)


with open("./AI/tokenizer.pickle", "rb") as f:
  tokenizer = pickle.load(f)

tokenizer.fit_on_texts(features)
training = tokenizer.texts_to_sequences(features)
# print(tokenizer.sequences_to_texts(training[0]))
padded = pad_sequences(training)
array = np.array(padded)

predict_dataset = tf.convert_to_tensor(array)

results = model.predict(predict_dataset).tolist()
i = 0
restaurantlist = []
for result in results:
  restaurantlist.append(result)
  restaurantlist[i].insert(1, allId[i])
  i += 1

restaurantlist.sort(reverse=True)

output = []

for restaurant in restaurantlist:
  output.append(restaurant[1])

# array = map(tuple, output)

# print(array)
# tupleArray = tuple(output)
# cur.execute(f"SELECT * FROM restaurants WHERE id IN {tupleArray}")
# restaurants = cur.fetchall()
print(json.dumps(output))
# print(str(json.dumps(output)))
sys.stdout.flush()
con.close()
