# %%
import pandas as pd
from math import sqrt
import numpy as np
import json
import matplotlib.pyplot as plt
import random
import sys
import psycopg2
import os
from dotenv import load_dotenv
load_dotenv()

id = int(sys.argv[1])

# call database basic settings
con = psycopg2.connect(
    host=os.getenv('DB_HOST'),
    database=os.getenv('DB_NAME'),
    user=os.getenv('DB_USER'),
    password=os.getenv('DB_PASSWORD')
)

# call restaurant db and get data
curs = con.cursor()
curs.execute(f"SELECT * FROM restaurants;")
rows = curs.fetchall()
list1 = []
for row in rows:
    list1.append(row)
res_df = pd.DataFrame(np.array(list1),
                    columns=['restaurant_id', 'name', 'district', 'address', 'price', 'category', 'reviews', 'smiles', 'cry', 'rating', 'link', 'photo'])
res_df['category'] = res_df.category.str.split('|')
res_df['rating'] = pd.to_numeric(res_df['rating'])
res_df['restaurant_id'] = pd.to_numeric(res_df['restaurant_id'])
resWithCategory_df = res_df.copy()
for index, row in res_df.iterrows():
    for category in row['category']:
        resWithCategory_df.at[index, category] = 1
resWithCategory_df = resWithCategory_df.fillna(0)
categorytable = resWithCategory_df.set_index(
    resWithCategory_df['restaurant_id'])
categorytable = categorytable.drop(categorytable.columns[0], axis=1).drop(categorytable.columns[1], axis=1).drop(categorytable.columns[2], axis=1).drop(categorytable.columns[3], axis=1).drop(categorytable.columns[4], axis=1).drop(categorytable.columns[5], axis=1).drop(
    categorytable.columns[6], axis=1).drop(categorytable.columns[7], axis=1).drop(categorytable.columns[8], axis=1).drop(categorytable.columns[9], axis=1).drop(categorytable.columns[10], axis=1).drop(categorytable.columns[11], axis=1)

# make user preference db
# get data
curs = con.cursor()
# change id=4 to id={id} when deploy
curs.execute(f"SELECT *  from preferences where user_id={id}")
rows = curs.fetchall()
preferenceList = None
if rows:
    preferenceList = rows[0][2]['dishList']

# import a template df for making user preference, the deault value for all keywords is 0.4 for better coverage as not all keywords are being asked in the register page
userinit = pd.read_csv('./AI/userinit.csv')
# set user_id to current user id
userinit.at[0, 'user_id'] = id
# set user preferred keywords = 1
if preferenceList is not None:
    for name in preferenceList:
        userinit.at[0, name] = 1
# userprice = preferenceList[(a number)] please add a column to record user price in database
# should not use this as the userprice should be import from db
if rows[0][2]['price']:
    userprice = rows[0][2]['price']
else:
    userprice = userinit['price'][0]
userinit = userinit.drop('user_id', 1).drop('price', 1)
userinit = userinit.transpose()
# make a series for calculation
userpreference = userinit[0]


# get user rating fro psql
curs = con.cursor()
curs.execute(f"SELECT *  from dinning_records where user_id={id}")
rows = curs.fetchall()
#rows [[id,user_id,restaurant_id,choice,created_at,updated_at]]
list2 = []
userratings = None
userProfileFromRatings = None
for row in rows:
    list2.append(row)
if list2:
    userratings = pd.DataFrame(np.array(list2),
                            columns=['db_id', 'user_id', 'restaurant_id', 'choice', 'created_at', 'updated_at'])
    userratings = userratings.sort_index(ascending=False).head(100)
    userratings = userratings.drop(userratings.columns[0], axis=1).drop(
        userratings.columns[4], axis=1).drop(userratings.columns[5], axis=1)
    userratings['choice'] = pd.to_numeric(userratings['choice'])
    userratings = userratings.groupby('restaurant_id').mean().reset_index()
    userratings['restaurant_id'] = pd.to_numeric(userratings['restaurant_id'])
    userratings = userratings.reset_index(drop=True)
    userprofile = resWithCategory_df[resWithCategory_df['restaurant_id'].isin(
        userratings['restaurant_id'].tolist())]
    userprofile = userprofile.reset_index(drop=True)
    userprofiletable = userprofile.drop(userprofile.columns[0], axis=1).drop(userprofile.columns[1], axis=1).drop(userprofile.columns[2], axis=1).drop(userprofile.columns[3], axis=1).drop(userprofile.columns[4], axis=1).drop(
        userprofile.columns[5], axis=1).drop(userprofile.columns[6], axis=1).drop(userprofile.columns[7], axis=1).drop(userprofile.columns[8], axis=1).drop(userprofile.columns[9], axis=1).drop(userprofile.columns[10], axis=1).drop(userprofile.columns[11], axis=1)
    userProfileFromRatings = userprofiletable.transpose().dot(
        userratings["choice"])

# check if userProfileFromRatings is true
usermovingavglist = userpreference
if userProfileFromRatings is not None:
    usermovingavglist = (userProfileFromRatings +
                        userpreference)/(userprofiletable.sum()+1)
    usermovingavglist = usermovingavglist.fillna(0)
# output usermovingavg as json, please put your result in outputlist ={"movingavg":usermovingavglistjson, "list":yourlist} and print(outputlist) as output to child_process so that I can get the movingavg and store in db, thanks
usermovingavglistjson = usermovingavglist.to_json()
outputlist = {"movingavg": usermovingavglistjson}

# calc score
userProfile = usermovingavglist
recommendationTable = (
    (categorytable*userProfile).sum(axis=1))/(userProfile.sum())
recommendationTable = recommendationTable.sort_values(ascending=False)
recommendationTable_df = recommendationTable.to_frame()
recommendationTable_df = recommendationTable_df.rename(
    columns={recommendationTable_df.columns.values[0]: "score"})
merged = pd.merge(recommendationTable_df, res_df,
                left_index=True, right_index=True)
merged = merged.reset_index()
merged = pd.merge(recommendationTable_df, res_df,
                left_index=True, right_index=True)
merged = merged.reset_index()
maxscore = merged.iloc[0]['score']
factor = 5/maxscore
merged['weighted'] = merged.apply(
    lambda x: x['score']*factor + x['rating'], axis=1)
merged = merged.sort_values(by=['weighted'], ascending=False)
merged = merged.reset_index()

# a function that calc price similarity


def pricesimilarity(merged, userbudget):
    Usedict = None
    Fdict = {"Above 801": 5, "401-800": 4, "201-400": 3,
            "101-200": 2, "51-100": 1, "Below 50": 0}
    Edict = {"Above 801": 1, "401-800": 5, "201-400": 4,
            "101-200": 3, "51-100": 2, "Below 50": 1}
    Ddict = {"Above 801": 0, "401-800": 1, "201-400": 5,
            "101-200": 4, "51-100": 3, "Below 50": 2}
    Cdict = {"Above 801": 0, "401-800": 1, "201-400": 2,
            "101-200": 5, "51-100": 4, "Below 50": 3}
    Bdict = {"Above 801": 0, "401-800": 1, "201-400": 2,
            "101-200": 3, "51-100": 5, "Below 50": 4}
    Adict = {"Above 801": 0, "401-800": 1, "201-400": 2,
            "101-200": 3, "51-100": 4, "Below 50": 5}
    if userbudget == "Above 801":
        Usedict = Fdict
    elif userbudget == "401-800":
        Usedict = Edict
    elif userbudget == "201-400":
        Usedict = Ddict
    elif userbudget == "101-200":
        Usedict = Cdict
    elif userbudget == "51-100":
        Usedict = Bdict
    elif userbudget == "Below 50":
        Usedict = Adict

    def transformer(val):
        return Usedict[val]
    merged['userpricesimilarity'] = merged['price'].apply(transformer)
    merged['weight'] = merged.apply(
        lambda x: x['weighted'] + x['userpricesimilarity']/3, axis=1)
    merged = merged.sort_values(by=['weight'], ascending=False)
    merged = merged.reset_index(drop=True)
    return merged


# add price consideration to weighted
merged = pricesimilarity(merged, userprice)
total = merged['weighted'].sum()
merged['probability'] = merged.apply(lambda x: x['weighted']/total, axis=1)

# genList function and genList


def genList(merged, i=20):
    headcolumns = list(merged.columns.values)
    df_ = pd.DataFrame(columns=headcolumns)

    def obtainsuggestion(merged, df_):
        C = True
        while C:
            for index, row in merged.iterrows():
                if random.random() < row['probability']*i:
                    df_.loc[df_.shape[0]] = merged.iloc[index]
                if df_.shape[0] >= i:
                    C = False
        return df_
    df_ = obtainsuggestion(merged, df_)
    df_ = df_.drop_duplicates(subset=['restaurant_id'], keep='first')
    df_ = df_.reset_index(drop=True)
    while df_.shape[0] < i:
        df_ = obtainsuggestion(merged, df_)
    indexes_to_drop = []
    counter = 0
    total = df_['weighted'].sum()
    M = df_.shape[0] - i
    while df_.shape[0] - counter > i:
        for index, row in df_.iterrows():
            if random.random() < (10-row['weighted'])/total:
                if index not in indexes_to_drop:
                    indexes_to_drop.append(index)
                    counter += 1
            if df_.shape[0] - counter <= i:
                break
    df_.drop(df_.index[indexes_to_drop], inplace=True)
    df_ = df_.reset_index(drop=True)
    return df_


recommlist = genList(merged, i=60)
##
recommlist = recommlist["restaurant_id"]
##
# change recommlist to json string
recommlist = recommlist.to_json()
outputlist["recommlist"] = recommlist
print(json.dumps(outputlist))
