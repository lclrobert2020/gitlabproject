import express from 'express';
import UserController from './user.controller';

export function userRouter(
  userController: UserController,
  isLoggedIn?: express.RequestHandler
) {
  let userRoutes = express.Router();
  userRoutes.get('/personalinfo', userController.getUserInfo)
  userRoutes.post('/login', userController.login)
  userRoutes.get('/login/google', userController.loginGoogle)
  userRoutes.post('/register', userController.register)
  userRoutes.get('/logout', userController.logout)
  userRoutes.post('/editform', userController.saveNewPreferenceAfterRegister)

  return userRoutes;
}