from fastapi import APIRouter, HTTPException
from pydantic import BaseModel
from app.config import appconfigs
from app.utils.image_io import load_image_into_numpy_array
import os
from app.letter.logodetection.logodetector import LogoDetector

router = APIRouter()

logoDetector = LogoDetector()


class RequestJson(BaseModel):
    original_photo_url: str


@router.post("/detectletterlogo")
async def detect_letter_logo(requestJson: RequestJson):
    # 1. load the image as numpy array
    photo_name = requestJson.original_photo_url
    print(photo_name)
    # original_image_path = os.path.join(appconfigs.IMG_STORAGE_PATH, photo_name)
    img_np = load_image_into_numpy_array(photo_name)
    # 2. detect logo and get logo name
    logo_tag = logoDetector.get_logo_tag(img_np)
    return logo_tag
