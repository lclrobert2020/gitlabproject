from fastapi import APIRouter, HTTPException, Body, Request
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from typing import Any, Optional
from pydantic import BaseModel
from app.document.textsummarization.textsummarizer import TextSummarizer
from app.document.ocr import ocr
from app.utils.image_io import load_image_into_numpy_array
from app.utils.logger import logger
from app.config import appconfigs
import os
import time

router = APIRouter()


class OCRRequest(BaseModel):
    original_photo_url: str


@router.post("/ocr")
async def image_to_text(request: OCRRequest) -> Any:
    # Load an image from file, and convert to text using OCR.

    # Args:
    #   path: the file path to the image
    # Returns:
    #   OCR text
    start_time = time.time()
    image_path = request.original_photo_url
    photo_name = request.original_photo_url
    print(photo_name)
    # image_path = os.path.join(appconfigs.IMG_STORAGE_PATH, photo_name)
    # image_np = load_image_into_numpy_array(image_path)
    image_np = load_image_into_numpy_array(photo_name)
    image_np = ocr.rotate_img_to_text_orientation(image_np)
    text, text_data = ocr.get_eng_text(image_np)
    text = ocr.strip_non_word_text(text, text_data)

    end_time = time.time()
    elapsed_time = end_time-start_time
    logger.debug(f"ocr proccessing time: {elapsed_time} seconds")
    return {"text": text}

textsummarizer = TextSummarizer()


class TextsummaryRequest(BaseModel):
    text: str
    ratio: Optional[float]


@router.post("/textsummary")
async def text_to_summary(request: TextsummaryRequest) -> Any:
    text = request.text
    ratio = request.ratio
    print("text", text)
    if ratio is None:
        result = textsummarizer.extractive_text_summary(text)
    else:
        result = textsummarizer.extractive_text_summary(text, ratio)
    if result == "input must have more than one sentence":
        res = {"error": "input must have more than one sentence"}
    else:
        summarizedText = result[0]
        generatedKeywords = result[1]
        res = {
            "summarizedText": summarizedText,
            "generatedKeywords": generatedKeywords
        }
    return res


class AbstractiveTextsummaryRequest(BaseModel):
    text: str


@router.post("/textabstractivesummary")
async def text_to_abstractive_summary(request: AbstractiveTextsummaryRequest) -> Any:
    text = request.text
    summarizedText = textsummarizer.abstractive_text_summary(text)
    return {"summarizedText": summarizedText}
