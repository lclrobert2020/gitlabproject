from fastapi import APIRouter
from app.utils.logger import logger
from app.config import appconfigs
from pydantic import BaseModel
from app.utils.image_io import load_image_into_numpy_array,save_numpy_image
from app.card.edgedetection.edgedetector import EdgeDetector
from app.card.cardobjectdetection.cardobjectdetector import CardObjectDetertor
import os
import cv2
import uuid
import time
from app.document.ocr import ocr
from typing import List, Optional, Any, Dict
from app.card.cardtextparser.cardtextparser import CardTextParser
from app.config import appconfigs
from typing import Any

router = APIRouter()

cardObjectDetector = CardObjectDetertor()
cardparser = CardTextParser(appconfigs.JOB_TITLES_DICT_PATH)

class RequestJson(BaseModel):
    original_photo_url: str


class CardData(BaseModel):
    photo: str
    ocr_text: str
    name: List[str]
    job_title: str
    tel: List[str]
    fax: List[str]
    website: List[str]
    email: List[str]
    address: str

class CardDataResponse(BaseModel):
    card_data_list:List[CardData]

@router.post("/getcarddata", response_model=Any)
async def get_card_data(requestJson: RequestJson):
    start_time = time.time()

    # response is list of card data
    card_data_list = []

    # 1. load the image as numpy array
    photo_name = requestJson.original_photo_url
    # original_image_path=os.path.join(appconfigs.IMG_STORAGE_PATH,photo_name)
    # img_np = load_image_into_numpy_array(original_image_path)
    img_np = load_image_into_numpy_array(photo_name)

    # 2. get the list of detected card images
    card_image_list = cardObjectDetector.get_detected_card_images(img_np)

    # get the card_data for each card
    for card_image in card_image_list:
        # a new dictionary to store card data.
        card_data={}
        # 3. get edge-cropped and perspective-transformed card
        perspective_img = EdgeDetector.get_perspective_img(card_image)
        # if can't preform perspective transform, skip to next iteration
        if perspective_img is None:
            perspective_img = card_image

        # 4. rotate the image to text orientation
        rotated_img = ocr.rotate_img_to_text_orientation(perspective_img)

        # 5. preform OCR
        ocr_text, text_data = ocr.get_eng_text(rotated_img)
        ocr_text = ocr.strip_non_word_text(ocr_text, text_data)

        card_data["ocr_text"] = ocr_text

        # 6. store the output image to storage folder with unique name.
        img_name: str = 'card_%s.jpg' % str(uuid.uuid4())
        save_numpy_image(img_name,rotated_img)
        card_data["photo"] = img_name

        # 7. preform card text parsing
        text_for_parse = ocr_text
        text_for_parse, card_data["email"] = cardparser.extract_emails(text_for_parse)
        text_for_parse, card_data["website"] =cardparser.extract_websites(text_for_parse)
        text_for_parse, card_data["fax"] = cardparser.extract_faxs(text_for_parse)
        text_for_parse, card_data["tel"] = cardparser.extract_tels(text_for_parse)
        text_for_parse, card_data["job_title"] = cardparser.extract_job_title(text_for_parse)
        text_for_parse, card_data["address"] = cardparser.extract_address(text_for_parse)
        text_for_parse, card_data["name"] = cardparser.extract_names(text_for_parse)

        # 8. add card data to response list
        card_data_list.append(card_data)

    logger.info(f"card_data_list:{card_data_list}")
    end_time = time.time()
    elapsed_time = end_time-start_time
    logger.debug(f"get_card_data proccessing time: {elapsed_time} seconds")

    return card_data_list
