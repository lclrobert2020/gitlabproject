from fastapi import FastAPI, Request, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from app.api.api import api_router

# load env file and configs
import app.config
# get env variables

# import os
# print("MODEL_DIRECTORY", os.getenv("MODEL_DIRECTORY"))
# print("PATH_TO_CARD_OBJ_DET_MODEL", os.getenv("PATH_TO_CARD_OBJ_DET_MODEL"))
# print("PATH_TO_CKPT", os.getenv("PATH_TO_CKPT"))
# print("PATH_TO_CFG", os.getenv("PATH_TO_CFG"))
# print("PATH_TO_LABELS", os.getenv("PATH_TO_LABELS"))

app = FastAPI()

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"testing"}

app.include_router(api_router)


# catch-all path and return 404 not found.
# https://stackoverflow.com/questions/63069190/how-to-capture-arbitrary-paths-at-one-route-in-fastapi
# @app.route("/{full_path:path}")
# async def catch_all(full_path: str):
#     print("full_path", full_path)
#     # print("full_path: "+full_path)
#     raise HTTPException(
#         status_code=404, detail=f"404 path: {full_path} not found")


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=5000, debug=True)
