import tensorflow as tf
from app.utils.logger import logger
import time
import numpy as np
from PIL import Image
from app.config import appconfigs
import cv2


class CardObjectDetertor:
    def __init__(self):
        logger.info("loading card object detection model")
        start_time = time.time()
        self._detect_fn = tf.saved_model.load(appconfigs.CARD_SAVED_MODEL_PATH)
        end_time = time.time()
        elapsed_time = end_time - start_time
        logger.info(f"finished loading card model with {elapsed_time} seconds")

    def get_detected_card_images(self, image_np):
        # Gets all detected card images

        # Args:
        #     image (numpy.ndarray): the image with cards inside

        # Returns:
        #     card_image_list (list<ndarray>): a list of card images

        # https://stackoverflow.com/questions/43272848/what-is-dimension-order-of-numpy-shape-for-image-data
        height,width, *_ = image_np.shape
        logger.debug(f"image size (height,width): {height},{width}")

        # hotfix: transpose error
        image_np=np.transpose(image_np,(1, 0, 2))
        height,width, *_ = image_np.shape

        # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
        input_tensor = tf.convert_to_tensor(image_np)
        # The model expects a batch of images, so add an axis with `tf.newaxis`.
        input_tensor = input_tensor[tf.newaxis, ...]

        # input_tensor = np.expand_dims(image_np, 0)
        detections = self._detect_fn(input_tensor)

        # All outputs are batches tensors.
        # Convert to numpy arrays, and take index [0] to remove the batch dimension.
        # We're only interested in the first num_detections.
        num_detections = int(detections.pop('num_detections'))
        detections = {key: value[0, :num_detections].numpy()
                      for key, value in detections.items()}
        detections['num_detections'] = num_detections

        # detection_classes should be ints.
        detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

        card_image_list = []


        detection_classes_size = detections['detection_classes'].shape[0]
        print("detection_classes_size:",detection_classes_size)
        for i in range(detection_classes_size):
            # if type == 1 (card) and socres > 50%
            if detections['detection_classes'][i] == 1 and detections['detection_scores'][i] > 0.5:
                box = detections['detection_boxes'][i]
                x_min_coord = self.fit_range(int((box[0] * height) + -100),0,height)
                y_min_coord = self.fit_range(int((box[1] * width) + -100),0,width)
                x_max_coord = self.fit_range(int((box[2] * height) + 100),0,height)
                y_max_coord = self.fit_range(int((box[3] * width) + 100),0,width)
                
                print("x_min_coord:",x_min_coord,"y_min_coord:",y_min_coord,"x_max_coord",x_max_coord,"y_max_coord",y_max_coord)
                card_image = image_np[x_min_coord:x_max_coord, y_min_coord:y_max_coord, :]

                # hotfix: transpose error
                card_image=np.transpose(card_image,(1, 0, 2))
                card_image_list.append(card_image)

        return card_image_list

    def fit_range(self,x,min,max):
        if x<min:
            x=min
        elif x>max:
            x=max
        return x