import imutils

def rotate_image(image,angle):
    return imutils.rotate_bound(image,angle)