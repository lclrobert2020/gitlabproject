from gensim.summarization.summarizer import summarize
from gensim.summarization import keywords
from transformers import T5Tokenizer, T5ForConditionalGeneration, T5Config
import json
import time
from app.utils.logger import logger


class TextSummarizer:
    def __init__(self):
        # load the model to memory
        logger.info("loading T5 and tokenizer models")
        start_time = time.time()
        self._model = T5ForConditionalGeneration.from_pretrained('t5-small')
        self._tokenizer = T5Tokenizer.from_pretrained('t5-small')
        end_time = time.time()
        elapsed_time = end_time - start_time
        logger.info(f"finished loading T5 and tokenizer models with {elapsed_time} seconds")

    def extractive_text_summary(self, text, ratio=0.5, keyWordNum=5):
        try:
            return [summarize(text, ratio), keywords(text, words=keyWordNum)]
        except ValueError:
            return "input must have more than one sentence"

    def abstractive_text_summary(self, text):
        model = self._model
        tokenizer = self._tokenizer
        preprocess_text = text.strip().replace("\n", "")
        t5_prepared_Text = "summarize: "+preprocess_text
        tokentext = tokenizer.tokenize(t5_prepared_Text)
        if len(tokentext) > 512:
            t5_prepared_Text = t5_prepared_Text[:512]
        tokenized_text = tokenizer.encode(t5_prepared_Text, return_tensors="pt")

        # summmarize
        summary_ids = model.generate(tokenized_text, num_beams=4, no_repeat_ngram_size=2, min_length=30, max_length=100, early_stopping=True)
        summarized_text = tokenizer.decode(summary_ids[0], skip_special_tokens=True)
        return summarized_text
