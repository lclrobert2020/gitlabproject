# install python packages with conda
the conda environment was saved using  
```bash
conda env export > environment.yml
```  
To create and install the environment:  
```bash
conda env create -f environment.yml
```  
To update the environment:  
```bash
conda activate text_cropper
conda env update --file local.yml
```  
Or without the need to activate the environment  
```bash
conda env update --name text_cropper --file local.yml
``` 
# To run the server
1. first install/update the conda environment
2. start the fastapi server  
    ```bash
    python server.py
    ```    
the server should run at "localhost:5000" or "127.0.0.1:5000"  
# To use the server
three main api routes:  
http://127.0.0.1:5000/document  
http://127.0.0.1:5000/card  
http://127.0.0.1:5000/letter  
all the sub-routes are defined in api/endpoints  
You can see the OpenAPI documentation at http://127.0.0.1:5000/docs
# card route
## card detection
to get the card images from an image,  
POST to http://127.0.0.1:5000/card/getcardimages
with json  
```json
{"original_photo_url":"/home/usrname/Pictures/Test photo/11.jpg"}
```  
the response is:  
```json
{"saved_img_paths": ["uuid1.jpg","uuid2.jpg"]}
```
# document route
## OCR
## text summarization
to do text summarization, POST http://127.0.0.1:5000/document/textsummary with raw text.  