import sys
sys.path.append("..")
from os.path import isfile, join
from os import listdir
from argparse import ArgumentParser
from app.utils.image_io import load_image_into_numpy_array
from app.document.ocr import ocr
from app.utils.logger import logger
import cv2

# parser = ArgumentParser()
# parser.add_argument("image_path", help="image path for objection detection")
# args = parser.parse_args()
# image_np = load_image_into_numpy_array(args.image_path)


# https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
# https://stackoverflow.com/questions/5899497/how-can-i-check-the-extension-of-a-file

# only_img_files = [join(path, f)
#                   for f in listdir(path)
#                   if isfile(join(path, f))
#                   and f.lower().endswith(('.png', '.jpg', '.jpeg'))]
# print(only_img_files)

path = "/home/tkmok/Workspace/gitlab/cohort11-prj3/image_storage"
only_img_files =[join(path, "card_dc014c9f-151b-4cf2-96c5-6bd7b68afdba.jpg")]

for image_path in only_img_files:
    print("image_path: ",image_path)
    image_np = load_image_into_numpy_array(image_path)

    angle = ocr.get_text_orientation(image_np)
    logger.debug(f"{angle}")

    rotated_img = ocr.rotate_img_to_text_orientation(image_np)

    cv2.imshow("rotated image",rotated_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    text = ocr.get_eng_text(rotated_img)
    print(text)
