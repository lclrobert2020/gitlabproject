import cv2
import sys
sys.path.append("..")
from app.card.cardobjectdetection.cardobjectdetector import CardObjectDetertor
from app.utils.logger import logger
from app.config import appconfigs
from app.utils.image_io import load_image_into_numpy_array
import imutils

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("image_path", help="image path for objection detection")
    args = parser.parse_args()

    cardObjectDetector = CardObjectDetertor()

    image_np = load_image_into_numpy_array(args.image_path)
    card_image_list = cardObjectDetector.get_detected_card_images(image_np)
    logger.debug(f"card_image_list length: {len(card_image_list)}")

    for card_image in card_image_list:
        print(f"card shape: {card_image.shape}")
        cv2.imshow("Card", imutils.resize(card_image, height = 650))
        cv2.waitKey(0)
        cv2.destroyAllWindows()
