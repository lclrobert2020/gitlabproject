import sys
sys.path.append("..")
import cv2
import imutils
import logging
logging.getLogger('boto3').setLevel(logging.CRITICAL)
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('s3transfer').setLevel(logging.CRITICAL)
logging.getLogger('urllib3').setLevel(logging.CRITICAL)

from dotenv import load_dotenv
from pathlib import Path

env_path = Path('..') / '.env'
print("env_path:",env_path)
load_dotenv(dotenv_path=env_path)

from app.utils.image_io import load_image_into_numpy_array,save_numpy_image


image_np=load_image_into_numpy_array("image-1606910038641.jpeg")

cv2.imshow("test aws s3", imutils.resize(image_np, height = 650))
cv2.waitKey(0)
cv2.destroyAllWindows()

save_numpy_image("test.jpg",image_np)
image_np=load_image_into_numpy_array("test.jpg")

cv2.imshow("test aws s3", imutils.resize(image_np, height = 650))
cv2.waitKey(0)
cv2.destroyAllWindows()