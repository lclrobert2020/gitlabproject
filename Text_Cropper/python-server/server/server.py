import uvicorn
# load env file and configs
from app.app import app
from app.config import appconfigs

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=5000, debug=True,
                log_config=appconfigs.LOGGING_CONFIG_PATH)
