#%%
#!/usr/bin/env python
# coding: utf-8
"""
Object Detection From TF2 Saved Model
=====================================
"""

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Suppress TensorFlow logging (1)
import pathlib
import tensorflow as tf
import nltk
# nltk.download('maxent_ne_chunker')
# nltk.download('words')
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')

import spacy
from spacy import displacy
from collections import Counter
import en_core_web_sm
nlp = en_core_web_sm.load()


tf.get_logger().setLevel('ERROR')           # Suppress TensorFlow logging (2)

# Enable GPU dynamic memory allocation
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

f = open("job.txt", "r")
job_titles = []
for each in f:
  job_titles.append(each)
f.close()

IMAGE_PATHS = [os.path.join("test_img", "06.jpg")]

print(IMAGE_PATHS)
import matplotlib            
print (matplotlib.rcParams['backend'])

# %%





MODEL_NAME = 'my_model'

PATH_TO_CKPT = os.path.join(MODEL_NAME, 'checkpoint/')
PATH_TO_CFG = os.path.join(MODEL_NAME, 'pipeline.config')

PATH_TO_LABELS = os.path.join("annotations", "label_map.pbtxt")


# %%
# Load the model
# ~~~~~~~~~~~~~~
# Next we load the downloaded model
import time
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as viz_utils

PATH_TO_SAVED_MODEL = os.path.join("my_model", "saved_model")
print('Loading model...', end='')
print(PATH_TO_SAVED_MODEL)
start_time = time.time()

# Load saved model and build the detection function
detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL)

end_time = time.time()
elapsed_time = end_time - start_time
print('Done! Took {} seconds'.format(elapsed_time))

# %%
# Load label map data (for plotting)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Label maps correspond index numbers to category names, so that when our convolution network
# predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility
# functions, but anything that returns a dictionary mapping integers to appropriate string labels
# would be fine.

category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS,
                                                                    use_display_name=True)

# %%
# Putting everything together
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The code shown below loads an image, runs it through the detection model and visualizes the
# detection results, including the keypoints.
#
# Note that this will take a long time (several minutes) the first time you run this code due to
# tf.function's trace-compilation --- on subsequent runs (e.g. on new images), things will be
# faster.
#
# Here are some simple things to try out if you are curious:
#
# * Modify some of the input images and see if detection still works. Some simple things to try out here (just uncomment the relevant portions of code) include flipping the image horizontally, or converting to grayscale (note that we still expect the input image to have 3 channels).
# * Print out `detections['detection_boxes']` and try to match the box locations to the boxes in the image.  Notice that coordinates are given in normalized form (i.e., in the interval [0, 1]).
# * Set ``min_score_thresh`` to other values (between 0 and 1) to allow more detections in or to filter out more detections.
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import warnings
import cv2
from skimage.filters import threshold_local
import argparse
import imutils
import re
import pytesseract
from PIL import Image
from numpy import asarray
from names_dataset import NameDataset
from find_job_titles import FinderAcora
import json

m = NameDataset()

def order_points(pts):
	# initialzie a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
	# return the ordered coordinates
	return rect

def four_point_transform(image, pts):
	# obtain a consistent order of the points and unpack them
	# individually
	rect = order_points(pts)
	(tl, tr, br, bl) = rect
	# compute the width of the new image, which will be the
	# maximum distance between bottom-right and bottom-left
	# x-coordiates or the top-right and top-left x-coordinates
	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
	maxWidth = max(int(widthA), int(widthB))
	# compute the height of the new image, which will be the
	# maximum distance between the top-right and bottom-right
	# y-coordinates or the top-left and bottom-left y-coordinates
	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
	maxHeight = max(int(heightA), int(heightB))
	# now that we have the dimensions of the new image, construct
	# the set of destination points to obtain a "birds eye view",
	# (i.e. top-down view) of the image, again specifying points
	# in the top-left, top-right, bottom-right, and bottom-left
	# order
	dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype = "float32")
	# compute the perspective transform matrix and then apply it
	M = cv2.getPerspectiveTransform(rect, dst)
	warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
	# return the warped image
	return warped, [M, maxWidth, maxHeight]

def rotate(image, center = None, scale = 1.0):
    angle=int(re.search('(?<=Rotate: )\d+', pytesseract.image_to_osd(image)).group(0))
    # print(angle)
    # angle=int(re.search('(?<=Orientation in degrees: )\d+', pytesseract.image_to_osd(image)).group(0))
    # (h, w) = image.shape[:2]

    # if center is None:
    #     center = (w / 2, h / 2)

    # # Perform the rotation
    # M = cv2.getRotationMatrix2D(center, angle, scale)
    # rotated = cv2.warpAffine(image, M, (w, h))

    return angle


warnings.filterwarnings('ignore')   # Suppress Matplotlib warnings
# matplotlib.use('TkAgg')
def load_image_into_numpy_array(path):
    """Load an image from file into a numpy array.

    Puts image into numpy array to feed into tensorflow graph.
    Note that by convention we put it into a numpy array with shape
    (height, width, channels), where channels=3 for RGB.

    Args:
      path: the file path to the image

    Returns:
      uint8 numpy array with shape (img_height, img_width, 3)
    """
    return np.array(Image.open(path))


for image_path in IMAGE_PATHS:
    print(image_path)
    print('Running inference for {}... '.format(image_path), end='')
    image = Image.open(image_path)
    image_np = load_image_into_numpy_array(image_path)

    # Things to try:
    # Flip horizontally
    # image_np = np.fliplr(image_np).copy()

    # Convert image to grayscale
    # image_np = np.tile(
    #     np.mean(image_np, 2, keepdims=True), (1, 1, 3)).astype(np.uint8)

    # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
    input_tensor = tf.convert_to_tensor(image_np)
    # The model expects a batch of images, so add an axis with `tf.newaxis`.
    input_tensor = input_tensor[tf.newaxis, ...]

    # input_tensor = np.expand_dims(image_np, 0)
    detections = detect_fn(input_tensor)

    # All outputs are batches tensors.
    # Convert to numpy arrays, and take index [0] to remove the batch dimension.
    # We're only interested in the first num_detections.
    num_detections = int(detections.pop('num_detections'))
    detections = {key: value[0, :num_detections].numpy()
                   for key, value in detections.items()}
    detections['num_detections'] = num_detections

    # detection_classes should be ints.
    detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

    image_np_with_detections = image_np.copy()

    viz_utils.visualize_boxes_and_labels_on_image_array(
          image_np_with_detections,
          detections['detection_boxes'],
          detections['detection_classes'],
          detections['detection_scores'],
          category_index,
          use_normalized_coordinates=True,
          max_boxes_to_draw=200,
          min_score_thresh=.10,
          agnostic_mode=False)


    plt.figure()
    plt.imshow(image_np_with_detections)
    width, height = image.size
    image_np_temp = image_np.copy()
    print(width, height)
    # print(detections['detection_scores'][0][0])
    for i in range(100):
      #if type == 1 (card) and socres > 50%
      if detections['detection_classes'][i]==1 and detections['detection_scores'][i] > 0.5 :
        box = detections['detection_boxes'][i]
        x_min_coord = int((box[0] * height) + -100)
        y_min_coord = int((box[1] * width) + -100)
        x_max_coord = int((box[2] * height) + 100)
        y_max_coord = int((box[3] * width) + 100)
        print((image_np_temp[x_min_coord:x_max_coord, y_min_coord:y_max_coord, :]).shape)
        card_image = image_np_temp[x_min_coord:x_max_coord, y_min_coord:y_max_coord, :]
        
        cv2.imwrite(os.path.join('export', 'card'+str(i) + '.jpg'),cv2.cvtColor(card_image, cv2.COLOR_RGB2BGR))
        # data = np.zeros((height, width, 3), dtype=np.uint8)
        # data[0:256, 0:256] = [255, 0, 0] # red patch in upper left
        # img = Image.fromarray(data, 'RGB')


        image = card_image #your image here
        ratio = image.shape[0] / 500.0
        orig = image.copy()
        image = imutils.resize(image, height = 500)
        # convert the image to grayscale, blur it, and find edges
        # in the image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(gray, 75, 200)
        # show the original image and the edge detected image
        print("STEP 1: Edge Detection")
        #cv2.imshow("Image", image)
        #cv2.imshow("Edged", edged)
        # cv2_imshow(image)
        # cv2_imshow(edged)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # find the contours in the edged image, keeping only the
        # largest ones, and initialize the screen contour
        cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:5]
        # loop over the contours
        for c in cnts:
          # approximate the contour
          peri = cv2.arcLength(c, True)
          approx = cv2.approxPolyDP(c, 0.02 * peri, True)
          # if our approximated contour has four points, then we
          # can assume that we have found our screen
          if len(approx) == 4:
              screenCnt = approx
              break
        # show the contour (outline) of the piece of paper
        print("STEP 2: Find contours of paper")
        cv2.drawContours(image, [screenCnt], -1, (0, 255, 0), 2)
        #cv2.imshow("Outline", image)
        # cv2_imshow(image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        
        # apply the four point transform to obtain a top-down
        # view of the original image
        warped = four_point_transform(orig, screenCnt.reshape(4, 2) * ratio)[0]
        # convert the warped image to grayscale, then threshold it
        # to give it that 'black and white' paper effect
        warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
        T = threshold_local(warped, 11, offset = 10, method = "gaussian")
        warped = (warped > T).astype("uint8") * 255
        # show the original and scanned images
        print("STEP 3: Apply perspective transform")
        # cv2_imshow(imutils.resize(orig, height = 650))
        # cv2_imshow(imutils.resize(warped, height = 650))
        # cv2.waitKey(0)

        key_for_transformation = four_point_transform(orig, screenCnt.reshape(4, 2) * ratio)[1]
        adjusted = cv2.warpPerspective(orig, key_for_transformation[0], (key_for_transformation[1], key_for_transformation[2]))
        rotation_of_orig = rotate(adjusted)
        rotated_orig = imutils.rotate_bound(adjusted,rotation_of_orig)

        rotated = imutils.rotate_bound(warped,rotation_of_orig)
        rotated_array = asarray(rotated)
        cv2.imwrite(os.path.join('export', 'card3'+str(i) + '.jpg'),cv2.cvtColor(imutils.resize(warped, height = 650), cv2.COLOR_RGB2BGR))
        cv2.imwrite(os.path.join('export', 'orig_card3'+str(i) + '.jpg'),cv2.cvtColor(rotated_array, cv2.COLOR_RGB2BGR))
        cv2.imwrite(os.path.join('export', 'rotate_card3'+str(i) + '.jpg'),cv2.cvtColor(rotated_orig, cv2.COLOR_RGB2BGR))

        text = pytesseract.image_to_string(rotated_orig, lang="eng")
        print(text)
        emails = re.findall(r"[a-z0-9\.\-+_]+[ ]*@[ ]*[a-z0-9\.\-+_ ]+\.[a-z]+", text)
        final_emails = []
        for email in emails:
          text = text.replace(email, "")
          final_emails.append(email.replace(" ", ""))
        print("email: ", final_emails)

        websites = re.findall(r"[a-z0-9\.\-+_]+\.[a-z]+", text)
        print("websites: ", websites)

        # fax = re.search(r"((F|f)ax:? ?|(F|f):? ?)(((\*1)[ -.])?\(?(\d{3})?\)?[ -.]?(\d*)?[ -.]?(\d*)?[ -.]?(\d*))", text, re.MULTILINE)
        # print("fax: ", fax)

        matches = re.finditer(r"((F|f)ax:? ?|(F|f):? ?)(((\*1)[ -.])?\(?(\d{3})?\)?[ -.]?(\d*)?[ -.]?(\d*)?[ -.]?(\d*))", text, re.MULTILINE)
        fax = ["fax"]
        for matchNum, match in enumerate(matches, start=1):
          i = 0
          if len(match.group()) > len(fax[i]):
            fax.pop(i)
            fax.append(match.group())
            i = 1 + 1 
        text = text.replace(fax[0], "")
        fax = re.sub(r"[a-zA-Z:]*", "", fax[0]).strip()
        print(fax)

        tels = [x.group(0) for x in re.finditer(r"\(?\s?(\d[ -.]+)?\s?\)?\(?(\d+)\)?[ -.]?(\d+)?[ -.]?(\d+)", text, re.MULTILINE)]
        telephone = []
        # tels = re.findall(r"\(?(\d[ -.]+)?\)?\(?(\d+)\)?[ -.]?(\d+)?[ -.]?(\d+)", text, re.MULTILINE)
        for tel in tels:
          if len(tel.replace(",", "").replace("-", "")) > 7:
            telephone.append(tel)
        # print(tels)
        print(telephone)
        # print(text.strip())

#-------------------------------------------------------------------------
        allJobTitles = [x for x in job_titles if x in text.strip()]
        job_title = ""
        if allJobTitles:
          job_title = allJobTitles[0]
          if len(allJobTitles) > 1:
            for title in allJobTitles:
              if len(title) > len(job_title):
                job_title = title
          else: 
            job_title = allJobTitles[0]
        else:
          allJobTitles = None

        print("job title:  ", job_title)
        text = text.replace(job_title, "")
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        print("----------------------------------")
        text = text.replace(',\n', ', ') #Join address with ','
        lines = text.strip().split('\n') 
        address = ""
        for line in lines:
          if len(line.split(',')) > 2:
            print(line)
            address = line
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            text = text.replace(line, "")
        split_text = text.split( )
        name = []
        for word in split_text:
          if m.search_first_name(word):
            name.append(word)
          elif m.search_last_name(word):
            name.append(word)
        if len(name) > 1:
          name = list(dict.fromkeys(name))
        print(name)
        print("----------------------------------")
        rows = text.strip().split('\n')
        possibleName = []

        for row in rows: 
          #--------------------------Address----------------------------------
          # if row[-1] == ",":
            
          #------------------------------------------------------------

          sentences = nltk.sent_tokenize(row)
          tokenized_sentences = [nltk.word_tokenize(sentence) for sentence in sentences]
          tagged_sentences = [nltk.pos_tag(sentence) for sentence in tokenized_sentences]
          chunked_sentences = nltk.ne_chunk_sents(tagged_sentences, binary=True)

          def extract_entity_names(t):
              entity_names = []

              if hasattr(t, 'label') and t.label:
                  if t.label() == 'NE':
                      entity_names.append(' '.join([child[0] for child in t]))
                  else:
                      for child in t:
                          entity_names.extend(extract_entity_names(child))

              return entity_names

          entity_names = []
          for tree in chunked_sentences:
              if extract_entity_names(tree):
                for eachResult in extract_entity_names(tree):
                  possibleName.append(eachResult)
        print(possibleName)
        print("----------------------------------")

#-------------------------------------------------------------------------
        print("----------------------------------")
        
        sent = nlp(text)
        print("result:   ",[token for token in sent if token.ent_type_ == 'PERSON'])
        print("----------------------------------")
        
        # print("Final: -----> ", [(X.text, X.label_) for X in doc.ents])

        # for line in text.strip().splitlines():
        #   sent = nlp(line)
        #   if [token for token in sent if token.ent_type_ == 'PERSON']:
        #     print(line,"\n",[token for token in sent if token.ent_type_ == 'PERSON'])

        
        card_data = {
          "name": name,
          "job title": job_title,
          "tel": telephone,
          "fax": fax,
          "website": websites,
          "email": final_emails,
          "address": address
        }
        card_json = json.dumps(card_data)
        print(json.loads(card_json))

# plt.show()

# sphinx_gallery_thumbnail_number = 2
# %%
