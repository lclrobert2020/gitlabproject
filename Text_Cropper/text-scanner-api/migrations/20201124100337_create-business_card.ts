import * as Knex from "knex";

const usersTableName = "business_card";

export async function up(knex: Knex): Promise<void> {
    
        await knex.schema.createTable(usersTableName, (table) => {
        table.increments();
        table.string("holder_name", 255)
        table.string("holder_title", 255)
        table.string("email_on_card", 255)
        table.specificType('phone_array', 'integer ARRAY');
        table.string("company_name", 255)
        table.string("company_address", 255)
        table.string("company_website", 255)
        table.boolean("belongs_to_user")
        table.integer("document_id").unsigned();
        table.foreign('document_id').references('document.id');
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(usersTableName);
}


