import * as Knex from "knex";

const usersTableName = "document";

export async function up(knex: Knex): Promise<void> {

    await knex.schema.createTable(usersTableName, (table) => {
        table.increments();
        table.integer("document_id").unsigned();
        table.foreign('document_id').references('document.id');
        table.text("OCR_text")
        table.text("summarized_text")
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(usersTableName);
}

