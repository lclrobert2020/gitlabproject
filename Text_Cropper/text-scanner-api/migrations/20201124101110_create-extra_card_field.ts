import * as Knex from "knex";

const usersTableName = "extra_card_field";

export async function up(knex: Knex): Promise<void> {
    
        await knex.schema.createTable(usersTableName, (table) => {
        table.increments();
        table.integer("card_id").unsigned();
        table.foreign('card_id').references('business_card.id');
        table.string("field_name",255)
        table.string("field_data",255)
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(usersTableName);
}



