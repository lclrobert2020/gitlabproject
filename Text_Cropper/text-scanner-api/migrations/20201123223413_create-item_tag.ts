import * as Knex from "knex";

const usersTableName = "item_tag";

export async function up(knex: Knex): Promise<void> {
    
        await knex.schema.createTable(usersTableName, (table) => {
        table.increments();
        table.integer("tag_id").unsigned();
        table.foreign('tag_id').references('tag.id');
        table.integer("item_id").unsigned()
        table.foreign('item_id').references('item.id');
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(usersTableName);
}

