import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("item");
    if(hasTable){
        return knex.schema.table('item', (table) => {
            table.enu('type', ['business_card', 'letter', 'document'])
        });  
    }else{
        return Promise.resolve();
    }
};

export async function down(knex: Knex) {
    const hasTable = await knex.schema.hasTable("users");
    if(hasTable){
        return knex.schema.alterTable('users',(table)=>{
            table.dropColumn("type")
        });
    }else{
        return Promise.resolve();
    }
};