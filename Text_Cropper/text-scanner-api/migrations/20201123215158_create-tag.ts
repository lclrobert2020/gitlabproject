import * as Knex from "knex";

const usersTableName = "tag";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(usersTableName, (table) => {
        table.increments();
        table.integer("users_id").unsigned();
        table.foreign('users_id').references('users.id');
        table.string("tag_name", 255);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists(usersTableName);
}
