import { itemController } from '../main';

import express from "express"

export const itemRoutes = express.Router()


itemRoutes.get('/itemImage/:filename', itemController.getCardImage);
