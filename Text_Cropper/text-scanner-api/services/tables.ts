const tables = Object.freeze({
    USERS: "users",
    BUSINESS_CARD: "business_card",
    DOCUMENT: "document",
    EXTRA_CARD_FIELD: "extra_card_field",
    ITEM: "item",
    ITEM_TAG: "item_tag",
    TAG: "tag",
    PHONES: "phones_on_card",
    LETTER: "letter",
    LOGO: "logo"
});

export default tables;
