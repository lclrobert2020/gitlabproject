import Knex from "knex";
import { IUser } from "./models";
import tables from "./tables";

export class UserService {
    constructor(private knex: Knex) {}

    loadAllUsers = async () => {
        const result = await this.knex(tables.USERS).select("*");
        return result;
    };

    loadUserByEmail = async (email: string) => {
        const result = await this.knex(tables.USERS)
            .select("*")
            .where("email", email)
            .first();
        return result;
    };

    loadUserByID = async ({ id: id }: IUser) => {
        const result = await this.knex(tables.USERS)
            .select("*")
            .where("id", id)
            .first();
        return result;
    };

    saveNewUser = async (user: IUser) => {
        const newUser = await this.knex
            .insert({
                username: user.username,
                password: user.password,
                email: user.email,
                profile_picture_url: user.profile_picture_url,
            })
            .into(tables.USERS)
            .returning("*");
        return newUser;
    };

    updateUsername = async ({ username, id }: IUser) => {
        const newUsername = await this.knex("users")
            .update({ username: username, updated_at: this.knex.fn.now() })
            .where("id", id);
        return newUsername;
    };

    updateUserPassword = async ({ password, id }: IUser) => {
        const newPassword = await this.knex("users")
            .update({ password: password, updated_at: this.knex.fn.now() })
            .where("id", id);
        return newPassword;
    };

    resetPassword = async ({ password, email }: IUser) => {
        return await this.knex("users")
            .update({ password: password, updated_at: this.knex.fn.now() })
            .where("email", email);
    };
}
