import Knex from "knex";
import tables from "./tables";

export class LetterService {
    constructor(private knex: Knex) { }

    saveNewLetter = async (userId: number, photo: string) => {
        const result = await this.knex
            .insert({
                users_id: userId,
                photo_url: photo,
                is_finalized: false,
                is_deleted: false,
                created_at: this.knex.fn.now(),
                updated_at: this.knex.fn.now(),
                type: "letter"
            })
            .into(tables.ITEM)
            .returning('id')

        return result[0]
    }

    getOrganization = async (id: number) => {
        console.log("Getting organization using id ", id)
        const result = await this.knex
        .select('logo_name')
        .where('id', id)
        .from(tables.LOGO)
        console.log(result)
        return result[0].logo_name
    }

    saveLetter = async (data: object) => {
         await this.knex
        .insert(data)
        .into(tables.LETTER)
        return "Done"
    }

    getLetters = async (id: number) => {
        const letters = await this.knex(tables.ITEM)
        .select("item.id", "photo_url", "logo_name", "logo_photo_url", "item.created_at")
        .join("letter", "letter.item_id", "=", "item.id")
        .join("logo", "letter.logo_id", "=", "logo.id")
        .where({
            "item.users_id": id,
            "item.is_deleted": false 
        })
        .orderBy('item.created_at', 'desc')
        return letters
    }

    deleteLetter = async (itemId: number) => {
        // await this.knex(tables.LETTER)
        // .where('item_id', itemId)
        // .del()
        await this.knex(tables.ITEM)
        .update({
            is_deleted: true
        })
        .where('id', itemId)
        return
    }

    getUserId = async (itemId: number) => {
        const result= await this.knex(tables.ITEM)
        .select('users_id')
        .where('id', itemId)

        return result[0].users_id
    }

}