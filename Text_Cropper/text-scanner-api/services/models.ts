export interface IUser {
    id?: number;
    username?: string;
    email?: string;
    password?: string;
    profile_picture_url?: string;
}

export interface Card {
    id?: number;
    user_id?: number;
    photo_url?: string;
    is_finalized?: boolean;
    is_deleted?: boolean;
    created_at?: Date;
    updated_at?: Date;
}

export interface CardData {
    card_image_url?: string;
    holder_name?: string;
    holder_title?: string;
    phone_array?: string | string[];
    fax?: string;
    company_website?: string;
    email_on_card?: string;
    company_address?: string;
    item_id?: number;
}

export interface Item {
    id?: number;
    user_id?: number;
    photo_url?: string;
    is_finalized?: boolean;
    is_deleted?: boolean;
    created_at?: Date;
    updated_at?: Date;

}
export interface ProfileData {
    id?: number;
    name?: string;
    email?: string;
    photo?: string;
    password?: string;
}

declare global {
    namespace Express {
        interface Request {
            user?: IUser;
        }
    }
}
