import Knex from "knex";
// import { ProfileData } from "./models";
import tables from "./tables";

export class ProfileService {
    constructor(private knex: Knex) {}

    getProfile = async (id: number) => {
        const profile = await this.knex
        .from(tables.USERS)
        .select('username', 'email', 'profile_picture_url')
        .where('id', id)
        
        return profile[0]
    }

    saveImage = async (id: number, filename: string) => {
        const result = await this.knex
        .from(tables.USERS)
        .update({profile_picture_url: filename})
        .where('id', id)
        .returning('profile_picture_url')

        return result
    }

    saveName = async (id: number, name: string) => {
        const result = await this.knex
        .from(tables.USERS)
        .update({username: name})
        .where('id', id)
        .returning('username')

        return result
    }

    savePassword = async (id: number, password: string) => {
        const result = await this.knex
        .from(tables.USERS)
        .update({password: password})
        .where('id', id)
        .returning('email')

        return result
    }
}