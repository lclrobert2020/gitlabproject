import dotenv from "dotenv";
import fs from "fs";

let mode = process.env.NODE_ENV || "development";
console.log("Mode", mode);
let envFile = ".env." + mode;
let envFileContent = fs.readFileSync(envFile).toString();
let env = dotenv.parse(envFileContent);

let configs = {
    test: {
        client: "postgresql",
        connection: {
            database: env.TEST_DB_NAME,
            user: env.DB_USERNAME,
            password: env.DB_PASSWORD,
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "knex_migrations",
        },
    },

    development: {
        client: "postgresql",
        connection: {
            database: env.DB_NAME,
            user: env.DB_USERNAME,
            password: env.DB_PASSWORD,
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "knex_migrations",
        },
    },

    staging: {
        client: "postgresql",
        connection: {
            database: "my_db",
            user: "username",
            password: "password",
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "knex_migrations",
        },
    },

    production: {
        client: "postgresql",
        connection: {
            database: "my_db",
            user: "username",
            password: "password",
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "knex_migrations",
        },
    },
};

module.exports = configs;
export default configs;
