import { UserService } from "../services/users.service";
import { Response, Request } from "express";
import { logger } from "../utils/logger";
import { checkPassword, hashPassword } from "../utils/hash";
import fetch from "node-fetch";
import jwt from "../jwt";
import jwtSimple from "jwt-simple";

export class UserController {
    constructor(private userService: UserService) { }

    getAllUsers = async (req: Request, res: Response) => {
        try {
            const userArr = await this.userService.loadAllUsers();
            res.json({ user_arr: userArr });
            return;
        } catch (err) {
            logger.error(err.toString());
            res.status(500).json({ message: "Internal server error" });
            return;
        }
    };

    register = async (req: Request, res: Response) => {
        console.log("user controller 1", req.body.email);
        let { email, username, password, confirmPassword } = req.body;
        let emailError = "";
        let usernameError = "";
        let passwordError = "";
        let confirmPasswordError = "";
        let regUsername = /^[a-zA-Z0-9]{6,255}$/;
        let regEmail = /^\w+@[a-zA-Z0-9._-]+?\.[a-zA-Z]{2,3}$/;
        let regPassword = /^(?=.*[a-z]).{6,}$/;

        console.log("user controller 2");

        if (password != confirmPassword) {
            confirmPasswordError = "Password don't match";
        }
        if (!regEmail.test(email)) {
            emailError = "Email is not valid";
        }
        if (!regUsername.test(username)) {
            usernameError = "Username doesn't match the required pattern";
        }
        if (!regPassword.test(password)) {
            passwordError = "Password doesn't match the required pattern";
        }
        console.log("user controller 3");

        try {
            console.log("user controller 3.00", email);
            let emailResult = await this.userService.loadUserByEmail(email);
            console.log("user controller 3.01", emailResult);
            // if (emailResult[0]) {
            //     console.log('user controller 3.1')

            //     emailError = "Email has been registered"
            // }
            console.log("user controller 3.02", emailResult);
            if (
                !emailError &&
                !passwordError &&
                !confirmPasswordError &&
                !usernameError
            ) {
                console.log("user controller 4");

                const userHashPW = await hashPassword(password);
                console.log("user controller 5");

                await this.userService.saveNewUser({
                    username: username,
                    password: userHashPW,
                    email: email,
                    profile_picture_url: req.file?.filename,
                });
                console.log("user controller 6");
                res.json({
                    success: true,
                    url:
                        "/?verify=Your account is setup successfully. Please login to start.",
                });
                console.log("user controller 7");

                return;
            }

            res.json({
                success: false,
                passwordError: passwordError,
                usernameError: usernameError,
                confirmPasswordError: confirmPasswordError,
            });
        } catch (err) {
            logger.error(err.message);
            res.status(500).json({
                msg: "Internal Server Error for Registration",
            });
            return;
        }
    };

    login = async (req: Request, res: Response) => {
        const { email, password /* remember */ } = req.body;
        try {
            if (!email || !password) {
                res.status(401).json({ msg: "Wrong Username/Password" });
            }
            const user = await this.userService.loadUserByEmail(email);
            console.log("Loaded user info: ", user);
            if (user === undefined) {
                res.status(401).json({ msg: "Wrong Username/Password" });
                return
            }
            const checkPW = await checkPassword(password, user.password);
            // console.log('Loaded user info: ', user)
            console.log(checkPW)
            if (!checkPW) {
                res.status(401).json({ msg: "Wrong Username/Password" });
            }
            const payload = {
                id: user.id,
                email: user.email,
                username: user.username,
                profile_picture_url: user.profile_picture_url,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                token: token,
                user: JSON.stringify(payload),
            });
        } catch (error) {
            // res.redirect("/?password=Error Occurred");
            console.log("Login controller error at normal login");
            res.status(500).json(error.toString());
        }
    };

    loginFacebook = async (req: Request, res: Response) => {
        try {
            if (!req.body.accessToken) {
                res.status(401).json({ msg: "No Facebook Access Token!" });
            }

            const { accessToken } = req.body;
            const fetchResponse = await fetch(
                `https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`
            );
            const result = await fetchResponse.json();
            if (result.error) {
                res.status(401).json({ msg: "Wrong Facebook Access Token!" });
            }

            let ifHasUser = await this.userService.loadUserByEmail(result.email);
            if (!ifHasUser) {
                const randomString = Math.random().toString(36).slice(-8);
                const hashedPassword = await hashPassword(randomString);

                ifHasUser = await this.userService.saveNewUser({
                    username: result.name,
                    password: hashedPassword,
                    email: result.email,
                    profile_picture_url: result.picture.data.url,
                });
            }

            const user = await this.userService.loadUserByEmail(result.email);
            const payload = {
                id: user.id,
                email: user.email,
                username: user.username,
                profile_picture_url: user.profile_picture_url,
            };

            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                token: token,
                user: JSON.stringify(payload),
            });
        } catch (error) {
            console.log("Facebook Login controller error", error.toString());
            res.status(500).json({ msg: "Facebook login error" });
        }
    };

    loginGoogle = async (req: Request, res: Response) => {
        try {
            if (!req.body.tokenID) {
                res.status(401).json({ msg: "No Google Token ID!" });
            }
            const { tokenID } = req.body;
            const fetchResponse = await fetch(
                `https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${tokenID}`
            );
            const result = await fetchResponse.json();

            console.log("Google login result: ", result);
            if (result.error) {
                res.status(401).json({ msg: "Wrong Google Access Token!" });
            }

            let ifHasUser = await this.userService.loadUserByEmail(result.email);
            if (!ifHasUser) {
                const randomString = Math.random().toString(36).slice(-8);
                const hashedPassword = await hashPassword(randomString);
                ifHasUser = await this.userService.saveNewUser({
                    username: result.name,
                    password: hashedPassword,
                    email: result.email,
                    profile_picture_url: result.picture,
                });
            }

            const user = await this.userService.loadUserByEmail(result.email);
            const payload = {
                id: user.id,
                email: user.email,
                username: user.username,
                profile_picture_url: user.profile_picture_url,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                token: token,
                user: JSON.stringify(payload),
            });
        } catch (error) {
            console.log("Google Login controller error", error.toString());
            res.status(500).json({ msg: "Google login error" });
        }
    };

    getInfo = async (req: Request, res: Response) => {
        try {
            const user = req.user;
            await res.json(user);
        } catch (error) {
            console.log("Google Login controller error", error.toString());
            res.status(500).json({ msg: "Get user info error" });
        }
    };
}
