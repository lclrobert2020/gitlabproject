import { Response, Request } from "express"
import { ItemService } from "../services/item.service";
import { getTimeLimitedImageUrl } from "../utils/image.io";
export class ItemController {
  constructor(private itemService: ItemService) { }

  getCardImage = async (req: Request, res: Response) => {
    try {
      const filename = req.params.filename;
      console.log("filename of item image:", filename);

      const canAccess = this.itemService.checkImageBelongToCurrUser(req.user?.id, filename);
      if (canAccess) {
        //const image = await getS3Obj(filename);
        //https://github.com/expressjs/express/issues/732
        //res.end(image, 'binary');
        const imgUrl = getTimeLimitedImageUrl(filename);
        console.log("aws image private url",imgUrl);
        res.json({ success: true, url: imgUrl });
      } else {
        res.json({ success: false, message: "no right to access to photo" });
      }
    } catch (error) {
      res.status(500).json({ success: false, message: "Internal server error: Getting card photo" });
    }
  }

}
