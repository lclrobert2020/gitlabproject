import { Response, Request } from "express"
import fetch from "node-fetch";
import { CardService } from "../services/card.service";
import { deleteS3Obj } from "../utils/image.io";

export class CardController {
    constructor(private cardService: CardService) { }


    getPythonResult = async (req: Request, res: Response) => {
        // const filename = req.file.filename;
        //filename upload to s3:
        //console.log("req.files:",req.files);
        //console.log("req.file:",req.file);
        //@ts-ignore
        const filename = req.file.key;
        console.log("File name of the business card image from react: ", filename)
        try {
            // const pythonResult = await fetch(`http://localhost:5010/card/getcarddata`,
            // const pythonResult = await fetch(`http://localhost:5000/card/getcarddata`,
            const pythonResult = await fetch(process.env.PYTHON_SERVER + "/card/getcarddata",
                {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ original_photo_url: filename })
                }
            )
            const extractedData = await pythonResult.json()

            console.log("pythonResult:", extractedData)
            if (extractedData.length < 1){
                res.status(500).json({ success: false, message: "Internal server error: card controller" })
                return
            }
            let dataWithId: any = []
            if (extractedData) {
                for (let eachCard of extractedData) {
                    console.log("123", req.user)
                    const cardId = await this.cardService.saveNewItem(eachCard.photo, req.user?.id as number)
                    console.log("saved card cardId:", cardId);
                    eachCard.id = cardId
                    dataWithId.push(eachCard)
                    console.log("eachCard", eachCard)
                }
            }
            console.log("dataWithId:", dataWithId)

            //delete original image from storage
            deleteS3Obj(filename);

            //return the result to frontend
            res.json({ success: true, result: dataWithId })
        }
        catch (err) {
            console.log(err.toString());
            res.status(500).json({ success: false, message: "Internal server error: card controller" });
        }
    }

    saveNewCardInfo = async (req: Request, res: Response) => {
        console.log("Received new card info: ", req.body)
        const {
            id,
            fullName,
            jobTitle,
            tel,
            email,
            website,
            address
        } = req.body
        try {
            const newCardID = await this.cardService.saveNewBusinessCard({
                holder_name: fullName,
                holder_title: jobTitle,
                email_on_card: email,
                company_website: website,
                company_address: address,
                item_id: id
            })
            for (let eachTel of tel) {
                await this.cardService.savePhonesOnBusinessCard(eachTel, newCardID)
            }

            const updatedItem = await this.cardService.updateItem(id)
            const result = { newCardID, updatedItem }
            res.json({ success: true, result: result })
        } catch (error) {
            res.status(500).json({ success: false, message: "Internal server error: Saving new card" });
        }
    }

    loadAllCardsInfo = async (req: Request, res: Response) => {
        try {
            const allCards = await this.cardService.getAllBusinessCards(req.user?.id);
            // console.log("allCards", allCards)
            res.json({ success: true, allCards: allCards })
        } catch (error) {
            res.status(500).json({ success: false, message: "Internal server error: Getting info" });
        }
        // console.log("Card controller loadAllCardInfo runs")
    }

    deleteOneCard = async (req: Request, res: Response) => {
        try {
            const cardID = parseInt(req.params.index)
            console.log("cardID: ", cardID)
            await this.cardService.deletePhonesOnCard(cardID)
            await this.cardService.deleteOneCard(cardID);
            console.log("card deleted ",)

            res.json({ result: "okay" })
        } catch (error) {
            res.status(500).json({ success: false, message: "Internal server error: Failed to delete" });

        }
    }

}