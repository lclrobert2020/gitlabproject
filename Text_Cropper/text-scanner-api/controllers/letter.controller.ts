import { Response, Request } from "express"
import fetch from "node-fetch";
import { LetterService } from "../services/letter.service";

const organization = ["boc", "clp", "hsbc", "ird", "wsd", "other"]

export class LetterController {
    constructor(private letterService: LetterService) { }

    getPythonResult = async (req: Request, res: Response) => {
        //@ts-ignore
        const filename = req.file.key;
        console.log("File name of the letter image from react: ", filename)
        const id = req.user?.id
        console.log(id);

        let orgId
        let itemId
        let output
        console.log(process.env.PYTHON_SERVER)
        try {
            const pythonResult = await fetch(process.env.PYTHON_SERVER + "/letter/detectletterlogo",
                // const pythonResult = await fetch("http://localhost:5000/letter/detectletterlogo",
                {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ original_photo_url: filename })
                })
            let result = await pythonResult.json()
            console.log("result from python", result)

            if (result !== null) {
                console.log("in letter controller getPythonResult nested if");
                orgId = organization.indexOf(result) + 1;
                console.log()
                itemId = await this.letterService.saveNewLetter(id as number, filename)
                console.log("Id after letter saved: ", itemId);

            } else {
                result = "other"
                orgId = organization.indexOf(result) + 1;
                itemId = await this.letterService.saveNewLetter(id as number, filename)
            }
            console.log("the id of organization: ", orgId)

            const org = await this.letterService.getOrganization(orgId as number)
            console.log("get name of organization: ", org)
            output = {
                item_id: itemId,
                photo: filename,
                organization: org,
                organization_id: orgId
            }

            console.log(output);
            res.json({ success: true, result: output })
        } catch (error) {
            res.status(500).json({ success: false, message: "Internal server error" })
        }
    }

    saveLetter = async (req: Request, res: Response) => {
        // const id = req.user?.id
        const data = req.body.data
        console.log("Saving letter")
        console.log(data)
        try {
            this.letterService.saveLetter(data)
        } catch (error) {
            res.status(500).json("Internal Server Error")
        }
        res.json("Success")

    }

    getLetters = async (req: Request, res: Response) => {
        let results: any = []
        try {
            const sqlresults = await this.letterService.getLetters(req.user?.id as number)
            // const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            console.log(sqlresults)
            if (sqlresults.length > 0) {
                for (let result of sqlresults) {
                    // results.push({...result, created_at: result.created_at.toLocaleDateString("en-US",options)})
                    results.push({ ...result, created_at: result.created_at.toDateString() })
                }
            }
            console.log(results)
        } catch (error) {
            res.status(500).json("Internal Server Error")
        }
        res.json({ success: true, allLetters: results })
    }

    deleteLetter = async (req: Request, res: Response) => {
        const itemId = parseInt(req.params.index)
        console.log("Deleting");
        console.log(itemId);
        const userid = req.user?.id
        console.log(userid, await this.letterService.getUserId(itemId));


        if (userid == await this.letterService.getUserId(itemId)) {
            try {
                console.log("Verified");

                await this.letterService.deleteLetter(itemId)
            } catch (error) {
                res.json("Unsuccess")
            }
        }
        res.json("Done")
    }
}