import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux';

import BusinessCardRecord from '../components/BusinessCardRecord'
import CameraButton from '../components/CameraButton'
import "../css/BusinessCard.css"
import { resetUploader } from '../redux/uploader/action'
import { switchBotNavBarMode } from '../redux/botNavBar/action';


const BusinessCard: React.FC = () => {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(switchBotNavBarMode("BusinessCard"))
    })

    useEffect(() => {
        return () => {
            dispatch(resetUploader());
        };
    }, [dispatch]);

    return (
        <div>
            <BusinessCardRecord />
        </div>
    )
}

export default BusinessCard
