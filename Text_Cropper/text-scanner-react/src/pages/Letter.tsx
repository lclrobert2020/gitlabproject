import { resolveProjectReferencePath } from "typescript";
// import LetterComponent from "../components/LetterRecord"

import React, { useEffect } from 'react'
import CameraButton from "../components/CameraButton";
import { Link } from "react-router-dom";
import LetterRecord from "../components/LetterRecord";
import { useDispatch } from "react-redux";
import { switchBotNavBarMode } from "../redux/botNavBar/action";

const Letter: React.FC = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(switchBotNavBarMode("Letter"))
    })
    return (
        <div>

            <LetterRecord />
        </div>
    )
}

export default Letter
