import React from "react";
import {
    Input,
    InputLabel,
    makeStyles,
    FormControl,
    Paper,
    Button,
    FormGroup,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import ReactFacebookLogin, {
    ReactFacebookLoginInfo,
} from "react-facebook-login";

import { IRootState } from "../redux/store";
import {
    loginThunk,
    loginFacebookThunk,
    loginGoogleThunk,
} from "../redux/auth/thunk";
import { loginProcess, loginFailed } from "../redux/auth/action";
import Loading from "../components/Loading";
import GoogleLogin from "react-google-login";
import Register from "../components/Register";
import "../css/Login.css"

interface formInput {
    email: string;
    password: string;
}

const useStyles = makeStyles((theme) => ({
    container: {
        marginTop: "25px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: "80vh",
    },
    paper: {
        padding: "15px 15px",
        maxWidth: "1000px",
        textAlign: "center",
        [theme.breakpoints.down('xs')]: {
            width: '88vw',
        },
    },
}));

const Login: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const isLoginProcessing = useSelector(
        (state: IRootState) => state.auth.isLoginProcessing
    );
    const { register, handleSubmit } = useForm<formInput>();

    const onSubmit = (data: formInput) => {
        if (!isLoginProcessing) {
            dispatch(loginThunk(data.email, data.password));
        }
    };

    const fBOnCLick = () => {
        dispatch(loginProcess());
        return null;
    };

    const fBCallback = (
        userInfo: ReactFacebookLoginInfo & { accessToken: string }
    ) => {
        if (userInfo.accessToken && !isLoginProcessing) {
            dispatch(loginFacebookThunk(userInfo.accessToken));
        } else {
            dispatch(loginFailed("Login failed"))
        }
        return null;
    };

    const onGoogleSuccess = (response: any) => {
        if (!isLoginProcessing && response.tokenId) {
            dispatch(loginGoogleThunk(response.tokenId));
        }
    };

    const onGoogleFailure = (response: any) => {
    };

    if (isLoginProcessing) {
        return <Loading />;
    }

    return (
        <div className={classes.container}>
            <Paper className={classes.paper}>
                <h3>Login</h3>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <FormControl fullWidth={true}>
                        <InputLabel htmlFor="email">Email address</InputLabel>
                        <Input
                            type="text"
                            id="email"
                            name="email"
                            required
                            inputRef={register}
                        ></Input>
                    </FormControl>
                    <FormControl fullWidth={true}>
                        <InputLabel htmlFor="password">Password</InputLabel>
                        <Input
                            type="password"
                            id="password"
                            name="password"
                            required
                            inputRef={register}
                        ></Input>
                    </FormControl>
                    <div className="basic-btn-gp">
                        <div className="submit-btn">
                            <Button type="submit" variant="outlined">
                                Submit
                            </Button>
                        </div>
                    </div>
                </form>
                <div>
                    <div className="register-btn">
                        <Register />
                    </div>
                </div>
                <FormControl>
                    <div className="fb-button">
                        <FormGroup>
                            <ReactFacebookLogin
                                appId={
                                    process.env.REACT_APP_FACEBOOK_APP_ID ||
                                    ""
                                }
                                autoLoad={false}
                                fields="name,email,picture"
                                onClick={fBOnCLick}
                                callback={fBCallback}
                            />
                        </FormGroup>
                    </div>
                </FormControl>
                <p></p>
                <FormControl>
                    <div className="g-button">
                        <GoogleLogin
                            clientId={
                                process.env.REACT_APP_GOOGLE_CLIENT_ID || ""
                            }
                            buttonText="LOGIN WITH GOOGLE"
                            onSuccess={onGoogleSuccess}
                            onFailure={onGoogleFailure}
                            cookiePolicy={"single_host_origin"}
                            style={{
                                // marginTop: "50px",
                                borderRadius: "4px",
                                // width: "300px",
                            }}
                        // isSignedIn={true}
                        />
                    </div>
                </FormControl>
            </Paper>
        </div >
    );
};

export default Login;
