import { Card, CardActionArea, CardContent, CardMedia, makeStyles, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import "../css/Home.css"
import { IRootState } from "../redux/store";
import { useEffect } from 'react';
import { switchBotNavBarMode } from "../redux/botNavBar/action";
import Information from "../components/Information";


const useStyles = makeStyles({
    root: {
        minWidth: "200px",
        // width: 200,
        minHeight: "200px",
        padding: "0px"
    },
    media: {
        height: "148px",

    },
    logoHolder: {
        display: "flex",
        justifyContent: "center"

    }
});

const Home: React.FC = () => {
    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated)
    const classes = useStyles();
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(switchBotNavBarMode("Home"))
    })

    return (
        <>
            {!isAuthenticated && (
                <>
                    <Information />
                </>
            )}
            {isAuthenticated && (
                <div className="function-cards-all">
                    <div className="function-card">
                        <Card className={classes.root}>
                            <CardActionArea>
                                <Link to="businessCard">
                                    <CardContent>
                                        <Typography gutterBottom
                                            component="h4"
                                            style={{
                                                boxSizing: "content-box",
                                                // padding: "16px"
                                            }}>
                                            Business Card
                                        </Typography>
                                    </CardContent>
                                    <CardMedia
                                        className={classes.media}
                                        image="/function-01.png"
                                        title="Business Card"
                                    />
                                </Link>
                            </CardActionArea>

                        </Card>
                    </div>
                    <div className="function-card">
                        <Card className={classes.root}>
                            <CardActionArea>
                                <Link to="letter">
                                    <CardContent>
                                        <Typography gutterBottom component="h4">
                                            Letter
                                </Typography>
                                    </CardContent>
                                    <CardMedia
                                        className={classes.media}
                                        image="/function-02.png"
                                        title="Letter"
                                    />
                                </Link>
                            </CardActionArea>

                        </Card>
                    </div>
                    <div className="function-card">
                        <Card className={classes.root}>
                            <CardActionArea>
                                <Link to="TextSummarizer">
                                    <CardContent>
                                        <Typography gutterBottom component="h4">
                                            Text Summarization
                                </Typography>
                                    </CardContent>
                                    <CardMedia
                                        className={classes.media}
                                        image="/function-03.png"
                                        title="Text Summarization"
                                    />
                                </Link>
                            </CardActionArea>

                        </Card>
                    </div>
                </div>
            )}
        </>
    );
};

export default Home;
