import { useState, useEffect } from 'react';

const { REACT_APP_API_SERVER } = process.env
// export async function getImageUrl(filename: string) {
//   const res = await fetch(`${REACT_APP_API_SERVER}/cardImage/${filename}`, {
//     headers: { "Authorization": `Bearer ${localStorage.getItem('token')}` }
//   })
//   const resultJson = await res.json();
//   if (resultJson.success) {
//     return resultJson.url;
//   } else {
//     return null;
//   }
// }

// //https://www.digitalocean.com/community/tutorials/creating-a-custom-usefetch-react-hook
// export const useFetch = (url:string, options:RequestInit) => {
//   const [response, setResponse] = useState(null);
//   const [error, setError] = useState(null);
//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const res = await fetch(url, options);
//         const json = await res.json();
//         setResponse(json);
//       } catch (error) {
//         setError(error);
//       }
//     };
//     fetchData();
//   }, []);
//   return { response, error };
// };

interface imageUrlResponse {
  success: boolean;
  url?: string;
  message?: string;
}

export const useCardImageUrl = (filename:string) => {
  const [response, setResponse] = useState<null|imageUrlResponse>(null);
  const [error, setError] = useState(null);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch(`${REACT_APP_API_SERVER}/itemImage/${filename}`, {
          headers: { "Authorization": `Bearer ${localStorage.getItem('token')}` }
        })
        const json = await res.json();
        setResponse(json);
      } catch (error) {
        setError(error);
      }
    };
    fetchData();
  }, [filename]);
  return { response, error };
}