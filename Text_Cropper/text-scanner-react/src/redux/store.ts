import { applyMiddleware, combineReducers, createStore, compose } from "redux";
import thunk, { ThunkDispatch as OldThunkDispatch } from "redux-thunk";
import {
    RouterState,
    connectRouter,
    routerMiddleware,
    CallHistoryMethodAction,
} from "connected-react-router";
import { createBrowserHistory } from "history";
// import logger from 'redux-logger'

import { IAuthState } from "./auth/state";
import { IAuthActions } from "./auth/action";
import { authReducer } from "./auth/reducer";

import { IloadingState } from "./general/state";
import { IGeneralActions } from "./general/action";
import { generalReducer } from "./general/reducer";

import { IUploaderActions } from "./uploader/action"
import { uploaderReducers } from "./uploader/reducer"
import { IUploaderState } from "../redux/uploader/state"

import { IBusinessCardState } from './businessCard/state';
import { IBusinessCardActions } from './businessCard/action';
import { businessCardReducer } from './businessCard/reducer';

import { IProfileState } from './profile/state';
import { ProfileActions } from './profile/actions';
import { profileReducer } from './profile/reducer';

import { ILetterUploaderState } from "./letterUpload/state";
import { LetterUploaderActions } from "./letterUpload/actions";
import { letterUploaderReducer } from "./letterUpload/reducer";

import { IlettersState } from "./letter/state";
import { ILettersActions } from "./letter/actions";
import { letterRecordReducer } from "./letter/reducer"

import { ITextSummarizerState } from "./textSummarizer/state"
import { ITextSummarizerActions } from "./textSummarizer/actions";
import { textSummarizerRecordReducer } from "./textSummarizer/reducer";

import { IBotNavBarState } from "./botNavBar/state";
import { IBotNavBarActions } from './botNavBar/action';
import { botNavBarReducers } from './botNavBar/reducer';
declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}

export const history = createBrowserHistory();

export interface IRootState {
    auth: IAuthState;
    loading: IloadingState;
    uploader: IUploaderState;
    businessCard: IBusinessCardState;
    profile: IProfileState;
    router: RouterState;
    letterUploader: ILetterUploaderState;
    letter: IlettersState;
    textSummarizer: ITextSummarizerState;
    botNavBar: IBotNavBarState;
}

export type IRootAction =
    | IAuthActions
    | IGeneralActions
    | IUploaderActions
    | IBusinessCardActions
    | ProfileActions
    | CallHistoryMethodAction
    | LetterUploaderActions
    | ILettersActions
    | ITextSummarizerActions
    | IBotNavBarActions

const rootReducers = combineReducers<IRootState>({
    auth: authReducer,
    loading: generalReducer,
    uploader: uploaderReducers,
    businessCard: businessCardReducer,
    profile: profileReducer,
    letter: letterRecordReducer,
    letterUploader: letterUploaderReducer,
    textSummarizer: textSummarizerRecordReducer,
    router: connectRouter(history),
    botNavBar: botNavBarReducers,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>;

export const store = createStore<IRootState, IRootAction, {}, {}>(
    rootReducers,
    composeEnhancers(
        // applyMiddleware(logger),
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    )
);
