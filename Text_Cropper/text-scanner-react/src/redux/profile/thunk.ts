import { updateName, updatePhoto } from "../auth/action";
import { ThunkDispatch } from "../store";
import { getProfileProcessing, getProfileSuccess } from "./actions";
import axios from "axios"
import { logoutThunk } from "../auth/thunk";

const { REACT_APP_API_SERVER } = process.env;

// export function getUserProfileThunk() {
//     return async (dispatch: ThunkDispatch) => {
//         const token = localStorage.getItem('token')
//         const res = await fetch(`${REACT_APP_API_SERVER}/profile`, {
//             method: 'GET',
//             headers: {
//                 Authorization: `Bearer ${token}`
//             }
//         })
//         const result = await res.json()
//         if (res.status !== 200) {

//         } else {
//             dispatch(getProfileSuccess(result.profile))
//         }
//     }
// }

export function sendUserProfileThunk(photo: FormData) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token')
        const res = await fetch(`${REACT_APP_API_SERVER}/profile`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                //'Content-Type': 'multipart/form-data'
            },
            body: photo
        })
    }
}

export function sendNameThunk(name: string) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(getProfileProcessing())
        const token = localStorage.getItem('token')
        const res = await fetch(`${REACT_APP_API_SERVER}/profile/name`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ name }),
        });

        const result = await res.json();

        if (res.status !== 200) {
            return
        } else {
            dispatch(updateName(result[0]))
            dispatch(getProfileSuccess())
        }
    }
}

export function sendPhotoThunk(formData: FormData) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(getProfileProcessing())

        const token = localStorage.getItem('token')
        const res = await axios.post(`${REACT_APP_API_SERVER}/profile/image`, formData, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        dispatch(updatePhoto(res.data[0]))
        dispatch(getProfileSuccess())

    }
}

export function sendPasswordThunk(password: string) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(getProfileProcessing())

        const token = localStorage.getItem('token')
        const res = await fetch(`${REACT_APP_API_SERVER}/profile/password`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ password }),
        });

        const result = await res.json();

        if (res.status !== 200) {
            return
        } else {
            dispatch(logoutThunk())
            dispatch(getProfileSuccess())
        }
    }
}