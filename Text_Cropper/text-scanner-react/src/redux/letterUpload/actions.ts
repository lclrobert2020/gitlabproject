export const GET_DATA_PROCESSING = '@@LETTER/GET_DATA_PROCESSING';
export const WRITE_DATA_SUCCESS = '@@LETTER/WRITE_DATA_SUCCESS'
export const GET_DATA_SUCCESS = '@@LETTER/GET_DATA_SUCCESS'
export const RESET_LETTER = '@@LETTER/RESET_LETTER'


export function getDataProcessing() {
    return {
        type: GET_DATA_PROCESSING as typeof GET_DATA_PROCESSING,
    }
}

export function writeLetterData(data: object) {
    return {
        type: WRITE_DATA_SUCCESS as typeof WRITE_DATA_SUCCESS,
        data
    }
}

export function getDataSuccess() {
    return {
        type: GET_DATA_SUCCESS as typeof GET_DATA_SUCCESS,
    }
}

export function resetLetter() {
    return {
        type: RESET_LETTER as typeof RESET_LETTER
    }
}

type ActionCreatorTypes = typeof getDataProcessing | typeof getDataSuccess | typeof writeLetterData | typeof resetLetter;
export type LetterUploaderActions = ReturnType<ActionCreatorTypes>