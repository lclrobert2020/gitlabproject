import { IAuthState, initialState, IUserInfo } from "./state";
import {
    IAuthActions,
    LOGIN_PROCESS,
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    LOGOUT,
    REGISTER_PROCESS,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    UPDATE_NAME,
    UPDATE_PHOTO
} from "./action";


export const authReducer = (
    state: IAuthState = initialState,
    action: IAuthActions
) => {
    switch (action.type) {
        case LOGIN_PROCESS:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: true,
                msg: "",
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                isLoginProcessing: false,
                msg: "",
                user: action.user,
            };
        case LOGIN_FAILED:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: false,
                msg: action.msg,
            };
        case LOGOUT:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: false,
                msg: "",
            };
        case REGISTER_PROCESS:
            return {
                ...state,
                isRegistered: false,
                isRegisterProcessing: true,
            };
        case REGISTER_SUCCESS:
            return {
                ...state,
                isRegistered: true,
                isRegisterProcessing: false,
                register: action.result,
            };
        case REGISTER_FAILED:
            return {
                ...state,
                isAuthenticated: false,
                isLoginProcessing: false,
                msg: action.msg,
            };
        case UPDATE_PHOTO:
            return {
                ...state,           
                user: {...state.user as IUserInfo,
                    profile_picture_url: action.photo}
            };
        case UPDATE_NAME:
            return {
                ...state,
                user: {...state.user as IUserInfo,
                    username: action.name}
            }
        default:
            return state;
    }
};
