import { IUserInfo } from "./state";
export const LOGIN_PROCESS = "@@AUTH/LOGIN_PROCESS";
export const LOGIN_SUCCESS = "@@AUTH/LOGIN_SUCCESS";
export const LOGIN_FAILED = "@@AUTH/LOGIN_FAILED";
export const LOGOUT = "@@AUTH/LOGOUT";
export const REGISTER_PROCESS = "@@AUTH/REGISTER_PROCESS";
export const REGISTER_SUCCESS = "@@AUTH/REGISTER_SUCCESS";
export const REGISTER_FAILED = "@@AUTH/REGISTER_FAILED";
export const UPDATE_NAME = "@@AUTH/UPDATE_NAME"
export const UPDATE_PHOTO = "@@AUTH/UPDATE_PHOTO"



export function loginProcess() {
    return {
        type: LOGIN_PROCESS as typeof LOGIN_PROCESS,
    };
}

export function loginSuccess(user: IUserInfo) {
    return {
        type: LOGIN_SUCCESS as typeof LOGIN_SUCCESS,
        user,
    };
}

export function loginFailed(msg: string) {
    return {
        type: LOGIN_FAILED as typeof LOGIN_FAILED,
        msg,
    };
}

export function logout() {
    return {
        type: LOGOUT as typeof LOGOUT,
    };
}

export function registerProcess() {
    return {
        type: REGISTER_PROCESS as typeof REGISTER_PROCESS,
    };
}

export function registerSuccess(result: string) {
    return {
        type: REGISTER_SUCCESS as typeof REGISTER_SUCCESS,
        result,
    };
}

export function registerFailed(msg: string) {
    return {
        type: REGISTER_FAILED as typeof REGISTER_FAILED,
        msg,
    };
}

export function updateName(name: string) {
    return {
        type: UPDATE_NAME as typeof UPDATE_NAME,
        name,
    }
}

export function updatePhoto(photo: string) {
    return {
        type: UPDATE_PHOTO as typeof UPDATE_PHOTO,
        photo,
    }
}



type AuthActionCreators =
    | typeof loginProcess
    | typeof loginSuccess
    | typeof loginFailed
    | typeof logout
    | typeof registerProcess
    | typeof registerSuccess
    | typeof registerFailed
    | typeof updatePhoto
    | typeof updateName;

export type IAuthActions = ReturnType<AuthActionCreators>;
