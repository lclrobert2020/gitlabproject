export interface IUserInfo {
    id: number;
    username: string;
    email: string;
    profile_picture_url?: string | null;
}

export interface IAuthState {
    isAuthenticated: boolean | null;
    isLoginProcessing: boolean;
    isRegistered: boolean | null;
    isRegisterProcessing: boolean;
    msg: string;
    result: string;
    user: IUserInfo | null;
}

export const initialState = {
    isAuthenticated: null,
    isLoginProcessing: false,
    isRegistered: false,
    isRegisterProcessing: false,
    msg: "",
    result: "",
    user: null
};
