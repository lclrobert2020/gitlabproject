export interface IUploaderState {
    IsWebcamOpen: boolean
    captureImgStr: string | undefined
    WebCamBtuText: string
    IsLoading: Boolean
    ErrorMsg: string | undefined
    ReturnedData: any | object[] | undefined
    Mode: number | undefined
    IsReturnedDataState: boolean
}

export const initialState = {
    IsWebcamOpen: false,
    captureImgStr: undefined,
    WebCamBtuText: "Use Webcam",
    IsLoading: false,
    ErrorMsg: undefined,
    ReturnedData: undefined,
    Mode: undefined,
    IsReturnedDataState: false,
}