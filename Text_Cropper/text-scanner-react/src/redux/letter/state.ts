export interface Iletter {
    id: number,
    photo_url: string,
    logo_name: string,
    logo_photo_url: string,
    created_at: Date
}

export interface IlettersState {
    is_deleteing: boolean,
    is_processing: boolean,
    letters: Array<Iletter> | null,
    msg: string
}

export const initLettersState: IlettersState = {
    is_deleteing: false,
    is_processing: false,
    letters: [],
    msg: ""
}
