import { IlettersState, initLettersState } from './state';
import { GET_LETTER_PROCESSING, GET_LETTER_SUCCESS, GET_LETTER_FAILED, ILettersActions, DELETE_LETTER_PROCESSING, DELETE_LETTER_SUCCESS, DELETE_LETTER_FAILED } from './actions'

export const letterRecordReducer = (
    state: IlettersState = initLettersState,
    action: ILettersActions): IlettersState => {
    switch (action.type) {
        case GET_LETTER_PROCESSING:
            return {
                ...state,
                is_processing: true
            };
        case GET_LETTER_SUCCESS:
            return {
                ...state,
                letters: action.data,
                is_processing: false,
                msg: "Success"
            };
        case GET_LETTER_FAILED:
            return {
                ...state,
                is_processing: false,
                msg: "Internal Server Error"
            };
        case DELETE_LETTER_PROCESSING:
            return {
                ...state,
                is_deleteing: true,
                msg: "Success"
            };
        case DELETE_LETTER_SUCCESS:
            return {
                ...state,
                is_deleteing: false,
                msg: "Success"
            };
        case DELETE_LETTER_FAILED:
            return {
                ...state,
                is_deleteing: false,
                msg: "Internal Server Error"
            };
        default:
            return state;
    }
}
