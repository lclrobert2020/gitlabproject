import { ThunkDispatch } from "../store"
import { deleteLetterFailed, deleteLetterProcessing, deleteLetterSuccess, getLetterFailed, getLettersProcessing, getLettersSuccess } from "./actions"

const { REACT_APP_API_SERVER } = process.env

export function getAllLettersThunk() {
    return async (dispatch: ThunkDispatch) => {
        dispatch(getLettersProcessing())
        const res = await fetch(`${REACT_APP_API_SERVER}/letters`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json();
        if (res.status !== 200) {
            dispatch(getLetterFailed())
        } else {
            dispatch(getLettersSuccess(result.allLetters))

        }
    }
}

export function deleteLetterThunk(itemId: number) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(deleteLetterProcessing())
        const token = localStorage.getItem('token');
        const res = await fetch(`${REACT_APP_API_SERVER}/deleteletter/${itemId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        if (res.status !== 200) {
            dispatch(deleteLetterFailed())
        } else {
            dispatch(deleteLetterSuccess())
        }
    }
}