import { IBotNavBarState , initialState } from "./state"
import { IBotNavBarActions } from './action'

const SWITCH_MODE= "@@BOTNAVBAR/SWITCH_MODE"

export const botNavBarReducers = (state: IBotNavBarState  = initialState, action: IBotNavBarActions) => {
    switch (action.type) {
        case SWITCH_MODE:
            return {
                ...state, label: action.label
            }
        default:
            return state
    }
}