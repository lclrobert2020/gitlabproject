import { ITextSummarizerState, initTextSummarizerState } from './state';
import { DELETE_TEXTSUMMARY_FAILED, DELETE_TEXTSUMMARY_PROCESSING, DELETE_TEXTSUMMARY_SUCCESS, GET_TEXTSUMMARY_FAILED, GET_TEXTSUMMARY_PROCESSING, GET_TEXTSUMMARY_SUCCESS, ITextSummarizerActions } from './actions'

export const textSummarizerRecordReducer = (
    state: ITextSummarizerState = initTextSummarizerState,
    action: ITextSummarizerActions): ITextSummarizerState => {
    switch (action.type) {
        case GET_TEXTSUMMARY_PROCESSING:
            return {
                ...state,
                is_processing: true
            };
        case GET_TEXTSUMMARY_SUCCESS:
            return {
                ...state,
                textSummary: action.data,
                is_processing: false,
                msg: "Success"
            };
        case GET_TEXTSUMMARY_FAILED:
            return {
                ...state,
                is_processing: false,
                msg: "Internal Server Error"
            };
        case DELETE_TEXTSUMMARY_PROCESSING:
            return {
                ...state,
                is_deleteing: true,
                msg: "Success"
            };
        case DELETE_TEXTSUMMARY_SUCCESS:
            return {
                ...state,
                is_deleteing: false,
                msg: "Success"
            };
        case DELETE_TEXTSUMMARY_FAILED:
            return {
                ...state,
                is_deleteing: false,
                msg: "Internal Server Error"
            };
        default:
            return state;
    }
}
