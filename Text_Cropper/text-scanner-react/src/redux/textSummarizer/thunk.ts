import { ThunkDispatch } from "../store"
import { push } from "connected-react-router";
import { writeFetchData } from '../uploader/action';
import { deleteTextSummaryFailed, deleteTextSummaryProcessing, deleteTextSummarySuccess, getTextSummaryFailed, getTextSummaryProcessing, getTextSummarySuccess } from "./actions"

const { REACT_APP_API_SERVER } = process.env

export function sendStringToSummaryThunk(data: any, setValue: any, setSendToStoreState: any, setSubBtnText: any, setErrorMsg: any, setTextAreaIsLoading: any) {
    return async (dispatch: ThunkDispatch) => {
        setTextAreaIsLoading(true)
        try {
            const token = localStorage.getItem('token');
            const res = await fetch(`${REACT_APP_API_SERVER}/generateSummary`,
                {
                    method: 'POST',
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                }
            );
            const resData = await res.json();
            if (resData.data?.textSummary && resData.success) {
                setTextAreaIsLoading(false)
                setErrorMsg(false)
                setValue("Text", resData.data.textSummary.summarizedText)
                dispatch(writeFetchData(resData))
                setSubBtnText("Store Summary")
                setSendToStoreState(true)
            } else {
                if (resData.error) {
                    setTextAreaIsLoading(false)
                    setErrorMsg(resData.error)
                } else {
                    setTextAreaIsLoading(false)
                    setErrorMsg("Unable to use Summary service")
                }
            }
        } catch (e) {
            setTextAreaIsLoading(false)
            setErrorMsg("Unable to use Summary service")
        }
    }
}

export function sendSummaryToStoreThunk(data: any) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${REACT_APP_API_SERVER}/saveSummary`,
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        );
        const resData = await res.json();
        if (resData.success) { dispatch(push("/")) }
    }
}

export function getAllTextSummaryThunk() {
    return async (dispatch: ThunkDispatch) => {
        dispatch(getTextSummaryProcessing())
        const res = await fetch(`${REACT_APP_API_SERVER}/textsummaryrecord`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        const result = await res.json();
        if (res.status !== 200) {
            dispatch(getTextSummaryFailed())
        } else {
            dispatch(getTextSummarySuccess(result.allTextSummary))

        }
    }
}

export function deleteTextSummaryThunk(itemId: number) {
    return async (dispatch: ThunkDispatch) => {
        dispatch(deleteTextSummaryProcessing())
        const token = localStorage.getItem('token');
        const res = await fetch(`${REACT_APP_API_SERVER}/deleteTextSummary/${itemId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        if (res.status !== 200) {
            dispatch(deleteTextSummaryFailed())
        } else {
            dispatch(deleteTextSummarySuccess())
        }
    }
}