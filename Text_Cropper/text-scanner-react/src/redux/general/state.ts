export interface IloadingState {
    isLoading: boolean;
}

// export interface IGeneralState {

// }

export const initialState = {
    isLoading: false,
};
