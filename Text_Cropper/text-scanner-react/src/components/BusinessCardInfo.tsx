import { makeStyles, Card, CardHeader, Avatar, CardMedia, CardContent, Grid } from '@material-ui/core';
import { lightBlue } from '@material-ui/core/colors';
import { useForm } from 'react-hook-form';
import { IBusinessCardInfo } from '../redux/businessCard/state';
import React from 'react';
import { useDispatch } from 'react-redux';
import { Button } from '@material-ui/core';
import useState from 'react';


const useStyles = makeStyles((theme) => ({
    rootForContactForm: {
        maxWidth: 345,
        minHeight: 300,
        marginTop: '3vh',
        backgroundColor: "#EEE",
        marginLeft: 'auto',
        marginRight: 'auto',
        overflow: `auto !important`,
    },
    media: {
        height: 0,
        paddingTop: "56.25%", // 16:9
        overflow: "auto",
    },
    expand: {
        transform: "rotate(0deg)",
        marginLeft: "auto",
        transition: theme.transitions.create("transform", {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: "rotate(180deg)",
    },
    avatar: {
        backgroundColor: lightBlue[500],
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    saveBtn: {
        backgroundColor: "#28DB9F",
        border: "1px solid #3ce8ae",
        color: "#FFFFFF",
        width: 100,
        margin: 8,
        padding: 8,
        '&:hover': {
            backgroundColor: "#3F89FF",
            // color: "#27FFBB"
        }
    },
    bCardSubmit: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    autoFillCol: {
        fontSize: 14
    }
}));

const BusinessCardInfo = (props: any) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cardInfo = props.location.state;
    const {
        id,
        photo_url,
        bCard_id,
        holder_name,
        holder_title,
        email_on_card,
        company_fax,
        company_name,
        company_address,
        company_website,
        item_id,
        phones,
    } = cardInfo;

    const [currentCard, setCurrentCard] = React.useState({
        card_image_url: photo_url,
        id: bCard_id,
        fullName: holder_name,
        jobTitle: holder_title,
        tel: phones[0],
        fax: company_fax,
        email: email_on_card,
        website: company_website,
        address: company_address,
    })

    const { register, handleSubmit } = useForm<IBusinessCardInfo>({
        // resolver: yupResolver(schema),
        mode: "onBlur",
        defaultValues: {
            card_image_url: currentCard.card_image_url,
            id: currentCard.id,
            fullName: currentCard.fullName,
            jobTitle: currentCard.jobTitle,
            tel: currentCard.tel,
            fax: currentCard.fax,
            email: currentCard.email,
            website: currentCard.website,
            address: currentCard.address,
        },
    });

    const updateCardSubmit = (data: any) => {
    }

    return (
        <>
            <div key={`cardData_${id}`}>
                <Card className={classes.rootForContactForm}>
                    <CardHeader
                        avatar={
                            <Avatar
                                aria-label="recipe"
                                className={classes.avatar}
                            ></Avatar>
                        }
                        title={holder_name} style={{ height: '50px' }}
                    />
                    <CardMedia
                        className={classes.media}
                        image={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${photo_url}`}
                    />
                    <form
                        onSubmit={handleSubmit(updateCardSubmit)}
                        className="whole-contact-form"
                    >
                        <CardContent style={{ padding: '16px 0px 16px 0px' }}>
                            <input
                                type="hidden"
                                name="card_image_url"
                                ref={register}
                            />
                            <input type="hidden" name="id"
                                ref={register}
                            />
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">Name</div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="text"
                                            name="fullName"
                                            className={classes.autoFillCol}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">
                                            Job Title
                                        </div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="text"
                                            name="jobTitle"
                                            className={classes.autoFillCol}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">Tel</div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="tel"
                                            name="tel"
                                            className={classes.autoFillCol}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            {phones.length > 1 ? (
                                <>
                                    {phones.slice(1).map((eachTel: string, index: number) => (
                                        <div className="contact-column" key={`tel${index}`}>
                                            <Grid container>
                                                <Grid item xs={4} lg={4}>
                                                    <div className="contact-tag">Extra Tel</div>
                                                </Grid>
                                                <Grid item xs={8} lg={8}>
                                                    <input
                                                        type="tel"
                                                        name={`extraTel${index}`}
                                                        value={eachTel}
                                                        className={classes.autoFillCol}
                                                        ref={register}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </div>
                                    ))}
                                </>
                            ) : ""}
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">Fax</div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="tel"
                                            name="fax"
                                            className={classes.autoFillCol}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">
                                            Website
                                        </div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="text"
                                            name="website"
                                            className={classes.autoFillCol}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">Email</div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="email"
                                            name="email"
                                            className={classes.autoFillCol}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className="contact-column">
                                <Grid container>
                                    <Grid item xs={4} lg={4}>
                                        <div className="contact-tag">
                                            Address
                                        </div>
                                    </Grid>
                                    <Grid item xs={8} lg={8}>
                                        <input
                                            type="text"
                                            name="address"
                                            className={classes.autoFillCol}
                                            ref={register}
                                        />
                                    </Grid>
                                </Grid>
                            </div>
                            <div className={classes.bCardSubmit}>
                                <Button
                                    type="submit"
                                    name={`submitBtn_${id}`}
                                    className={classes.saveBtn}
                                >
                                    Save
                                </Button>
                            </div>
                        </CardContent>
                    </form>
                </Card>
            </div>
        </>
    )
}

export default BusinessCardInfo
