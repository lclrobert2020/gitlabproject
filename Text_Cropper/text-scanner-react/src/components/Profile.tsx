import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IRootState } from '../redux/store'
import { Avatar, FormControl, FormHelperText, Input, InputLabel, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { useForm } from 'react-hook-form';
import axios from 'axios'
import EditIcon from '@material-ui/icons/Edit';
import { logoutThunk } from '../redux/auth/thunk';
import { updateName, updatePhoto } from '../redux/auth/action'
import { IUserInfo } from '../redux/auth/state';
import { sendNameThunk, sendPasswordThunk, sendPhotoThunk } from '../redux/profile/thunk';
import Loading from './Loading';
import Logout from './Logout';

const { REACT_APP_API_SERVER } = process.env

interface formInput {
    username?: string;
    email?: string;
    photo?: string;
    password?: string;
}

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: 275,
        maxWidth: 500,
        height: 'auto',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginLeft: 'auto',
        marginRight: 'auto',
        boxShadow: theme.shadows[10],
        borderRadius: 10,
    },
    icon: {
        textAlign: 'center',
        justifyContent: 'center',
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
        marginTop: 14,
    },
    pos: {
        marginBottom: 12,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(250, 250, 250, 0.2)',
    },
    paper: {
        backgroundColor: 'rgba(250, 250, 250, 0.9)',
        boxShadow: theme.shadows[10],
        padding: theme.spacing(4, 4, 4),
        borderRadius: 25,
        maxWidth: 500,
        marginLeft: 20,
        marginRight: 20,
        '&:focus': {
            outline: "none",
        },
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    container: {
        display: 'flex',
        alignItems: 'center'
    },
}));

const ProfileComponent: React.FC = () => {
    const dispatch = useDispatch()
    const profile = useSelector((state: IRootState) => state.auth.user as IUserInfo)
    const profile_pic = profile.profile_picture_url
    const classes = useStyles();
    const { register, handleSubmit } = useForm();
    const [openPhotoEdit, setOpenPhotoEdit] = useState(false);
    const [openNameEdit, setOpenNameEdit] = useState(false);
    const [openEmailEdit, setOpenEmailEdit] = useState(false);
    const [openEdit, setOpenEdit] = useState(false);
    const [file, setFile] = useState("");
    const [filename, setFilename] = useState('Choose File')
    const [imageURL, setImageURL] = useState("");
    const [validateMsg, setValidateMsg] = useState("")
    // const isLoading = useSelector((state: IRootState) => state.profile.is_processing);
    const handlePhotoOpen = () => {
        setOpenPhotoEdit(true);
    };

    const handlePhotoClose = () => {
        setOpenPhotoEdit(false);
        setFilename('')
        setFile('')
    };

    const handleNameOpen = () => {
        setOpenNameEdit(true);
    };

    const handleNameClose = () => {
        setOpenNameEdit(false);
    };

    const handleEmailOpen = () => {
        setOpenEmailEdit(true);
    };

    const handleEmailClose = () => {
        setOpenEmailEdit(false);
        setValidateMsg("")
    };

    const handleOpen = () => {
        setOpenEdit(true);
    };

    const handleClose = () => {
        setOpenEdit(false);
    };

    const onSubmit = async (data: formInput) => {
    };

    const onSelectedPhoto = (e: any) => {
        setFile(e.target.files[0]);
        setFilename(e.target.files[0].name);

    }

    const onSubmitPhoto = async (event: any) => {
        const formData = new FormData()
        formData.append('image', file, filename)
        dispatch(sendPhotoThunk(formData))
        setFilename('')
        setFile('')
        setOpenPhotoEdit(false)
    }

    const onSubmitPassword = async (data: any) => {
        const password = data.password
        const retype = data.retype_password
        if (password !== retype) {
            setValidateMsg("Password do not match")
            return
        } else {
            dispatch(sendPasswordThunk(password))
            setValidateMsg("")
            setOpenEmailEdit(false)
        }
    }



    const onSubmitName = async (data: any) => {
        const name = data.username
        dispatch(sendNameThunk(name))
        setOpenNameEdit(false)
    }

    useEffect(() => {
        if (profile_pic) {
            if (profile_pic.slice(0, 5) === "https") {
                setImageURL(profile_pic);
            } else {
                const imgAPI: any =
                    process.env.REACT_APP_API_SERVER +
                    "/" +
                    profile_pic;
                setImageURL(imgAPI);
            }
        }
    }, [profile.username, profile_pic])

    // if (isLoading) {
    //     return <Loading />;
    // }
    return (
        <div>
            <Card className={classes.root} variant="outlined">
                <CardContent>
                    <div>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            User Profile
                        </Typography>
                    </div>
                    <div>
                        <div className={classes.container}>
                            <Typography variant="h5" component="h2">
                                {profile.username}
                            </Typography>
                            <Button type="button" onClick={handleNameOpen}>
                                <EditIcon />
                            </Button>
                        </div>

                        <Typography className={classes.pos} color="textSecondary">
                            {profile.email}
                        </Typography>
                        <Button size="small" type="button" onClick={handleEmailOpen}>
                            Change Password
                        </Button>
                        <Button size="small" type="button">
                            <Logout />
                        </Button>

                    </div>
                </CardContent>
                <div className={classes.icon}>
                    <Avatar alt="Remy Sharp" src={imageURL as string} className={classes.large} />
                    <CardActions>
                        <Button size="small" type="button" onClick={handlePhotoOpen}>Edit</Button>
                    </CardActions>

                </div>
            </Card>
            <div>
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={openPhotoEdit}
                    onClose={handlePhotoClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={openPhotoEdit}>
                        <div className={classes.paper}>
                            <h3>Please complete the fields below</h3>
                            <form onSubmit={handleSubmit(onSubmitPhoto)}>

                                <FormControl fullWidth={true}>
                                    {/* <InputLabel htmlFor="Picture">Picture</InputLabel> */}
                                    <Input type="file" id="picture" required inputRef={register} onChange={onSelectedPhoto} />
                                    {filename}
                                </FormControl>
                                <br /><br />
                                <Button size="small" type="submit">Save</Button>
                            </form>
                        </div>
                    </Fade>
                </Modal>
            </div>
            <div>
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={openNameEdit}
                    onClose={handleNameClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={openNameEdit}>
                        <div className={classes.paper}>
                            <h3>Please complete the fields below</h3>
                            <form onSubmit={handleSubmit(onSubmitName)}>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="username">Username</InputLabel>
                                    <Input type="text" id="username" name="username" required inputRef={register} />
                                </FormControl>
                                <br /><br />
                                <Button size="small" type="submit">Save</Button>
                            </form>
                        </div>
                    </Fade>
                </Modal>
            </div>
            <div>
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={openEmailEdit}
                    onClose={handleEmailClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={openEmailEdit}>
                        <div className={classes.paper}>
                            <h3>Please complete the fields below</h3>
                            <form onSubmit={handleSubmit(onSubmitPassword)}>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="password">Password</InputLabel>
                                    <Input type="password" id="password" name="password" required inputRef={register} />
                                </FormControl>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="password">Re-type Password</InputLabel>
                                    <Input type="password" id="retype_password" name="retype_password" required inputRef={register} />
                                </FormControl>
                                <h5>{validateMsg}</h5>
                                <br /><br />
                                <Button size="small" type="submit">Save and Logout</Button>
                            </form>
                        </div>
                    </Fade>
                </Modal>
            </div><div>
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={openEdit}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={openEdit}>
                        <div className={classes.paper}>
                            <h3>Please complete the fields below</h3>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="username">Username</InputLabel>
                                    <Input type="text" id="username" name="username" required inputRef={register} />
                                </FormControl>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="password">Email</InputLabel>
                                    <Input type="text" id="password" name="email" required inputRef={register} />
                                </FormControl>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="password">Password</InputLabel>
                                    <Input type="password" id="password" name="password" required inputRef={register} />
                                </FormControl>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="password">Re-type Password</InputLabel>
                                    <Input type="password" id="retype_password" required inputRef={register} />
                                </FormControl>

                                <br /><br />
                                <Button size="small" type="submit">Submit and Logout</Button>
                            </form>
                        </div>
                    </Fade>
                </Modal>
            </div>
            <div>
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    className={classes.modal}
                    open={openEdit}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={openEdit}>
                        <div className={`${classes.paper} profile-paper`}>
                            <h3>Please complete the fields below</h3>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="username">Username</InputLabel>
                                    <Input type="text" id="username" name="username" required inputRef={register} />
                                </FormControl>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="password">Email</InputLabel>
                                    <Input type="text" id="password" name="email" required inputRef={register} />
                                </FormControl>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="password">Password</InputLabel>
                                    <Input type="password" id="password" name="password" required inputRef={register} />
                                </FormControl>
                                <FormControl fullWidth={true}>
                                    <InputLabel htmlFor="password">Re-type Password</InputLabel>
                                    <Input type="password" id="retype_password" required inputRef={register} />
                                </FormControl>

                                <br /><br />
                                <Button size="small" type="submit">Submit and Logout</Button>
                            </form>
                        </div>
                    </Fade>
                </Modal>
            </div>
        </div>
    )
}

export default ProfileComponent