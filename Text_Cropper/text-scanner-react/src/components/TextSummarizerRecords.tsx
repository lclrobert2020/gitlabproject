import { Theme, createStyles, Card, CardActionArea, CardContent, Typography, emphasize, CardMedia, makeStyles, Button, Link, IconButton, useTheme, withStyles, Divider, SwipeableDrawer } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../redux/store'
import Paper from '@material-ui/core/Paper';
import Loading from './Loading';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { deleteTextSummaryThunk, getAllTextSummaryThunk } from '../redux/textSummarizer/thunk';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import clsx from 'clsx';
import ListIcon from '@material-ui/icons/List';
import ImageIcon from '@material-ui/icons/Image';

import "../css/TextSummarizer.css"

type Anchor = 'top' | 'left' | 'bottom' | 'right';

const BootstrapButton = withStyles({
    root: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 16,
        //   padding: '6px 12px',
        //   border: '1px solid',
        lineHeight: 1.5,
        //   backgroundColor: '#0063cc',
        //   borderColor: '#0063cc',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            // backgroundColor: '#0069d9',
            // borderColor: '#0062cc',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: 'rgb(235,235,235)',
            borderColor: 'rgb(235,235,235)',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(235,235,235,.5)',
        },
    },
})(Button);

const StyledBreadcrumb = withStyles((theme: Theme) => ({
    root: {
        backgroundColor: theme.palette.grey[100],
        height: theme.spacing(3),
        color: theme.palette.grey[800],
        fontWeight: theme.typography.fontWeightRegular,
        '&:hover, &:focus': {
            backgroundColor: theme.palette.grey[300],
        },
        '&:active': {
            boxShadow: theme.shadows[1],
            backgroundColor: emphasize(theme.palette.grey[300], 0.12),
        },
    },
}))(Chip) as typeof Chip;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            maxWidth: '36ch',
            backgroundColor: theme.palette.background.paper,
            borderRadius: "0 0 4px 4px",
        },
        inline: {
            display: 'inline',
        },
        paper: {
            display: 'flex',
            // height: '80%',
            // overflowY: 'scroll',
            [theme.breakpoints.down('xs')]: {
                justifyContent: 'center',
            },
        },
        lists: {
            overflowY: 'scroll',
            height: '70vh',
            width: '35vw',
            [theme.breakpoints.down('xs')]: {
                display: 'none'
            },

        },
        drawer: {
            overflowY: 'scroll',
            // height: '65vh',
            width: '50vw',
            zIndex: 2

        },
        viewer: {
            position: 'relative',
            overflowY: 'scroll',
            height: '70vh',
            width: '65vw',
            backgroundColor: "rgba(255, 255, 255, 0.88)",
            padding: '20px 12px 20px 12px',
            boxSizing: 'border-box',
            textAlign: "justify",
            [theme.breakpoints.down('xs')]: {
                width: '92vw !important',
            },
        },
        icon: {
            width: '30px',
            height: '40px',
            color: "#3FF89FF",
            // marginLeft: 'auto',
            marginRight: 'auto',

        },
        iconContainer: {
            textAlign: 'center',
        },
        paragraph: {
            marginTop: '20px',
            textAlign: 'justify'
        },
        list: {
            width: 250,
        },
        fullList: {
            width: 'auto',
        },
        button: {
            display: 'none',
            [theme.breakpoints.down('xs')]: {
                display: 'inline'
            },
        },
        btn: {
            display: 'flex',
            alignItems: 'center',
            flexWrap: 'wrap',
            justifyContent: 'flex-end',
            // [theme.breakpoints.down('xs')]: {
            //     diaplay: 
            // }
        },
        btnGroup: {
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            backgroundColor: "#3F89FF",
            padding: "10px 10px 10px 0",
            borderRadius: "4px 4px 0 0",
            // position: "fixed",
            // width: '100vw',
            height: 'auto',
            top: 0,
            textAlign: 'center',
            marginLeft: 'auto',
            marginRight: 'auto',
            [theme.breakpoints.down('xs')]: {
                width: '92vw !important',
            },
        },
        imageBtn: {
            // [theme.breakpoints.down('xs')]: {
            //     // display: 'flex',
            //     position: 'fixed',
            //     top: '0px',
            //     right: '0px',
            //     transform: 'translate(-50%, -50%)'
            // },
        },
        modal: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        modal_paper: {
            overflowY: 'scroll',
            backgroundColor: theme.palette.background.paper,
            boxShadow: theme.shadows[10],
            padding: theme.spacing(1, 1, 1),
            maxWidth: '80%',
            maxHeight: '80%',
        },
        modal_picture: {
            maxWidth: '100%',
            maxHeight: '100%',
        },
        recordContainer: {
            marginTop: "3vh",
            // marginBottom:"0vh",
        },
        noCardNotice: {
            padding: "12px 0",
            textAlign: 'center',
            color: "#4371f0",
        },
        listButton: {
            width: '100%',
        }
    }),
);

const TextSummarizerRecords: React.FC = () => {
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });



    const toggleDrawer = (anchor: Anchor, open: boolean) => (
        event: React.KeyboardEvent | React.MouseEvent,
    ) => {
        if (
            event &&
            event.type === 'keydown' &&
            ((event as React.KeyboardEvent).key === 'Tab' ||
                (event as React.KeyboardEvent).key === 'Shift')
        ) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = (anchor: Anchor) => (
        <Paper className={classes.drawer}>
            {textSummaries.textSummary?.map((textSummary: any) => (
                <>
                    <BootstrapButton onClick={() => { setSelectedText(textSummary.OCR_text); setSelectedSummarized(textSummary.summarized_text); setSelectedImg(textSummary.photo_url); setOpen(false); setState({ ...state, 'left': false }) }}>
                        <List className={classes.root}>
                            <ListItem alignItems="flex-start">
                                {(textSummary.photo_url == null) ?
                                    <div>
                                        <ListItemAvatar>
                                            <Avatar className={classes.icon} variant="square" src={`/icon/doc.svg`} />
                                        </ListItemAvatar>
                                    </div>
                                    :
                                    <div >
                                        <ListItemAvatar>
                                            <Avatar variant="square" src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${textSummary.photo_url}`} />
                                        </ListItemAvatar>
                                    </div>
                                }
                                <ListItemText
                                    primary={textSummary.created_at}
                                    secondary={
                                        <React.Fragment>
                                            <Typography
                                                component="span"
                                                variant="body2"
                                                className={classes.inline}
                                                color="textPrimary"
                                            >
                                            </Typography>
                                            {(textSummary.summarized_text == null) ?
                                                textSummary.summarized_text :
                                                textSummary.summarized_text.substring(0, 60) + '...'}
                                        </React.Fragment>
                                    }
                                />
                            </ListItem>


                        </List>
                    </BootstrapButton>
                </>
            ))}
        </Paper>
    );

    const classes = useStyles();
    const dispatch = useDispatch();
    const theme = useTheme();
    const [selectedText, setSelectedText] = useState("")
    const [selectedSummarized, setSelectedSummarized] = useState(false);
    const [open, setOpen] = useState(false);
    const [selectedImg, setSelectedImg] = useState(null)
    const [openModal, setOpenModal] = React.useState(false);


    // const [switch, setSwitch] = useState(false);

    const handleOpenModal = () => {
        setOpenModal(true);
    };

    const handleCloseModal = () => {
        setOpenModal(false);

    };

    const handleOpen = (event: any) => {
        event.preventDefault();
        setOpen(true);
    };

    const handleClose = (event: any) => {
        event.preventDefault();
        setOpen(false);
    };

    const textSummaries = useSelector((state: IRootState) => state.textSummarizer);

    useEffect(() => {
        dispatch(getAllTextSummaryThunk())
        const mql = window.matchMedia('(max-width: 600px)')
        if (mql.matches) {
            setState({ ...state, 'left': false })
        }

        // if (textSummaries?.textSummary?.length as number == 0) {
        //     // setSelectedSummarized("No records at the moment")
        //     setSelectedText("No record at the moment")

        // } else {
        //     setSelectedText("")
        // }

    }, [textSummaries.is_deleteing, selectedText])

    return (
        <div className={classes.recordContainer}>
            <div className={classes.btnGroup}>
                <div>
                    <Button className={classes.button} onClick={toggleDrawer('left', true)}> <ListIcon /> </Button>
                    <SwipeableDrawer
                        anchor='left'
                        open={state['left']}
                        onClose={toggleDrawer('left', false)}
                        onOpen={toggleDrawer('left', true)}
                    >
                        {list('left')}
                    </SwipeableDrawer>
                </div>
                <div className={classes.btn}>
                    <div>
                        <Breadcrumbs aria-label="breadcrumb">
                            <StyledBreadcrumb label="Summarized" onClick={handleClose} />
                            <StyledBreadcrumb
                                label="Original"
                                onClick={handleOpen}
                            />
                        </Breadcrumbs>
                    </div>
                    {(selectedImg) && (
                        <div className={classes.imageBtn}>
                            <IconButton onClick={() => { handleOpenModal() }}>
                                <ImageIcon />
                            </IconButton>
                        </div>

                    )}
                </div>
            </div>
            <div className={classes.paper}>
                <Paper className={classes.lists}>
                    {textSummaries.textSummary?.map((textSummary: any) => (
                        <>
                            <BootstrapButton className={classes.listButton} onClick={() => { setSelectedText(textSummary.OCR_text); setSelectedSummarized(textSummary.summarized_text); setSelectedImg(textSummary.photo_url); setOpen(false); }}>
                                <List className={classes.root}>
                                    <ListItem alignItems="flex-start">
                                        {(textSummary.photo_url == null) ?
                                            <div>
                                                <ListItemAvatar>
                                                    <Avatar className={classes.icon} variant="square" src={`/icon/doc.svg`} />
                                                </ListItemAvatar>
                                            </div>
                                            :
                                            <div >
                                                <ListItemAvatar>
                                                    <Avatar variant="square" src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${textSummary.photo_url}`} />
                                                </ListItemAvatar>
                                            </div>
                                        }
                                        <ListItemText
                                            primary={textSummary.created_at}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        component="span"
                                                        variant="body2"
                                                        className={classes.inline}
                                                        color="textPrimary"
                                                    >
                                                    </Typography>
                                                    {(textSummary.summarized_text == null) ?
                                                        textSummary.summarized_text :
                                                        textSummary.summarized_text.substring(0, 60) + '...'}
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                </List>
                            </BootstrapButton>
                        </>
                    ))}
                </Paper>
                <Paper className={classes.viewer}>
                    {(textSummaries.textSummary == null || textSummaries.textSummary?.length as number == 0) ?
                        <div className={classes.noCardNotice}>
                            There are no any records of text summary yet.
                        </div> :
                        <div className={classes.paragraph}>
                            {(open == true) ?
                                selectedText : selectedSummarized}
                        </div>
                    }
                </Paper>
            </div>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openModal}
                onClose={handleCloseModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={openModal}>
                    <div className={classes.modal_paper}>

                        <TransformWrapper wheel={{ wheelEnabled: false }} pan={{ lockAxisY: false }}>
                            <TransformComponent >

                                <img className={classes.modal_picture} src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${selectedImg}`} ></img>
                            </TransformComponent>
                        </TransformWrapper>
                    </div>
                </Fade>
            </Modal>
        </div >
    )
    //         <div className={classes.paper}>
    //             <Paper className={classes.lists}>
    //                 {textSummaries.textSummary?.map((textSummary: any) => (
    //                     <>
    //                         <BootstrapButton onClick={() => { setSelectedText(textSummary.OCR_text); console.log(selectedText); setSelectedSummarized(textSummary.summarized_text); setSelectedImg(textSummary.photo_url); setOpen(false); }}>
    //                             <List className={classes.root}>
    //                                 <ListItem alignItems="flex-start">
    //                                     {(textSummary.photo_url == null) ?
    //                                         <div>
    //                                             <ListItemAvatar>

    //                                                 <Avatar className={classes.icon} variant="square" src={`/icon/doc.svg`} />

    //                                             </ListItemAvatar>
    //                                         </div>
    //                                         :
    //                                         <div >
    //                                             <ListItemAvatar>
    //                                                 <Avatar variant="square" src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${textSummary.photo_url}`} />
    //                                             </ListItemAvatar>
    //                                         </div>
    //                                     }
    //                                     <ListItemText
    //                                         primary={textSummary.created_at}
    //                                         secondary={
    //                                             <React.Fragment>
    //                                                 <Typography
    //                                                     component="span"
    //                                                     variant="body2"
    //                                                     className={classes.inline}
    //                                                     color="textPrimary"
    //                                                 >
    //                                                 </Typography>
    //                                                 {(textSummary.summarized_text == null) ?
    //                                                     textSummary.summarized_text :
    //                                                     textSummary.summarized_text.substring(0, 60) + '...'}
    //                                             </React.Fragment>
    //                                         }
    //                                     />
    //                                 </ListItem>


    //                             </List>
    //                         </BootstrapButton>
    //                     </>
    //                 ))}
    //             </Paper>
    //             <Paper className={classes.viewer}>
    //                 <div className={classes.paragraph}>
    //                     {(open == true) ?
    //                         selectedText : selectedSummarized}
    //                 </div>
    //             </Paper>
    //         </div>
    //         <Modal
    //             aria-labelledby="transition-modal-title"
    //             aria-describedby="transition-modal-description"
    //             className={classes.modal}
    //             open={openModal}
    //             onClose={handleCloseModal}
    //             closeAfterTransition
    //             BackdropComponent={Backdrop}
    //             BackdropProps={{
    //                 timeout: 500,
    //             }}
    //         >
    //             <Fade in={openModal}>
    //                 <div className={classes.modal_paper}>

    //                     <TransformWrapper wheel={{ wheelEnabled: false }} pan={{ lockAxisY: false }}>
    //                         <TransformComponent >

    //                             <img className={classes.modal_picture} src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${selectedImg}`} ></img>
    //                         </TransformComponent>
    //                     </TransformWrapper>
    //                 </div>
    //             </Fade>
    //         </Modal>
    //     </div >
    // )

}

export default TextSummarizerRecords