import React, { useState } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
    Backdrop,
    Modal,
    Grid,
    TextField,
    Button,
} from "@material-ui/core";
import { useSpring, animated } from "react-spring/web.cjs"; // web.cjs is required for IE 11 support
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import "../css/Buttons.css";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import { registerThunk } from "../redux/auth/thunk";

interface registerFormInput {
    email: string;
    username: string;
    password: string;
    confirmPassword: string;
    profile_picture_url: string;
}

const schema = yup.object().shape({
    email: yup
        .string()
        .required()
        .matches(/^\w+@[a-zA-Z0-9._-]+?\.[a-zA-Z]{2,3}$/),
    username: yup
        .string()
        .required()
        .min(6, "Username must not less than 6 digits"),
    password: yup
        .string()
        .required()
        .matches(/^(?=.*[a-z]).{6,}$/),
    confirmPassword: yup
        .string()
        .required()
        .oneOf([yup.ref("password")], "Passwords don't match."),
});

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        modal: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",

        },
        paper: {
            backgroundColor: theme.palette.background.paper,
            // border: '2px solid #888',
            // boxShadow: theme.shadows[2],
            padding: theme.spacing(2, 4, 3),
            width: '400px',
            height: '600px',
            [theme.breakpoints.down('xs')]: {
                width: '84vw',
            },
            '&:focus': {
                outline: "none",
            },
        },
        registerSubmit: {
            marginTop: "25px",
        },
    })
);

interface FadeProps {
    children?: React.ReactElement;
    in: boolean;
    onEnter?: () => {};
    onExited?: () => {};
}

const Fade = React.forwardRef<HTMLDivElement, FadeProps>(function Fade(
    props,
    ref
) {
    const { in: open, children, onEnter, onExited, ...other } = props;
    const style = useSpring({
        from: { opacity: 0 },
        to: { opacity: open ? 1 : 0 },
        onStart: () => {
            if (open && onEnter) {
                onEnter();
            }
        },
        onRest: () => {
            if (!open && onExited) {
                onExited();
            }
        },
    });

    return (
        <animated.div ref={ref} style={style} {...other}>
            {children}
        </animated.div>
    );
});

export default function Register() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const isRegisterProcessing = useSelector(
        (state: IRootState) => state.auth.isRegisterProcessing
    );
    const [open, setOpen] = useState(false);
    const [user, setUser] = useState({
        email: "",
        username: "",
        password: "",
        confirmPassword: "",
        profile_picture_url: "",
    });
    const { register, handleSubmit, errors } = useForm<registerFormInput>({
        resolver: yupResolver(schema),
    });

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleChange = (e: any) => {
        const { name, value, files } = e.target;
        if (open) {
            setUser((prevUser) => ({
                ...prevUser,
                [name]: value,
            }));
        }
    };

    const onRegisterSubmit = (data: registerFormInput) => {
        if (
            !isRegisterProcessing &&
            data.email &&
            data.password &&
            data.username &&
            data.confirmPassword
        ) {
            const formData = new FormData();
            formData.append("email", data.email);
            formData.append("username", data.username);
            formData.append("password", data.password);
            formData.append("confirmPassword", data.confirmPassword);
            formData.append("profile_picture_url", data.profile_picture_url[0]);
            // for (var value of formData.values()) {
            //     console.log(value); 
            //  }
            dispatch(registerThunk(formData));
        }
    };
    return (
        <div>
            <Button type="button" variant="outlined" className="basic-btn" onClick={handleOpen}>
                Register
            </Button>
            <Modal
                aria-labelledby="spring-modal-title"
                aria-describedby="spring-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h3 id="spring-modal-title">Register</h3>
                        <form onSubmit={handleSubmit(onRegisterSubmit)}>
                            <hr />
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        placeholder="Type your email here"
                                        name="email"
                                        id="register-email"
                                        label="Email"
                                        type="email"
                                        variant="outlined"
                                        onChange={handleChange}
                                        margin="normal"
                                        inputRef={register}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        error={
                                            errors.email?.message ? true : false
                                        }
                                        helperText={
                                            errors.email?.message
                                                ? `Please enter a valid email`
                                                : null
                                        }
                                        required
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        placeholder="Type your username here"
                                        name="username"
                                        id="register-username"
                                        label="Username"
                                        variant="outlined"
                                        onChange={handleChange}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        required
                                        inputRef={register}
                                        inputProps={{
                                            minLength: 6,
                                            maxLength: 20,
                                        }}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12} lg={6}>
                                    <TextField
                                        placeholder="Type your password here"
                                        name="password"
                                        id="register-password"
                                        label="Password"
                                        type="password"
                                        variant="outlined"
                                        onChange={handleChange}
                                        margin="normal"
                                        inputRef={register}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        required
                                        inputProps={{
                                            minLength: 6,
                                            maxLength: 20,
                                        }}
                                        error={
                                            errors.password?.message ? true : false
                                        }
                                        helperText={
                                            errors.password?.message
                                                ? `Password must be at least 6 characters long and contain one small english character`
                                                : null
                                        }
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12} lg={6}>
                                    <TextField
                                        placeholder="Re-type your password here"
                                        label="Confirm Password"
                                        name="confirmPassword"
                                        id="register-confirmPassword"
                                        type="password"
                                        variant="outlined"
                                        onChange={handleChange}
                                        margin="normal"
                                        inputRef={register}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        inputProps={{
                                            minLength: 6,
                                            maxLength: 20,
                                        }}
                                        error={
                                            errors.confirmPassword?.message
                                                ? true
                                                : false
                                        }
                                        helperText={
                                            errors.confirmPassword?.message
                                                ? errors.confirmPassword?.message
                                                : null
                                        }
                                        required
                                        fullWidth
                                    />
                                </Grid>
                                <input
                                    id="upload-photo"
                                    name="profile_picture_url"
                                    type="file"
                                    onChange={handleChange}
                                    ref={register}
                                />
                                {/* <img id="output" src={user.profile_picture_url} alt="user profile" /> */}
                                <div className={classes.registerSubmit}>
                                    <Button type="submit" variant="outlined">
                                        Submit
                                    </Button>
                                </div>
                            </Grid>
                        </form>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
