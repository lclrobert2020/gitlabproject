import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import InputSlider from "./InputSlider"
import { Button, makeStyles } from '@material-ui/core';

import { IRootState } from '../redux/store';
import { sendSummaryToStoreThunk, sendStringToSummaryThunk } from '../redux/textSummarizer/thunk';
import Loading from './Loading';

const useStyles = makeStyles((theme) => ({
    textArea: {
        height: "25vh",
        margin: "8px",
        padding: "8px",
        resize: "none",
        textAlign: "justify",
        borderRadius: "4px",
        color: "#3F89FF",
        '&:focus': {
            outline: 0
        }
    }
}));


const SummarizedTextArea = (props: any) => {
    const setRestObj = props.setRest
    const classes = useStyles();
    const [sendToStoreState, setSendToStoreState] = useState<Boolean>(false)
    const dispatch = useDispatch()
    const Data = useSelector((state: IRootState) => state.uploader.ReturnedData)
    const Mode = useSelector((state: IRootState) => state.uploader.Mode)
    const [textAreaIsLoading, setTextAreaIsLoading] = useState<Boolean>(false)
    const [summaryMode, setSummaryMode] = useState<number>(2) // summaryMode default to 2 which is extractive
    const [errorMsg, setErrorMsg] = useState<any>(true)
    const OCRText = Data?.data?.OCRResult;
    const SummarizedText = Data?.data?.text;
    const Text = Mode === 1 ? "Get summary" : "Save summary to server";
    const BtnText = OCRText ? "Get summary" : Text;
    const [SubBtnText, setSubBtnText] = useState<string>(BtnText)
    const { register, handleSubmit, errors, setValue } = useForm({
        defaultValues: {
            Text: OCRText ? OCRText : SummarizedText,
            Percentage: 50
        }
    })

    useEffect(() => {
        setRestObj.fx = () => { setValue("Text", "") }
        setRestObj.fx2 = setSubBtnText
        setRestObj.fx3 = () => { setSendToStoreState(false) }
    }, [setRestObj, setValue])

    const onSubmit = (data: any) => {
        data.summaryMode = summaryMode
        data.Mode = Mode
        if (Data?.itemId) {
            data.itemId = Data.itemId
        }
        if (sendToStoreState) {
            data.documentId = Data.documentId
            data.itemId = Data.itemId
            dispatch(sendSummaryToStoreThunk(data));
            return
        }
        setTextAreaIsLoading(true)
        dispatch(sendStringToSummaryThunk(data, setValue, setSendToStoreState, setSubBtnText, setErrorMsg, setTextAreaIsLoading))
        return

    }
    const [color1, setColor1] = useState<any>('#3F89FF');
    const [color2, setColor2] = useState<any>('#3F89FF');
    const [color3, setColor3] = useState<any>('#FFF');
    const [color4, setColor4] = useState<any>('#FFF');
    return (
        <>
            {!textAreaIsLoading && <div>
                <div>
                    {!sendToStoreState && <div style={{ display: "flex", justifyContent: "space-evenly", flexWrap: "wrap" }}>
                        <Button
                            variant="contained"
                            style={{
                                margin: "8px",
                                backgroundColor: color1,
                                color: color3
                            }}
                            // color={color1}
                            onClick={() => {
                                setSummaryMode(1);
                                setColor1('#27FFBB');
                                setColor2('#3F89FF');
                                setColor3('#3F89FF');
                                setColor4("#FFF")
                            }}
                        >
                            Abstractive Summary
                    </Button>
                        <Button
                            variant="contained"
                            style={{
                                margin: "8px",
                                backgroundColor: color2,
                                color: color4
                            }}
                            // color={color2}
                            onClick={() => {
                                setSummaryMode(2);
                                setColor2('#27FFBB');
                                setColor1('#3F89FF');
                                setColor4("#3F89FF")
                                setColor3('#FFF');
                            }}
                        >
                            Extractive Summary
                </Button>
                    </div>}
                </div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <textarea
                        placeholder="Place your text here"
                        style={{
                            width: `calc(100% - 16px)`,
                            maxWidth: `calc(100% - 16px)`,
                        }}
                        className={classes.textArea}
                        name="Text"
                        ref={register({ required: true })}
                    />
                    <br />
                    {errors.Text &&
                        <div style={{ color: "#FFF" }}>
                            {errors.Text.type + " text input"}
                        </div>
                    }
                    <input
                        className="HiddenPercentageInput"
                        type="hidden"
                        name="Percentage"
                        ref={register}
                    />
                    {errorMsg && errorMsg}
                    {!sendToStoreState && summaryMode === 2 &&
                        <div style={{ paddingBottom: "16px" }}>
                            <InputSlider
                                setFieldValue={(arg: any) => { setValue("Percentage", arg) }} />
                        </div>}
                    <div style={{ margin: "8px" }}>
                        <Button
                            variant="contained"
                            component="label"
                            style={{
                                backgroundColor: "#3F89FF",
                                color: "#FFF"
                            }}
                        >
                            {SubBtnText}
                            <input name="submit_button" hidden type="submit" value={SubBtnText} />
                        </Button>
                    </div>
                </form>
            </div>}
            {textAreaIsLoading &&
                <>
                    <Loading />
                </>}
        </>
    );
}

export default SummarizedTextArea