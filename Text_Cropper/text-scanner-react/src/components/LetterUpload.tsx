import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import {
    captureImg,
    toggleWebcam,
    resetUploader,
} from "../redux/uploader/action";
import WebcamCapture from "./Webcam";
import LetterCropper from "./LetterCropper";
import Loading from "./Loading";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { FormControl, FormHelperText, InputLabel, MenuItem, Select } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { getDataProcessing, getDataSuccess } from "../redux/letterUpload/actions";
import { saveLetterThunk } from "../redux/letterUpload/thunk";

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        maxWidth: 345,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: '10%',
        marginBottom: '10%',
        // height: '1000px'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    btnBox: {
        width: "92vw",
        padding: "12px 0 12px",
        display: "flex",
        justifyContent: "space-around"
    },
    uploadBtn: {
        padding: "4px 12px",
        width: 160,
        backgroundColor: "#3f89ff !important",
        color: "#FFFFFF",

    }
}));

const { REACT_APP_API_SERVER } = process.env

const LetterUploadComponent: React.FC = () => {
    const { register, handleSubmit } = useForm();
    const classes = useStyles();
    const cardRes = useSelector(
        (state: IRootState) => state.uploader.ReturnedData
    );
    const dispatch = useDispatch();
    const ImgUploaderloadingState = useSelector(
        (state: IRootState) => state.uploader.IsLoading
    );
    const webCamStatus = useSelector(
        (state: IRootState) => state.uploader.IsWebcamOpen
    );
    const webCamBtnText = useSelector(
        (state: IRootState) => state.uploader.WebCamBtuText
    );
    const isReturnedState = useSelector(
        (state: IRootState) => state.uploader.IsReturnedDataState
    );

    const letterState = useSelector(
        (state: IRootState) => state.letterUploader
    )

    const isLoading = useSelector((state: IRootState) => state.letterUploader.is_processing);

    const onSubmit = async (event: any) => {
        dispatch(getDataProcessing())
        const data = {
            item_id: letterState.item_id,
            logo_id: parseInt(org as string)
        }
        dispatch(saveLetterThunk(data))
        dispatch(getDataSuccess())
    }

    const [org, setOrg] = useState(letterState.organization_id?.toString());


    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setOrg(event.target.value as string);
    };

    useEffect(() => {
        setOrg(letterState.organization_id?.toString())
        return () => {
            dispatch(resetUploader());
            // dispatch(resetCardSavedArr())
        };
    }, [letterState, dispatch])

    const onSelectFile = (e: any) => {
        if (e.target.files && e.target.files.length > 0) {
            const reader: any = new FileReader();
            reader.addEventListener("load", () =>
                dispatch(captureImg(reader.result))
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    if (isLoading) {
        return <Loading />;
    }

    return (
        <div className="BusinessCardUploaderContainer"
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
            {!isReturnedState && (
                <div>
                    {!ImgUploaderloadingState && (
                        <>
                            <div className={classes.btnBox}>
                                <Button
                                    className={classes.uploadBtn}
                                    variant="contained"
                                    onClick={() => { dispatch(toggleWebcam()) }}
                                >{
                                        webCamBtnText}
                                </Button>
                                <Button
                                    className={classes.uploadBtn}
                                    variant="contained"
                                    component="label">
                                    Upload
                                    <input
                                        type="file"
                                        hidden
                                        accept="image/*"
                                        onClick={() => { (!webCamStatus || dispatch(toggleWebcam())) }}
                                        onChange={onSelectFile}
                                    />
                                </Button>
                            </div>
                            {webCamStatus && <WebcamCapture />}
                            <LetterCropper fetchPath="/uploadletter" />
                        </>
                    )}
                    {ImgUploaderloadingState && <Loading />}
                </div>
            )}


            {isReturnedState && (
                <>
                    <Card className={classes.root}>
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="auto"
                                // image={`${REACT_APP_API_SERVER}/${letterState.photo}`}
                                image={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${letterState.photo}`}
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {letterState.organization}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    Is it correct?
                            </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <FormControl className={classes.formControl}>
                                    {/* <InputLabel id="demo-simple-select-outlined-label"></InputLabel> */}
                                    <Select
                                        labelId="demo-simple-select-outlined-label"
                                        id="demo-simple-select-outlined"
                                        value={org}
                                        onChange={handleChange}
                                        inputRef={register}
                                    >

                                        <MenuItem value={1}>Bank Of China</MenuItem>
                                        <MenuItem value={2}>CLP Power</MenuItem>
                                        <MenuItem value={3}>HSBC</MenuItem>
                                        <MenuItem value={4}>Inland Revenue Department</MenuItem>
                                        <MenuItem value={5}>Water Supplies Department</MenuItem>
                                        <MenuItem value={6}>Other</MenuItem>
                                    </Select>
                                </FormControl>
                                <Button size="small" color="primary" type="submit">
                                    Save
                                </Button>
                            </form>
                        </CardActions>
                    </Card>

                </>
            )}
        </div>
    );
};

export default LetterUploadComponent;