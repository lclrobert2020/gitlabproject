import React from 'react';
import { Theme, makeStyles, createStyles } from '@material-ui/core';
// interface Props {
//     selectedImg: string
// }
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            display: 'block',
            position: 'absolute',
            top: 0,
            left: 0,
            width: "100vw",
            height: "100vh",
            backgroundColor: "rgba(175, 175, 175, 0.44)"
        },
        img: {
            display: 'block',
            maxWidth:'60%',
            maxHeight: '80%',
            alignItems: 'center',
            marginTop: '60px auto',
            marginBottom: 'auto',
            marginLeft: 'auto',
            marginRight: 'auto',
            border: '3px solid white'
        }
    }));


const PhotoModal = (props: any) => {
    const classes = useStyles();
    return (
        <div className={classes.backdrop}>
            <img className={classes.img} src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${props.selectedImg}`} ></img>
        </div>
    )
}

export default PhotoModal