// import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
// import Typography from '@material-ui/core/Typography';
// import Slider from '@material-ui/core/Slider';
// import { Autorenew } from '@material-ui/icons';

// const useStyles = makeStyles({
//   root: {
//     maxWidth: 600,
//     marginLeft: "auto",
//     marginRight: "auto",
//     paddingLeft: 20,
//     paddingRight: 20

//   },
// });

// function valuetext(value: number) {
//     console.log(value)
//   return `${value}°C`;
// }

// export default function DiscreteSlider() {
//   const classes = useStyles();

//   return (
//     <div className={classes.root}>
//       <Typography id="discrete-slider-small-steps" gutterBottom>
//       Drag the slider, or enter a number in the box, to set the percentage of text to keep in the summary.
//       </Typography>
//       <Slider
//         defaultValue={50}
//         getAriaValueText={valuetext}
//         aria-labelledby="discrete-slider-small-steps"
//         step={10}
//         marks
//         min={0}
//         max={100}
//         valueLabelDisplay="auto"
//       />
//     </div>
//   );
// }

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Input from '@material-ui/core/Input';
import Settings from '@material-ui/icons/Settings';

const useStyles = makeStyles({
  root: {
    maxWidth: 600,
    marginLeft: "auto",
    marginRight: "auto",
    paddingLeft:20,
    paddingRight:20,
  },
  input: {
    width: 42,
  },
});

export default function InputSlider(props:any) {
  const setFieldValue = props.setFieldValue 
  const classes = useStyles();
  const [value, setValue] = React.useState<number | string | Array<number | string>>(50);

  const handleSliderChange = (event: any, newValue: number | number[]) => {
    setValue(newValue);
    setFieldValue(newValue)
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value === '' ? '' : Number(event.target.value));
    setFieldValue(event.target.value === '' ? '' : Number(event.target.value))
  };

  const handleBlur = () => {
    if (value < 0) {
      setValue(10);
      setFieldValue(10)
    } else if (value > 100) {
      setValue(100);
      setFieldValue(100)
    }
  };

  return (
    <div className={classes.root}>
      <Typography id="input-slider" gutterBottom>
      Drag the slider, or enter a number in the box, to set the percentage of text to keep in the summary.
      </Typography>
      <Grid container spacing={2} alignItems="center">
        <Grid item>
          <Settings />
        </Grid>
        <Grid item xs>
          <Slider
            value={typeof value === 'number' ? value : 10}
            onChange={handleSliderChange}
            step={10}
            aria-labelledby="input-slider"
            min={10}
            max={100}
          />
        </Grid>
        <Grid item>
          <Input
            className={classes.input}
            value={value}
            margin="dense"
            onChange={handleInputChange}
            onBlur={handleBlur}
            inputProps={{
              step: 10,
              min: 10,
              max: 100,
              type: 'number',
              'aria-labelledby': 'input-slider',
            }}
          />%
        </Grid>
      </Grid>
    </div>
  );
}