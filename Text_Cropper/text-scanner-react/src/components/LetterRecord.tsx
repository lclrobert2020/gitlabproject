import { Theme, createStyles, Card, CardActionArea, CardContent, Typography, CardMedia, makeStyles, Button, Link, IconButton, withStyles } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { deleteLetterThunk, getAllLettersThunk } from '../redux/letter/thunk';
import { IRootState } from '../redux/store'
import Paper from '@material-ui/core/Paper';
import Loading from './Loading';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        letterContainer: {
            marginTop: '5vh',
            marginBottom: '5vh'
        },
        root: {
            display: 'flex',
            flexWrap: 'wrap',
            '& > *': {
                margin: theme.spacing(1),
                width: theme.spacing(16),
                height: theme.spacing(16),
            },
            [theme.breakpoints.down('xs')]: {
                display: 'inline',
            }
        },

        image_container: {
            // flex: "1",
            width: "100px",
            height: "100px",
            overflow: "hidden",
            display: 'flex',
            alignItems: 'center',
            textAlign: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            // padding: '50px'

        },
        image: {
            width: "100px",
            height: "100px",
            // borderStyle: 'solid',
            borderRadius: 10,
            // boxShadow: '2px 2px 2px'
            [theme.breakpoints.down('xs')]: {
                width: '100px',
                height: '100px',
            },

        },
        paper: {
            display: 'flex',
            width: '45%',
            height: '15%',
            marginLeft: 'auto',
            marginRight: 'auto',
            alignItems: 'center',
            justifyContent: 'space-between',
            padding: '0px 20px 0px 0px',
            fontSize: '10px',
            borderRadius: '6px',
            backgroundColor: 'rgb(255,255,255)',
            [theme.breakpoints.down('xs')]: {
                width: '90%',
            },
            // backgroundColor: '#D1D9D9',
        },
        button: {
            width: '15px',
            height: '15px',
        },
        icon: {
            width: '60px',
            height: '60px',
            borderRadius: '100%',
        },
        modal: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            maxHeight: "80vh"
        },
        modal_paper: {
            overflowY: 'scroll',
            backgroundColor: theme.palette.background.paper,
            // border: '2px solid #000',
            boxShadow: theme.shadows[10],
            padding: theme.spacing(1, 1, 1),
            maxWidth: '80%',
            maxHeight: '80%',
        },
        modal_picture: {
            maxWidth: '100%',
            maxHeight: '100%',
        },
        noCardNotice: {
            padding: 12,
            textAlign: 'center',
            color: "#FFFFFF",
        },
        margin: {
            margin: theme.spacing(1),
        },
        btnGroup: {
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-evenly'
        }
    }),
);

const BootstrapButton = withStyles({
    root: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 16,
        color: "rgb(50,50,50)",
        padding: '5px 5px',
        height: '100%',
        // border: '1px solid',
        lineHeight: 1.5,
        backgroundColor: 'rgb(255,255,255)',
        width: '90%',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            backgroundColor: 'rgb(200,200,200)',
            borderColor: '#0062cc',
            boxShadow: 'none',
            color: 'rgb(255,255,255)',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: 'rgb(180,180,180)',
            borderColor: '#005cbf',
        },

    },
})(Button);

const LetterRecord: React.FC = () => {

    const classes = useStyles();
    const dispatch = useDispatch();
    const [selectedImg, setSelectedImg] = useState(null)
    // const [zoom, setZoom] = useState("zoomIn")
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);

    };

    const letters = useSelector((state: IRootState) => state.letter);

    useEffect(() => {
        dispatch(getAllLettersThunk())
    }, [letters.is_deleteing])

    if (letters.is_deleteing) {
        return <Loading />;
    }

    if (letters.letters == null || letters.letters.length as number === 0) {
        return (
            <div className={classes.noCardNotice}>
                There are no any records of letter yet.
            </div>
        )
    }

    return (
        <div className={classes.letterContainer}>
            <div className={classes.root}>
                {letters.letters?.map((letter: any) => (
                    <>
                        {/* <Paper className={classes.paper} elevation={3}> */}
                        <div className={classes.paper}>

                            <BootstrapButton variant="contained" color="primary" disableRipple className={classes.margin} onClick={async () => { setSelectedImg(letter.photo_url); handleOpen() }}>
                                <div className={classes.btnGroup}>
                                    <div className={classes.image_container}>
                                        <img className={classes.image} src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${letter.photo_url}`}>
                                        </img>
                                    </div>
                                    <div>
                                        {letter.created_at}
                                    </div>
                                    <div className={classes.image_container}>
                                        <div>
                                            <img className={classes.icon} src={`/icon/${letter.logo_photo_url}`} />
                                        </div>
                                    </div>
                                </div>
                            </BootstrapButton>
                            <IconButton size="small" onClick={() => {
                                // if (window.confirm('Are you sure to delete this record?')) {
                                dispatch(deleteLetterThunk(letter.id))
                                // }
                            }}>
                                <CloseIcon className={classes.button} />
                            </IconButton>
                        </div>

                        {/* </ Paper> */}
                        {/* <PhotoModal selectedImg={selectedImg} /> */}

                    </>
                ))
                }
            </div >
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.modal_paper}>
                        <TransformWrapper wheel={{ wheelEnabled: false }} pan={{ lockAxisY: false }}>
                            <TransformComponent >

                                <img className={classes.modal_picture} src={`https://text-scanner-image.s3-ap-southeast-1.amazonaws.com/${selectedImg}`} ></img>
                            </TransformComponent>
                        </TransformWrapper>
                    </div>
                </Fade>
            </Modal>
        </div >
    )

}

export default LetterRecord