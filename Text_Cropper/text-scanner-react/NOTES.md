Basic setup

```Bash
yarn add redux react-redux @types/react-redux
yarn add redux-logger @types/redux-logger
yarn add redux-thunk @types/redux-thunk
yarn add react-router-dom @types/react-router-dom use-react-router
yarn add connected-react-router
yarn add reactstrap react react-dom
yarn add bootstrap
```

React login setup

```Bash
yarn add react-facebook-login @types/react-facebook-login
yarn add react-google-login @types/react-google-login
```
