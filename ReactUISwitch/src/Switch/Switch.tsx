import React from "react";
import "./index.css";

interface Props {
    size?: "small" | "medium" | "large";
    time?: number;
    checked: boolean;
    onChange: (checked: boolean) => void;
    disabled?: boolean;
    children?: React.ReactNode;
    offColor?: string;
    onColor?: string;
    offHandleColor?: string;
    onHandleColor?: string;
}

const animation = (time: number) => {
    return {
        WebkitTransition: `all ${time}s linear`,
        MozTransition: `all ${time}s linear`,
        OTransition: `all ${time}s linear`,
        transition: `all ${time}s linear`,
    };
};

export default class Switch extends React.PureComponent<Props, {}> {
    constructor(props: Props) {
        super(props);
        this.handleColorSelector = this.handleColorSelector.bind(this);
        this.switchColor = this.switchColor.bind(this);
    }

    handleColorSelector(props: Props) {
        if (props.checked) {
            return props.onHandleColor ? props.onHandleColor : "white";
        } else {
            return props.offHandleColor ? props.offHandleColor : "white";
        }
    }

    switchColor(props: Props) {
        if (props.checked) {
            return props.onColor ? props.onColor : "blue";
        } else {
            return props.offColor ? props.offColor : "grey";
        }
    }

    render() {
        return (
            <div
                className={`comp-switch ` + (this.props.size ? this.props.size + " " : "medium ") + (this.props.disabled ? "disable" : "")}
                onClick={() => {
                    if (!this.props.disabled) {
                        this.props.onChange(!this.props.checked);
                    }
                }}
                style={{
                    backgroundColor: this.switchColor(this.props),
                    ...(this.props.time ? animation(this.props.time) : {}),
                }}
            >
                <div
                    className={`btn-dot ` + (this.props.size ? this.props.size + " " : "medium ") + (this.props.checked ? "checked" : "")}
                    style={{
                        backgroundColor: this.handleColorSelector(this.props),
                        ...(this.props.time ? animation(this.props.time) : {}),
                    }}
                >
                    {this.props.children}
                </div>
            </div>
        );
    }
}
