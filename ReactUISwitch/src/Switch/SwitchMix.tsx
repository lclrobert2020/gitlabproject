import React from "react";
import "./index.css";

interface Props {
    size?: "small" | "medium" | "large";
    time?: number;
    checked: boolean;
    onChange: (checked: boolean) => void;
    disabled?: boolean;
    children?: React.ReactNode;
    offColor?: string;
    onColor?: string;
    offHandleColor?: string;
    onHandleColor?: string;
}

const animation = (time: number) => {
    return {
        WebkitTransition: `all ${time}s linear`,
        MozTransition: `all ${time}s linear`,
        OTransition: `all ${time}s linear`,
        transition: `all ${time}s linear`,
    };
};

const hasProps = (props: Props | {children?: React.ReactNode}): props is Props => {
    // type guard
    return (props as Props).checked !== undefined && (props as Props).onChange !== undefined;
};

export default class Switch extends React.PureComponent<Props | {}, {isSwitchChecked: boolean; isSwitchDisabled: boolean}> {
    constructor(props: Props | {children?: React.ReactNode}) {
        super(props);
        this.handleColorSelector = this.handleColorSelector.bind(this);
        this.switchColor = this.switchColor.bind(this);
        this.getCheckedStatus = this.getCheckedStatus.bind(this);
        this.onClickHandler = this.onClickHandler.bind(this);
        if (!hasProps(props)) {
            this.state = {
                // set up internal state if the component is used as uncontrolled
                isSwitchChecked: false,
                isSwitchDisabled: false,
            };
        }
    }

    handleColorSelector(props: Props | {children?: React.ReactNode}) {
        if (hasProps(props)) {
            return props.checked ? (props.onHandleColor ? props.onHandleColor : "white") : props.offHandleColor ? props.offHandleColor : "white";
        } else {
            return "white";
        }
    }

    switchColor(props: Props | {children?: React.ReactNode}) {
        if (hasProps(props)) {
            // check if the component is used as controlled
            if (props.checked) {
                return props.onColor ? (props.onColor ? props.onColor : "blue") : props.offColor ? props.offColor : "grey";
            }
        } else {
            return this.state.isSwitchChecked ? "blue" : "grey";
        }
    }

    getCheckedStatus() {
        return this.state.isSwitchChecked;
    }

    onClickHandler() {
        if (hasProps(this.props)) {
            if (!this.props.disabled) {
                this.props.onChange(!this.props.checked);
            }
        } else {
            this.setState(prevState => {
                return {
                    isSwitchChecked: !prevState.isSwitchChecked,
                };
            });
        }
    }

    render() {
        return (
            <div
                className={`comp-switch ` + (hasProps(this.props) && this.props.size ? this.props.size + " " : "medium ") + (hasProps(this.props) && this.props.disabled ? "disable" : "")}
                onClick={this.onClickHandler}
                style={{
                    backgroundColor: this.switchColor(this.props),
                    ...(hasProps(this.props) && this.props.time ? animation(this.props.time) : {}),
                }}
            >
                <div
                    className={`btn-dot ` + (hasProps(this.props) && this.props.size ? this.props.size + " " : "medium ") + (hasProps(this.props) && this.props.checked ? "checked" : this.state && this.state.isSwitchChecked ? "checked" : "")}
                    style={{
                        backgroundColor: this.handleColorSelector(this.props),
                        ...(hasProps(this.props) && this.props.time ? animation(this.props.time) : {}),
                    }}
                >
                    {this.props.children}
                </div>
            </div>
        );
    }
}
