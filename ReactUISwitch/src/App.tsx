import React from "react";
import SwitchFlex from "./Switch/SwitchFlex";
import Switch from "./Switch/Switch";
import SwitchMix from "./Switch/SwitchMix";

interface State {
    isSwitchChecked: boolean;
    isSwitchDisabled: boolean;
}

class App extends React.PureComponent<{}, State> {
    public state: State = {
        isSwitchChecked: false,
        isSwitchDisabled: false,
    };

    uncontrolledSwitch = React.createRef<SwitchMix>();

    onToggleDisable = () => this.setState({isSwitchDisabled: !this.state.isSwitchDisabled});

    onChange = (checked: boolean) => this.setState({isSwitchChecked: checked});

    render() {
        return (
            <div>
                <h1>Controlled Switch with FlexBox, animation for color changing(SwitchFlex.tsx)</h1>
                <p>Switch (using Flex Box) default</p>
                <SwitchFlex checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange} />
                <p>Switch (using Flex Box) customization </p>
                <p>size: small</p>
                <SwitchFlex size={"small"} checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange}>
                    <div style={{fontSize: "10px"}}>😀</div>
                </SwitchFlex>
                <p>size: medium, custom offColor and onColor</p>
                <SwitchFlex size={"medium"} offColor="lightblue" onColor="lightgreen" checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange}>
                    <div style={{fontSize: "20px"}}>👀</div>
                </SwitchFlex>
                <p>size: large custom offHandleColor and onHandleColor</p>
                <SwitchFlex size={"large"} offHandleColor="black" onHandleColor="yellow" checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange}>
                    <div style={{fontSize: "20px"}}>👍</div>
                </SwitchFlex>

                <h1>Controlled Switch without using FlexBox, animation for color changing and btn moving(Switch.tsx)</h1>
                <p>Switch default</p>
                <Switch checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange} />
                <p>size: small</p>
                <Switch size={"small"} checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange}>
                    <div style={{fontSize: "10px"}}>🤙</div>
                </Switch>
                <p>size: medium, custom offColor and onColor</p>
                <Switch size={"medium"} offColor="#F7D4EC" onColor="#CBE432" checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange}>
                    <div style={{fontSize: "15px"}}>❤️</div>
                </Switch>
                <p>size: large custom time(animation time),offHandleColor and onHandleColor</p>
                <Switch size={"large"} offHandleColor="#b81414" onHandleColor="#a5b814" time={1} checked={this.state.isSwitchChecked} disabled={this.state.isSwitchDisabled} onChange={this.onChange}>
                    <div style={{fontSize: "20px"}}>🌜</div>
                </Switch>

                <h1>Mixed Switch which can be used as both uncontrolled and controlled component</h1>
                <p>Uncontrolled</p>
                <SwitchMix />
                <p>Controlled</p>
                <SwitchMix checked={this.state.isSwitchChecked} onChange={this.onChange} disabled={this.state.isSwitchDisabled} />
                <p>Get the uncontrolled switch checked status by using this.ref.current.getCheckedStatus()</p>
                <SwitchMix ref={this.uncontrolledSwitch} />
                <button
                    onClick={() => {
                        if (this.uncontrolledSwitch.current) {
                            alert("Is it checked? " + this.uncontrolledSwitch.current.getCheckedStatus());
                        }
                    }}
                >
                    Press to check uncontrolledSwitch checked status
                </button>
                <p>Disable Toggle</p>
                <button type="button" onClick={this.onToggleDisable}>
                    Toggle Disable
                </button>
            </div>
        );
    }
}

export default App